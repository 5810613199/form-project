﻿[] = array 
0  = no parameter
{} = Object 
login:post{ // ล็อกอิน
	input={user_id,password}
	output= {user_info}
}
checkIsAuth:post{ // ล็อกอิน ?
	input={user_id}
	output = {boolean(true/false)}
}
checkIsStudent:post{ // เป็น นศ.?
	input={user_id}
	output= {boolean(true/false)}
}
checkIsTeacher:post{ // เป็น อจ.?
	input={user_id}
	output={boolean(true/false)}
}
checkIsStaff:post{ // เป็น จนท.?
	input={user_id}
	output={boolean(true/false)}
}
checkCanManage:post{ // เป็น จนท. ที่มีสิทธิ์จัดการ?
	input={user_id}
	output={boolean(true/false)}	
}

getAllForms:get{ //เอาทุกฟอร์มที่สร้างไว้
	input=0
	output= {[id,name,descrip]}			
} 
getFormByID:get{ //เอาฟอร์มไอดีนั้น
	input={form_id}
	output={id,name,descrip}
}
getQuestions:get{ //เอาทุกคำถามของฟอร์มนั้น
	input={form_id}
	output= {[no,type,detail,nchoice,reqAns,<option>[no,detail]]}
}
getAllRequest:get{ //เอาคำร้องทุกอย่างที่เกี่ยวข้อง 
		      ถ้านศ.เรียกจะเห็นคำร้องของตัวเอง
		      ถ้าอจ. หรือ จนท. เรียกจะเห็นคำร้องที่นศ.ยื่น ดูจากความสัมพันธ์กับ req_status
	input={user_id}
	output= {[req_id,name //ชื่อฟอร์ม,student //ชื่อนศ. ,semester,date,progress(%),status,descrip]}
}
getRoleByIdTeacher:get{ //เอาชื่อตำแหน่ง
	input={[user_id]}
	output={[role]}
}
getComment:get{ //เอาทุกคอมเมนท์ของคำร้องนั้น 
	input={req_id, user_id}
	output= {[role,comment,date,status]}
} 
getAnswerForm:get{ //เอาคำตอบของคำร้องนั้น
	input={student_id,req_id}
	output={[q_detail //คำถาม, ans_text //choiceกับtextรวมกัน ]}
}
getSigner:get{ //เอาคนลงนาม
	input=0
	output={[user_id,role]}
}
submitForm:post{
	input={[form_id,student_id,no,anstxt,ansch]}
	output={boolean(true/false)}}
}
submitComment:post{
	input={req_id, user_id, comment}
	output={boolean(true/false)}
}

updateFormName:put{ //เปลี่ยนชื่อฟอร์ม
	input:{form_id,form_name}
	output:{boolean(true/false)}
}

updatePosition:put{ //สลับตำแหน่งฟอร์ม
	input:{previous_form_id,current_form_id}
	output:{boolean(true/false)}
}









	
