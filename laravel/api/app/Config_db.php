<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config_db extends Model
{
    //Table Name
    protected $table = 'config';

    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    // we only want these 5 attributes able to be filled
    protected $fillable = array('personid', 'pos_detail');

    protected $connection = 'form';

    // Timestamps
    public $timestamps = false;

}
