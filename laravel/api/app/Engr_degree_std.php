<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Engr_degree_std extends Model
{
    //Table Name
    protected $table = 'engr_degree';

    protected $connection = 'std';

    protected $fillable = array('id', 'degree_name_th', 'degree_name_en');

    // Timestamps
    public $timestamps = false;
}
