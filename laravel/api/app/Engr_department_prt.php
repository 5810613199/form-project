<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Engr_department_prt extends Model
{
    //Table Name
    protected $table = 'engr_department';

    protected $connection = 's_t';

    protected $fillable = array('departmentid', 'departmentname');

    // Timestamps
    public $timestamps = false;
}
