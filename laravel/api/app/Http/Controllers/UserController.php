<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\Form;
use App\Form_question;
use App\Question_choice;
use App\Upload_q;
use App\Sign_control;
use App\User_fake;
use App\User;
use App\Stu_student;
use App\Per_person;
use App\Per_prtson_detail;
use App\Engr_department_prt;
use App\Engr_titleacademic;
use App\Engr_title_prt;
use App\Engr_title_std;
use App\Engr_degree_std;
use App\Engr_department_std;
use App\Req_status;
use App\Req_ans;
use App\Comment;
use App\Config_db;
use App\Subject_db;
use App\Upload_ans;
use App\Operation_lv;
use App\Session_con;
use Carbon\Carbon;
use App\oauth_access_tokens;
use Lcobucci\JWT\Parser;
use Illuminate\Support\Facades\Auth; 
use Validator;

class UserController extends Controller
{

    public $successStatus = 200;
    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            return response()->json(['success' => $success], $this-> successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401);
        } 
    }
 
    // check login
    public function checkIsAuth(Request $request)
    {
        if($request->header('username') == null){
            return response()->json('message: unauthenticated', 401);
        }
        if(count(Session_con::where('username', $request->header('username'))->get())<1){
            return response()->json('message: unauthenticated', 401);
        }
        $auth_user = $request->header('username');
        $check = $auth_user==$request['username'];
        if($check){
            return response()->json(true , 200);
        }
        return response()->json("message: Unauthenticated." , 200);
    }

    // check role student
    public function checkIsStudent(Request $request)
    {   
        // $auth_user = Auth::user();
        if($request->header('username') == null){
            return response()->json('message: unauthenticated', 401);
        }
        if(count(Session_con::where('username', $request->header('username'))->get())<1){
            return response()->json('message: unauthenticated', 401);
        }
        $auth_user = $request->header('username');
        if(count(Stu_student::where('stu_code', $auth_user)->get()) <1){
            return response()->json(false , 200);
        }
        return response()->json(true , 200);
    }

    // check role teacher

    public function checkIsTeacher(Request $request)
    {   
        if($request->header('username') == null){
            return response()->json('message: unauthenticated', 401);
        }
        if(count(Session_con::where('username', $request->header('username'))->get())<1){
            return response()->json('message: unauthenticated', 401);
        }
        $auth_user = $request->header('username');
        if(count(Per_person::where('ad_user', $auth_user)->get())<1){
            return response()->json('message: unauthenticated', 200);
        }
        $personid = Per_person::select('personid')->where('ad_user', $auth_user)->get()->first()->personid;
        if(count(Per_person::where('personid', $personid)->get()) <1 ){
            return response()->json(false , 200);
        }
        if($personid < 1000000){
            return response()->json(false, 200);
        }
        return response()->json(true , 200);
    }

    // check role staff

    public function checkIsStaff(Request $request)
    {   
        if($request->header('username') == null){
            return response()->json('message: unauthenticated', 401);
        }
        if(count(Session_con::where('username', $request->header('username'))->get())<1){
            return response()->json('message: unauthenticated', 401);
        }
        $auth_user = $request->header('username');
        if(count(Per_person::where('ad_user', $auth_user)->get())<1){
            return response()->json('message: unauthenticated', 200);
        }
        $personid = Per_person::select('personid')->where('ad_user', $auth_user)->get()->first()->personid;
        if(count(Per_person::where('personid', $personid)->get()) <1 ){
            return response()->json(false , 200);
        }
        if($personid < 1000000){
            return response()->json(true, 200);
        }
        return response()->json(false , 200);
    }

     // check role admin

     public function checkIsAdmin()
     {   
         $auth_user = Auth::user();
         $id = $auth_user->id;
         
         $user = User::findOrFail($id);
         $check = $user->username == 'admin';
         return response()->json($check , 200);
     }

    // set role 

    public function setConfig(Request $request)
    {   
        // return response()->json(Auth::user(), 200);
        if(!Auth::check()){
            return response()->json('message: unauthenticated', 401);
        }

        // if(checkIsAuth())
        $id = $request['id'];
        if(Config_db::find($id)){
            $config = Config_db::findOrFail($id);
            $temp = $config->personid;
            
            $config->personid = $request['u_id'];
            // $user = Per_person::findOrFail($request['u_id']);
            if(count(Per_person::where('personid', $request['u_id'])->get()) < 1){
                return response()->json(false, 200);
            }
            $sign_ctrl = Sign_control::all();
            
            foreach($sign_ctrl as $sign_item){
                if($temp==$sign_item->sign_t1){
                    $sign_item->sign_t1=$request['u_id'];
                } else if($temp==$sign_item->sign_t2){
                    $sign_item->sign_t2=$request['u_id'];
                } else if($temp==$sign_item->sign_t3){
                    $sign_item->sign_t3=$request['u_id'];
                } else if($temp==$sign_item->sign_t4){
                    $sign_item->sign_t4=$request['u_id'];
                } else if($temp==$sign_item->sign_t5){
                    $sign_item->sign_t5=$request['u_id'];
                }
                $sign_item->save();
            }
            // $item1 = Config_db::where('pos_detail', 'รองคณบดี')->get();
            // return response()->json($item1, 200);
            if($request['id']==Config_db::where('pos_detail', 'รองคณบดี')->first()->id || $request['id']==Config_db::where('pos_detail', 'หัวหน้าโครงการ TU-PINE')->first()->id){
                $req_status = Req_status::where('status', 'incomplete')->get();
                
                // return response()->json($req_status , 200);
                foreach($req_status as $req_item){
                    if($temp==$req_item->sign_t1){
                        $req_item->sign_t1=$request['u_id'];
                    } else if($temp==$req_item->sign_t2){
                        $req_item->sign_t2=$request['u_id'];
                    } else if($temp==$req_item->sign_t3){
                        $req_item->sign_t3=$request['u_id'];
                    } else if($temp==$req_item->sign_t4){
                        $req_item->sign_t4=$request['u_id'];
                    } else if($temp==$req_item->sign_t5){
                        $req_item->sign_t5=$request['u_id'];
                    }
                    $req_item->save();
                }
            }
            // $array_ex = array('รองคณบดี', 'หัวหน้าโครงการ TU-PINE');
            
            $config->save();

            return response()->json(true, 200);
        }
        // $check = $user->role == $request['role'];
        return response()->json(false , 200);
    }

    // set Operate_lv
    public function setOperate_lv(Request $request){
        if(!Auth::check()){
            return response()->json('message: unauthenticated', 401);
        }

        if(count(Operation_lv::where('personid', $request['u_id'])->get()) > 0){
            $opr_lv = Operation_lv::where('personid', $request['u_id'])->get()->first();
            $opr_lv->level_operate = $request['manage_lv'];
            $opr_lv->name_of_position = $request['name_of_position'];
            $opr_lv->save();
            return response()->json(true, 200);
        }
        // $check = $user->role == $request['role'];
        return response()->json(false , 200);
    }
    // get Student Information
    public function getStudentInfo(Request $request, $id){

        // $auth_user = Auth::user();
        // if($auth_user->role!='student'){
        //     return response()->json('message: unauthenticated', 401);
        // }
        // if($auth_user->id!=$id){
        //     return response()->json('message: can not access api', 200);
        // }

        if($request->header('username') == null){
            return response()->json('message: unauthenticated', 401);
        }
        $auth_user = $request->header('username');
        if($auth_user != $id){
            return response()->json('message: unauthenticated', 401);
        }
        if(count(Stu_student::where('stu_code', $auth_user)->get()) <1){
            return response()->json('message: unauthenticated', 401);
        }
        $std = Stu_student::where('stu_code', $id)->get()->first();
        $title = Engr_title_std::where('id', $std->title_id)->get()->first();
        $degree = Engr_degree_std::where('id', $std->degree_id)->get()->first();
        $department = Engr_department_std::where('dep_refer', $std->department_id)->get()->first();
        $advisor = Per_person::where('personid', $std->advisor_id)->get()->first();
        $stdarr = array($std, 'title'=>$title, 'degree' => $degree, 'department' => $department, 'advisor' => $advisor);

        // $student = Student::findOrFail($id);
        return response()->json($stdarr, 200);
    }

    // get All Teacher 
    public function getAllTeacher(){
        if(!Auth::check()){
            return response()->json('message: unauthenticated', 401);
        }
        $teacher = Per_person::where('personid', '>', 999999)->get();
        // return response()->json($teacher, 200);
        $teacher_arr = (array)null;
        foreach($teacher as $teach_item){
            $department = Engr_department_prt::where('departmentid', $teach_item->department)->get()->first();
            $titleaca = Engr_titleacademic::where('titleacaid', $teach_item->title_academic)->get()->first();
            $t5_arr = array('teacher'=> $teach_item, 'department' => $department, 'titleaca' => $titleaca);
            array_push($teacher_arr, $t5_arr);
        }
        return response()->json($teacher_arr, 200);
    }

    // get all staff teacher
    public function getTeacherStaff(){
            
        $person = Per_person::all();
        $person_arr = (array)null;
        foreach($person as $person_item){
            if($person_item->personid >= 1000000){
                $title = Engr_titleacademic::where('titleacaid', $person_item->title_academic)->get()->first();
            } else {
                $title = Engr_title_prt::where('titleid', $person_item->title)->get()->first();
            }
            $department = Engr_department_prt::where('departmentid', $person_item->department)->get()->first();
            $t5_arr = array('person'=> $person_item, 'title' => $title, 'department' => $department);
            array_push($person_arr, $t5_arr);
        }
        return response()->json($person_arr, 200);
    }

    // get all staff teacher
    public function getAllStaff(){
            
        $person = Per_person::all();
        $person_arr = (array)null;
        foreach($person as $person_item){
            if($person_item->personid < 1000000){
                $title = Engr_title_prt::where('titleid', $person_item->title)->get()->first();
                $department = Engr_department_prt::where('departmentid', $person_item->department)->get()->first();
                $opr_lv = Operation_lv::where('personid', $person_item->personid)->get()->first();
                $t5_arr = array('person'=> $person_item, 'title' => $title, 'department' => $department, 'opr_lv' => $opr_lv);
                array_push($person_arr, $t5_arr);
            }
        }
        return response()->json($person_arr, 200);
    }

    // Create new Config
    public function createConfig(Request $request){
        if(!Auth::check()){
            return response()->json('message: unauthenticated', 401);
        }

        $config = new Config_db;
        $config->personid = $request['u_id'];
        $config->pos_detail = $request['pos_detail'];
        // return response()->json(Config_db::select('*')->where('pos_detail', $request['pos_detail'])->get(), 200);
        if(count(Config_db::select('*')->where('pos_detail', $request['pos_detail'])->get())!=0){
            return response()->json(false, 200);
        }
        $config->save();
        return response()->json(true, 200);
        
        
        // $check = $user->role == $request['role'];
    }

    // get All Operate_lv
    public function getAllOperate_lv(){
        if(!Auth::check()){
            return response()->json('message: unauthenticated', 401);
        }

        $opr_lv = Operation_lv::all();

        return response()->json($opr_lv, 200);
    }


    // get setable config
    public function getsetConfig(){
        if(!Auth::check()){
            return response()->json('message: unauthenticated', 401);
        }

        $config = Config_db::whereNotBetween('personid', [11110, 11111])->get();

        return response()->json($config, 200);
    }

    // get Student Information
    public function getStudentInfoBystdid($id){
        // $User = User::findOrFail($id);
        // $student = $User->student->first();
        $student = Student::findOrFail($id);
        return response()->json($student, 200);
    }

    // get Teacher Information
    public function getTeacherInfo($id){
        $teacher = Per_person::where('personid', $id)->get()->first();
        $department = Engr_department_prt::where('departmentid', $teacher->department)->get()->first();
        $titleaca = Engr_titleacademic::where('titleacaid', $teacher->title_academic)->get()->first();
        $t5_arr = array('teacher'=> $teacher, 'department' => $department, 'titleaca' => $titleaca);
        return response()->json($t5_arr, 200);
    }

    // get Staff Information
    public function getStaffInfo($id){
        $staff = Per_person::where('personid', $id)->get()->first();
        $title = Engr_title_prt::where('titleid', $staff->title)->get()->first();
        $department = Engr_department_prt::where('departmentid', $staff->department)->get()->first();
        $staff = array('staff'=> $staff, 'title' => $title, 'department' => $department);
        return response()->json($staff, 200);
    }

    // get user info
    public function getUserInfo(Request $request){
        if($request->header('username') == null){
            return response()->json('message: unauthenticated', 401);
        }
        if(count(Session_con::where('username', $request->header('username'))->get())<1){
            return response()->json('message: unauthenticated', 401);
        }
        $username = $request->header('username');
        if(count(Stu_student::where('stu_code', $username)->get()) > 0){
            $std = Stu_student::where('stu_code', $username)->get()->first();
            $title = Engr_title_std::where('id', $std->title_id)->get()->first();
            $degree = Engr_degree_std::where('id', $std->degree_id)->get()->first();
            $department = Engr_department_std::where('dep_refer', $std->department_id)->get()->first();
            $advisor = Per_person::where('personid', $std->advisor_id)->get()->first();
            $stdarr = array($std, 'title'=>$title, 'degree' => $degree, 'department' => $department, 'advisor' => $advisor);
            return response()->json($stdarr, 200);
        } else {
            $person = Per_person::where('ad_user', $username)->get()->first();
            if($person->personid >= 1000000){
                $title = Engr_titleacademic::where('titleacaid', $person->title_academic)->get()->first();
            } else {
                $title = Engr_title_prt::where('titleid', $person->title)->get()->first();
            }
            $department = Engr_department_prt::where('departmentid', $person->department)->get()->first();
            $t5_arr = array('person'=> $person, 'title' => $title, 'department' => $department);
            return response()->json($t5_arr, 200);
        }
    }
    //get Role By id
    public function getRoleByIdTeacher(Request $request, $id){
        // $auth_user = Auth::user();
        // if($auth_user->role!='teacher'){
        //     return response()->json('message: unauthenticated', 401);
        // }

        if($request->header('username') == null){
            return response()->json('message: unauthenticated', 401);
        }
        $auth_user = $request->header('username');
        if(count(Per_person::where('ad_user', $auth_user)->get())<1){
            return response()->json('message: unauthenticated', 200);
        }
        $personid = Per_person::select('personid')->where('ad_user', $auth_user)->get()->first()->personid;
        if(count(Per_person::where('personid', $personid)->get()) <1 ){
            return response()->json('message: unauthenticated', 401);
        }
        if($personid < 1000000){
            return response()->json('message: unauthenticated', 401);
        }

        $user = Per_person::where('personid', $id)->get()->first();
        $department = Engr_department_prt::where('departmentid', $user->department)->get()->first();
        $titleaca = Engr_titleacademic::where('titleacaid', $user->title_academic)->get()->first();
        $userarr = array('user'=> $user, 'department' => $department, 'titleaca' => $titleaca);
        $config_db = Config_db::where('personid', $id)->get()->first();
        // $user = User::select('role')->where('id', $id)->get();
        return response()->json(array('teacher' => $userarr, 'config' => $config_db), 200);
    }

    public function logoutApi(Request $request)
    { 
        $value = $request->bearerToken();
        $id = (new Parser())->parse($value)->getHeader('jti');
        $token = $request->user()->tokens->find($id);
        $token->revoke();

        return response()->json(true, 200);
    }


    public function deleteConfig($id)
    {
        $auth_user = Auth::user();
        if($auth_user->username!='admin'){
            return response()->json('message: unauthenticated', 401);
        }   
        // Get article
        $config = Config_db::findOrFail($id);

        if($config->delete()){
            return response()->json(true, 200);
        }
        return response()->json(false, 200);
    }
}
