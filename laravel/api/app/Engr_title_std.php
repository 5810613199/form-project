<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Engr_title_std extends Model
{
    //Table Name
    protected $table = 'engr_title';

    protected $connection = 'std';

    protected $fillable = array('id', 'title_name_fth', 'title_name_lth', 'title_name_fen', 'title_name_len');

    // Timestamps
    public $timestamps = false;
}
