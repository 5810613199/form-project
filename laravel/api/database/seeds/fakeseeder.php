<?php

use Illuminate\Database\Seeder;
use App\Form;
use App\Form_question;
use App\Question_choice;
use App\Upload_q;
use App\Sign_control;
use App\User_fake;
use App\User;
use App\Stu_student;
use App\Per_person;
use App\Per_prtson_detail;
use App\Engr_department_prt;
use App\Engr_titleacademic;
use App\Engr_title_prt;
use App\Engr_title_std;
use App\Engr_degree_std;
use App\Engr_department_std;
use App\Req_status;
use App\Req_ans;
use App\Comment;
use App\Config_db;
use App\Subject_db;
use App\Upload_ans;

class fakeseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        // clear our database ------------------------------------------
        DB::connection('fake_ad')->table('users')->delete();

        // user 1
        $user1 = User_fake::create(array(
            'username'         => '5810680354',
            'email'         => 'tailove123@windowslive.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
            
        ));

        // user 2
        $user2 = User_fake::create(array(
            'username'         => '5810613199',
            'email'         => 'kriszoza.007@gmail.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
        ));

        // user 13
        $user13 = User_fake::create(array(
            'username'         => '5810610013',
            'email'         => 'soften@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
        ));

        // user 3 
        $user3 = User_fake::create(array(
            'username'         => 'teaher1',
            'email'         => 'teacher1@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
        ));

        // user 4 
        $user4 = User_fake::create(array(
            'username'         => 'teaher2',
            'email'         => 'teacher2@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
        ));

        // user 5 
        $user5 = User_fake::create(array(
            'username'         => 'teaher3',
            'email'         => 'teacher3@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
        ));

        // user 6
        $user6 = User_fake::create(array(
            'username'         => 'staff1',
            'email'         => 'staff1@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
        ));

        // user 7
        $user7 = User_fake::create(array(
            'username'         => 'staff2',
            'email'         => 'staff2@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
        ));

        // user 8
        $user8 = User_fake::create(array(
            'username'         => 'teaher4',
            'email'         => 'teacher4@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
        ));

        // user 9
        $user9 = User_fake::create(array(
            'username'         => 'teaher5',
            'email'         => 'teacher5@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
        ));

        // user 10
        $user10 = User_fake::create(array(
            'username'         => 'teaher6',
            'email'         => 'teacher6@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
        ));

        // user 11
        $user11 = User_fake::create(array(
            'username'         => 'teaher7',
            'email'         => 'teacher7@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
        ));

        // user 12
        $user12 = User_fake::create(array(
            'username'         => 'teaher8',
            'email'         => 'teacher8@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
        ));

        DB::connection('form')->table('users')->delete();
        // user 999
        $user999 = User::create(array(
            'username'         => 'admin',
            'email'         => 'admin@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
        ));

        $this->command->info('Create user Success!!');


        DB::connection('s_t')->table('per_person')->delete();
        DB::connection('s_t')->table('per_prtson_detail')->delete();
        DB::connection('s_t')->table('engr_department')->delete();
        DB::connection('s_t')->table('engr_titleacademic')->delete();


        // person title academic
        // prt titleaca1
        $titleaca_prt1 = Engr_titleacademic::create(array(
            'titleacaid' => '001', 
            'titleacafullt' => 'อาจารย์', 
            'titleacafulle' => 'Instructor', 
            'titleacalitt' => 'อ.', 
            'titleacalite' => 'Ins.'
        ));

        // prt titleaca2
        $titleaca_prt2 = Engr_titleacademic::create(array(
            'titleacaid' => '002', 
            'titleacafullt' => 'ผู้ช่วยศาสตราจารย์', 
            'titleacafulle' => 'Assistant Professor', 
            'titleacalitt' => 'ผศ.', 
            'titleacalite' => 'Asst. Prof.'
        ));

        // prt titleaca3
        $titleaca_prt3 = Engr_titleacademic::create(array(
            'titleacaid' => '003', 
            'titleacafullt' => 'รองศาสตราจารย์', 
            'titleacafulle' => 'Associate Professor', 
            'titleacalitt' => 'รศ.', 
            'titleacalite' => 'Assoc. Prof.'
        ));

        // prt titleaca4
        $titleaca_prt4 = Engr_titleacademic::create(array(
            'titleacaid' => '004', 
            'titleacafullt' => 'ศาสตราจารย์', 
            'titleacafulle' => 'Professor', 
            'titleacalitt' => 'ศ.', 
            'titleacalite' => 'Prof.'
        ));

        // prt titleaca5
        $titleaca_prt5 = Engr_titleacademic::create(array(
            'titleacaid' => '005', 
            'titleacafullt' => 'เจ้าหน้าที่', 
            'titleacafulle' => 'Staff', 
            'titleacalitt' => 'จนท.', 
            'titleacalite' => 'Stf.'
        ));

        $this->command->info('Create title academic person Success!!');

        // person Department
        // department 1 
        $dep_prt1 = Engr_department_prt::create(array(
            'departmentid' => '001', 
            'departmentname' => 'TU-PINE'
        ));
        // department 2
        $dep_prt2 = Engr_department_prt::create(array(
            'departmentid' => '002', 
            'departmentname' => 'NORMAL'
        ));

        $this->command->info('Create department person Success!!');

        // teacher 2
        $teacher2 = Per_person::create(array(
            'personid' => '1000002',
            'title' => 1,
            'fname_t'         => 'อาจารย์ตัวอย่าง',
            'lname_t' => 'คนที่2',
            'fname_e'         => 'exampleteacher',
            'lname_e' => 'second',
            'department' => $dep_prt1->departmentid,
            'title_academic' => $titleaca_prt1->titleacaid,
            'ad_user' => $user4->username,
        ));
        $teacher2_detail = Per_prtson_detail::create(array(
            'personid' => $teacher2->personid,
            'idcard' => '1111111111112',
            'email' => $user4->email,
            'positionid' => ''
        ));

        // teacher 3
        $teacher3 = Per_person::create(array(
            'personid' => '1000003',
            'title' => 2,
            'fname_t'         => 'อาจารย์ตัวอย่าง',
            'lname_t' => 'คนที่3',
            'fname_e'         => 'exampleteacher',
            'lname_e' => 'third',
            'department' => $dep_prt1->departmentid,
            'title_academic' => $titleaca_prt1->titleacaid,
            'ad_user' => $user5->username,
        ));
        $teacher3_detail = Per_prtson_detail::create(array(
            'personid' => $teacher3->personid,
            'idcard' => '1111111111113',
            'email' => $user5->email,
            'positionid' => ''
        ));


        // teacher 5
        $teacher5 = Per_person::create(array(
            'personid' => '1000005',
            'title' => 3,
            'fname_t'         => 'อาจารย์ตัวอย่าง',
            'lname_t' => 'คนที่5',
            'fname_e'         => 'exampleteacher',
            'lname_e' => 'fifth',
            'department' => $dep_prt2->departmentid,
            'title_academic' => $titleaca_prt2->titleacaid,
            'ad_user' => $user9->username,
        ));
        $teacher5_detail = Per_prtson_detail::create(array(
            'personid' => $teacher5->personid,
            'idcard' => '1111111111115',
            'email' => $user9->email,
            'positionid' => ''
        ));

        // teacher 6
        $teacher6 = Per_person::create(array(
            'personid' => '1000006',
            'title' => 4,
            'fname_t'         => 'อาจารย์ตัวอย่าง',
            'lname_t' => 'คนที่6',
            'fname_e'         => 'exampleteacher',
            'lname_e' => 'sixth',
            'department' => $dep_prt2->departmentid,
            'title_academic' => $titleaca_prt3->titleacaid,
            'ad_user' => $user10->username,
        ));
        $teacher6_detail = Per_prtson_detail::create(array(
            'personid' => $teacher6->personid,
            'idcard' => '1111111111116',
            'email' => $user10->email,
            'positionid' => ''
        ));

        // teacher 7
        $teacher7 = Per_person::create(array(
            'personid' => '1000007',
            'title' => 5,
            'fname_t'         => 'อาจารย์ตัวอย่าง',
            'lname_t' => 'คนที่5',
            'fname_e'         => 'exampleteacher',
            'lname_e' => 'seventh',
            'department' => $dep_prt1->departmentid,
            'title_academic' => $titleaca_prt4->titleacaid,
            'ad_user' => $user11->username,
        ));
        $teacher7_detail = Per_prtson_detail::create(array(
            'personid' => $teacher7->personid,
            'idcard' => '1111111111117',
            'email' => $user11->email,
            'positionid' => ''
        ));

        // teacher 8
        $teacher8 = Per_person::create(array(
            'personid' => '1000008',
            'title' => 1,
            'fname_t'         => 'อาจารย์ตัวอย่าง',
            'lname_t' => 'คนที่8',
            'fname_e'         => 'exampleteacher',
            'lname_e' => 'eightth',
            'department' => $dep_prt1->departmentid,
            'title_academic' => $titleaca_prt1->titleacaid,
            'ad_user' => $user12->username,
        ));
        $teacher8_detail = Per_prtson_detail::create(array(
            'personid' => $teacher8->personid,
            'idcard' => '1111111111118',
            'email' => $user12->email,
            'positionid' => ''
        ));

        $this->command->info('Create Teacher Success!!');

        // we'll create three different Staff
        // staff 1
        $staff1 = Per_person::create(array(
            'personid' => '0000001',
            'title' => 2,
            'fname_t'         => 'เจ้าหน้าที่ตัวอย่าง',
            'lname_t' => 'คนที่1',
            'fname_e'         => 'examplestaff',
            'lname_e' => 'first',
            'department' => $dep_prt1->departmentid,
            'title_academic' => $titleaca_prt5->titleacaid,
            'ad_user' => $user6->username,
        ));
        $staff1_detail = Per_prtson_detail::create(array(
            'personid' => $staff1->personid,
            'idcard' => '0000000000001',
            'email' => $user6->email,
            'positionid' => ''
        ));
        
        // staff 2
        $staff2 = Per_person::create(array(
            'personid' => '0000002',
            'title' => 3,
            'fname_t'         => 'เจ้าหน้าที่ตัวอย่าง',
            'lname_t' => 'คนที่2',
            'fname_e'         => 'examplestaff',
            'lname_e' => 'second',
            'department' => $dep_prt1->departmentid,
            'title_academic' => $titleaca_prt5->titleacaid,
            'ad_user' => $user7->username,
        ));
        $staff2_detail = Per_prtson_detail::create(array(
            'personid' => $staff2->personid,
            'idcard' => '0000000000002',
            'email' => $user7->email,
            'positionid' => ''
        ));

        $this->command->info('Create Staff Success!!');

        DB::connection('std')->table('stu_student')->delete();
        DB::connection('std')->table('engr_title')->delete();
        DB::connection('std')->table('engr_degree')->delete();
        DB::connection('std')->table('engr_department')->delete();

        // std title 
        // std title1
        $title_std1 = Engr_title_std::create(array(
            // 'id' => 1, 
            'title_name_fth' => 'นาย', 
            'title_name_lth' => 'นาย', 
            'title_name_fen' => 'Mr.', 
            'title_name_len' => 'Mr.'
        ));

        // std title2
        $title_std2 = Engr_title_std::create(array(
            // 'id' => 2, 
            'title_name_fth' => 'นางสาว', 
            'title_name_lth' => 'น.ส.', 
            'title_name_fen' => 'Miss.', 
            'title_name_len' => 'Miss.'
        ));

        // std title3
        $title_std3 = Engr_title_std::create(array(
            // 'id' => 3, 
            'title_name_fth' => 'นาง', 
            'title_name_lth' => 'นาง', 
            'title_name_fen' => 'Mrs.', 
            'title_name_len' => 'Mrs.'
        ));


        $this->command->info('Create title student Success!!');

        $dep_std1 = Engr_department_std::create(array(
            'depid' => 1,
            'major_th' => 'ปกติ',
            'major_en' => 'Normal', 
            'department_th' => 'คอมพิวเตอร์', 
            'department_en' => 'Computer', 
            'dep_refer' => 'CN'
        ));
        
        $dep_std2 = Engr_department_std::create(array(
            'depid' => 2,
            'major_th' => 'ทียูพาย',
            'major_en' => 'TU-PINE', 
            'department_th' => 'ไอเพน', 
            'department_en' => 'IPEN', 
            'dep_refer' => 'IP'
        ));

        $dep_std3 = Engr_department_std::create(array(
            'depid' => 3,
            'major_th' => 'ทียูพาย',
            'major_en' => 'TU-PINE', 
            'department_th' => 'อีบีเอ็ม', 
            'department_en' => 'EBM', 
            'dep_refer' => 'EBM'
        ));

        $dep_std4 = Engr_department_std::create(array(
            'depid' => 4,
            'major_th' => 'ทียูพาย',
            'major_en' => 'TU-PINE', 
            'department_th' => 'ซอฟต์เอ็น', 
            'department_en' => 'Soften', 
            'dep_refer' => 'SE'
        ));

        $this->command->info('Create department student Success!!');

        // student Degree
        // degree 1
        $degree_std1 = Engr_degree_std::create(array(
            // 'id' => 1,
            'degree_name_th' => 'ปริญญาตรี', 
            'degree_name_en' => 'Bachelor Degrees', 
        ));
        // degree 2
        $degree_std2 = Engr_degree_std::create(array(
            // 'id' => 2,
            'degree_name_th' => 'ปริญญาโท  ', 
            'degree_name_en' => 'Master Degrees', 
        ));

        // degree 3
        $degree_std3 = Engr_degree_std::create(array(
            // 'id' => 3,
            'degree_name_th' => 'ปริญญาเอก', 
            'degree_name_en' => 'Doctor Degrees', 
        ));


        $this->command->info('Create degree student Success!!');

        // we'll create three different Student
        // student 1
        $student1 = Stu_student::create(array(
            'stu_code'         => $user1->username,
            'title_id' => $title_std1->id,
            'fname_th'         => 'ธนัท',
            'lname_th' => 'บุญคง',
            'fname_en' => 'Thanat',
            'lname_en' => 'Bookong',
            'degree_id' => $degree_std1->id,
            'department_id' => $dep_std1->dep_refer,
            'advisor_id' => $teacher7->personid,
            'reg_telephone' => '0967144322',
            'email' => $user1->email,
        ));

        // student 2
        $student2 = Stu_student::create(array(
            'stu_code'         => $user2->username,
            'title_id' => $title_std1->id,
            'fname_th'         => 'กฤษณเทพ',
            'lname_th' => 'บุญพรหมมา',
            'fname_en' => 'Krisanatep',
            'lname_en' => 'Boonpromma',
            'degree_id' => $degree_std1->id,
            'department_id' => $dep_std1->dep_refer,
            'advisor_id' => $teacher6->personid,
            'reg_telephone' => '0967144322',
            'email' => $user2->email,
        ));

        // student 3
        $student3 = Stu_student::create(array(
            'stu_code'         => $user13->username,
            'title_id' => $title_std2->id,
            'fname_th'         => 'ซอฟต์เอนต์',
            'lname_th' => 'ทียูพาย',
            'fname_en' => 'Soften',
            'lname_en' => 'TUPINE',
            'degree_id' => $degree_std2->id,
            'department_id' => $dep_std2->dep_refer,
            'advisor_id' => $teacher6->personid,
            'reg_telephone' => '0967144322',
            'email' => $user13->email,
        ));

        $this->command->info('Create Student Success!!');

        DB::connection('form')->table('subject')->delete();

        // subject1 
        $subject1 = Subject_db::create(array(
            'personid'         => $teacher5->personid,
            'sub_code' => 'MA111',
            'sub_name' => 'Calculus1',
            'section' => 1
        ));

        // subject2
        $subject2 = Subject_db::create(array(
            'personid'         => $teacher6->personid,
            'sub_code' => 'PHY111',
            'sub_name' => 'Physic1',
            'section' => 1
        ));

        // subject3 
        $subject3 = Subject_db::create(array(
            'personid'         => $teacher7->personid,
            'sub_code' => 'TU156',
            'sub_name' => 'C Language',
            'section' => 1
        ));

        // subject4 
        $subject4 = Subject_db::create(array(
            'personid'         => $teacher8->personid,
            'sub_code' => 'MA111',
            'sub_name' => 'Calculus1',
            'section' => 2
        ));

        $this->command->info('Create Subject Success!!');


        DB::connection('form')->table('config')->delete();

        // config 5
        $config5 = Config_db::create(array(
            'personid'         => 11111,
            'pos_detail' => 'อาจารย์ที่ปรึกษา',
        ));

        // config 3
        $config3 = Config_db::create(array(
            'personid'         => 11110,
            'pos_detail' => 'อาจารย์ประจำวิชา',
        ));
                
        // config 1
        $config1 = Config_db::create(array(
            'personid'         => $teacher3->personid,
            'pos_detail' => 'รองคณบดี',
        ));

        // config 2
        $config2 = Config_db::create(array(
            'personid'         => $teacher2->personid,
            'pos_detail' => 'หัวหน้าโครงการ TU-PINE',
        ));

        // config 4
        $config4 = Config_db::create(array(
            'personid'         => $staff1->personid,
            'pos_detail' => 'เจ้าหน้าที่ตรวจสอบ',
        ));

        $this->command->info('Create Config Success!!');


        // clear our database ------------------------------------------
        DB::connection('form')->table('form')->delete();
        DB::connection('form')->table('form_question')->delete();
        DB::connection('form')->table('question_choice')->delete();
        DB::connection('form')->table('sign_control')->delete();
    
        // seed our form table -----------------------
        // we'll create three different bears
    
        // form 1
        $form_req1 = Form::create(array(
            'form_name'         => 'request for subject',
            'mod_by'         => 'thanat',
            'create_by' => 'thanat',
            'form_pos' => 3
        ));
    
        // form 2
        $form_req2 = Form::create(array(
            'form_name'         => 'leave off the study',
            'mod_by'         => 'kris',
            'create_by' => 'kris',
            'form_pos' => 2
        ));
    
        // form 3
        $form_req3 = Form::create(array(
            'form_name'         => 'resign form study',
            'mod_by'         => 'teacher1',
            'create_by' => 'teacher1',
            'form_pos' => 1
        ));
    
        $this->command->info('Create From Success!!');
    
        // seed our form_question table ------------------------
    
        // form 1 question 1
        $form_question11 = Form_question::create(array(
            'form_id'         => $form_req1->id,
            'question_seq'         => 1,
            'question_type' => 'checkboxes',
            'question_detail' => 'what is that',
            'n_choice' => 2,
            'req_ans' => true,
            'other' => true,
        ));
    
        // form 1 question 2
        $form_question12 = Form_question::create(array(
            'form_id'         => $form_req1->id,
            'question_seq'         => 2,
            'question_type' => 'subject',
            'question_detail' => 'what subject',
            'n_choice' => 2,
            'req_ans' => true,
            'other' => false,
        ));
    
        // form 2 question 1
        $form_question21 = Form_question::create(array(
            'form_id'         => $form_req2->id,
            'question_seq'         => 1,
            'question_type' => 'short answer',
            'question_detail' => 'what is your name',
            'n_choice' => 0,
            'req_ans' => true,
            'other' => false,
        ));
    
        // form 2 question 2
        $form_question22 = Form_question::create(array(
            'form_id'         => $form_req2->id,
            'question_seq'         => 2,
            'question_type' => 'short answer',
            'question_detail' => 'what is your fav',
            'n_choice' => 0,
            'req_ans' => true,
            'other' => false,
        ));
    
        // form 3 question 1
        $form_question31 = Form_question::create(array(
            'form_id'         => $form_req3->id,
            'question_seq'         => 1,
            'question_type' => 'Radio Button',
            'question_detail' => 'what is your pet',
            'n_choice' => 2,
            'req_ans' => true,
            'other' => false,
        ));
    
        // form 3 question 2
        $form_question32 = Form_question::create(array(
            'form_id'         => $form_req3->id,
            'question_seq'         => 2,
            'question_type' => 'file upload',
            'question_detail' => 'your paper',
            'n_choice' => 0,
            'req_ans' => true,
            'other' => false,
        ));
    
        $this->command->info('Create Question Success');
    
        // seed our choice table ---------------------
    
        // form 1 question 1 choice 1
        $choice111 = Question_choice::create(array(
            'question_id'         => $form_question11->id,
            'choice_detail'         => 'dog',
            'choice_seq' => 1,
        ));
    
        // form 1 question 1 choice 2
        $choice112 = Question_choice::create(array(
            'question_id'         => $form_question11->id,
            'choice_detail'         => 'cat',
            'choice_seq' => 2,
        ));
    
        // form 3 question 1 choice 1
        $choice131 = Question_choice::create(array(
            'question_id'         => $form_question31->id,
            'choice_detail'         => 'fish',
            'choice_seq' => 1,
        ));
    
        // form 3 question 1 choice 2
        $choice132 = Question_choice::create(array(
            'question_id'         => $form_question31->id,
            'choice_detail'         => 'bear',
            'choice_seq' => 2,
        ));
    
        $this->command->info('Create choice success!!');
    
        // seed our file upload table ---------------------
    
        // form 3 file upload 1
        $fileupload31 = Upload_q::create(array(
            'question_id'         => $form_question32->id,
            'form_id'         => $form_req3->id,
            'n_file' => 1,
            'file_type' => 'jpg',
        ));
    
        $this->command->info('Create file upload success!!');
        

        // seed our sign control table ---------------------

        // form 1 sign control
        $signcontrol1 = Sign_control::create(array(
            'form_id'         => $form_req1->id,
            'n_sign'         => 3,
            'sign_t1' => $staff1->personid,
            'sign_t2' => 11110,
            'sign_t3' => $teacher2->personid,
        ));

        // form 2 sign control
        $signcontrol2 = Sign_control::create(array(
            'form_id' => $form_req2->id,
            'n_sign'  => 3,
            'sign_t1' => $staff1->personid,
            'sign_t2' => 11111,
            'sign_t3' => $teacher2->personid,
        ));

        // form 3 sign control
        $signcontrol3 = Sign_control::create(array(
            'form_id' => $form_req3->id,
            'n_sign'  => 1,
            'sign_t1' => $staff1->personid,

        ));

        // clear our database ------------------------------------------
        DB::connection('form')->table('req_ans')->delete();
        DB::connection('form')->table('req_status')->delete();
        DB::connection('form')->table('comment')->delete();
        DB::connection('form')->table('upload_ans')->delete();

        // seed our Req_status table ------------------------
        // Req_status1 
        $reqstatus1 = Req_status::create(array(
            'form_id'         => $form_req1->id,
            'student_id'         => $student1->stu_code,
            'sign_t1' => $staff1->personid,
            'sign_t2' => $teacher5->personid,
            'sign_t3' => $teacher2->personid,
            'sign_t1_s' => 1,
            'sign_t2_s' => 2,
            'sign_t3_s' => 0,
            'status' => 'incomplete',
            'semester' => '2/61'
        ));

        // Req_status2 
        $reqstatus2 = Req_status::create(array(
            'form_id'         => $form_req2->id,
            'student_id'         => $student2->stu_code,
            'sign_t1' => $staff1->personid,
            'sign_t2' => $student2->advisor_id,
            'sign_t3' => $teacher2->personid,
            'sign_t1_s' => 0,
            'status' => 'incomplete',
            'semester' => '1/61'
        ));

        // Req_status3
        $reqstatus3 = Req_status::create(array(
            'form_id'         => $form_req3->id,
            'student_id'         => $student2->stu_code,
            'sign_t1' => $staff1->personid,
            'sign_t1_s' => 0,
            'status' => 'incomplete',
            'semester' => '1/61'
        ));

        $this->command->info('Create Req_status Success!!');

        // seed our Req_ans table ------------------------
        // Req_ans1 
        $reqans1 = Req_ans::create(array(
            'question_id'         => $form_question11->id,
            'req_id'         => $reqstatus1->id,
            'ans_choice' => $choice111->id,
        ));

        // Req_ans1 
        $reqans5 = Req_ans::create(array(
            'question_id'         => $form_question11->id,
            'req_id'         => $reqstatus1->id,
            'ans_choice' => $choice112->id,
        ));

        // Req_ans2
        $reqans2 = Req_ans::create(array(
            'question_id'         => $form_question12->id,
            'req_id'         => $reqstatus1->id,
            'ans_text' => 'MA111/1',
        ));

        // Req_ans3
        $reqans3 = Req_ans::create(array(
            'question_id'         => $form_question21->id,
            'req_id'         => $reqstatus2->id,
            'ans_text' => 'krisanatep',
        ));

        // Req_ans4
        $reqans4 = Req_ans::create(array(
            'question_id'         => $form_question22->id,
            'req_id'         => $reqstatus2->id,
            'ans_text' => 'football',
        ));

        // Req_ans5
        $reqans5 = Req_ans::create(array(
            'question_id'         => $form_question31->id,
            'req_id'         => $reqstatus3->id,
            'ans_choice' => $choice131->id,
        ));

        // Req_ans6
        $reqans6 = Req_ans::create(array(
            'question_id'         => $form_question31->id,
            'req_id'         => $reqstatus3->id,
            'ans_other' => 'Upload_file',
        ));

        $this->command->info('Create Req_ans Success!!');

        // Req_ans6
        $upload_ans1 = Upload_ans::create(array(
            'upload_id'         => $fileupload31->id,
            'ans_id'         => $reqans6->id,
            'upload_seq' => 1,
            'upload_file' => 'fileupload.jpg'
        ));


        // seed our Comment table ------------------------
        // comment1 
        $comment1 = Comment::create(array(
            'req_id'         => $reqstatus1->id,
            'comment_t1' => 'should approve',
            'comment_t2' => 'should not approve',
            'sign_time_1' => date("Y-m-d H:i:s", time()),
            'sign_time_2' => date("Y-m-d H:i:s", time()),
        ));

        // comment2
        $comment2 = Comment::create(array(
            'req_id'         => $reqstatus2->id,
        ));

        // comment3
        $comment3 = Comment::create(array(
            'req_id'         => $reqstatus3->id,
        ));

        $this->command->info('Create Comment Success!!');
    }
}
