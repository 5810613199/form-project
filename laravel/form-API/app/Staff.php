<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    //Table Name
    protected $table = 'staff';

    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    // we only want these 7 attributes able to be filled
    protected $fillable = array('u_id', 'person_id', 'fname_th', 'lname_th', 'phone_number', 'email', 'positionid');

    // Timestamps
    public $timestamps = false;

    // DEFINE RELATIONSHIPS --------------------------------------------------
    // each staff belong to user
    public function user() {
        return $this->belongsTo('App\User'); // this matches the Eloquent model
    }

    // each staff belong to operation_lv
    public function operation_lv() {
        return $this->belongsTo('App\Operation_lv', 'manage_lv'); // this matches the Eloquent model
    }
}
