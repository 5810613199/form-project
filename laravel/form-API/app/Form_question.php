<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form_question extends Model
{
    //Table Name
    protected $table = 'form_question';

    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    // we only want these 7 attributes able to be filled
    protected $fillable = array('form_id', 'question_seq', 'question_type', 'question_detail', 'n_choice', 'req_ans', 'other');

    // Timestamps
    public $timestamps = false;

    // DEFINE RELATIONSHIPS --------------------------------------------------
    // each form_question belong to form
    public function form() {
        return $this->belongsTo('App\Form', 'id'); // this matches the Eloquent model
    }

    // each form_question HAS many req_ans
    public function req_ans() {
        return $this->hasMany('App\Req_ans'); // this matches the Eloquent model
    }

    // each form_question HAS many question_choice
    public function question_choice() {
        return $this->hasMany('App\Question_choice', 'question_id'); // this matches the Eloquent model
    }

    // each form_question HAS many upload_q
    public function upload_q() {
        return $this->hasMany('App\Upload_q', 'question_id'); // this matches the Eloquent model
    }
}
