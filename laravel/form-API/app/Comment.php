<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //Table Name
    protected $table = 'comment';

    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    // we only want these 6 attributes able to be filled
    protected $fillable = array('req_id', 'comment_t1', 'comment_t2', 'comment_t3', 'comment_t4', 'comment_t5', 'sign_time_1', 'sign_time_2', 'sign_time_3', 'sign_time_4', 'sign_time_5');

    // Timestamps
    public $timestamps = false;

    // DEFINE RELATIONSHIPS --------------------------------------------------
    // each comment belong to req_status
    public function req_status() {
        return $this->belongsTo('App\Req_status', 'id'); // this matches the Eloquent model
    }

}
