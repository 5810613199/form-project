<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Carbon\Carbon;

class Req_status extends Model
{
    //Table Name
    protected $table = 'req_status';

    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    // we only want these 10 attributes able to be filled
    protected $fillable = array('form_id', 'student_id', 'sign_t1_s', 'sign_t2_s', 'sign_t3_s', 'sign_t4_s', 'sign_t5_s', 'created_at', 'updated_at', 'status');

    // Timestamps
    public $timestamps = true;

    // DEFINE RELATIONSHIPS --------------------------------------------------
    // each req_status belong to form
    public function form() {
        return $this->belongsTo('App\Form'); // this matches the Eloquent model
    }

    // each req_status belong to student
    public function student() {
        return $this->belongsTo('App\Student'); // this matches the Eloquent model
    }

    // each req_status HAS many req_ans
    public function req_ans() {
        return $this->hasMany('App\Req_ans', 'req_id'); // this matches the Eloquent model
    }

    // each req_status HAS many comment
    public function comment() {
        return $this->hasMany('App\Comment', 'req_id'); // this matches the Eloquent model
    }

    // each req_status belong to user_st1
    public function st1() {
        return $this->belongsTo('App\User', 'sign_t1'); // this matches the Eloquent model
    }

    // each req_status belong to user_st2
    public function st2() {
        return $this->belongsTo('App\User', 'sign_t2'); // this matches the Eloquent model
    }

    // each req_status belong to user_st3
    public function st3() {
        return $this->belongsTo('App\User', 'sign_t3'); // this matches the Eloquent model
    }

    // each req_status belong to user_st4
    public function st4() {
        return $this->belongsTo('App\User', 'sign_t4'); // this matches the Eloquent model
    }

    // each req_status belong to user_st5
    public function st5() {
        return $this->belongsTo('App\User', 'sign_t5'); // this matches the Eloquent model
    }
}
