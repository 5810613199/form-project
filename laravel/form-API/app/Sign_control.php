<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sign_control extends Model
{
    //Table Name
    protected $table = 'sign_control';

    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    // we only want these 7 attributes able to be filled
    protected $fillable = array('form_id', 'n_sign', 'sign_t1', 'sign_t2', 'sign_t3', 'sign_t4', 'sign_t5');

    // Timestamps
    public $timestamps = false;

    // DEFINE RELATIONSHIPS --------------------------------------------------
    // each sign_control belong to form
    public function form() {
        return $this->belongsTo('App\Form', 'id'); // this matches the Eloquent model
    }

    // each sign_control belong to user_st1
    public function st1() {
        return $this->belongsTo('App\User', 'sign_t1'); // this matches the Eloquent model
    }

    // each sign_control belong to user_st2
    public function st2() {
        return $this->belongsTo('App\User', 'sign_t2'); // this matches the Eloquent model
    }

    // each sign_control belong to user_st3
    public function st3() {
        return $this->belongsTo('App\User', 'sign_t3'); // this matches the Eloquent model
    }

    // each sign_control belong to user_st4
    public function st4() {
        return $this->belongsTo('App\User', 'sign_t4'); // this matches the Eloquent model
    }

    // each sign_control belong to user_st5
    public function st5() {
        return $this->belongsTo('App\User', 'sign_t5'); // this matches the Eloquent model
    }

    
}
