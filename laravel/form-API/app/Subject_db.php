<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject_db extends Model
{
    //Table Name
    protected $table = 'subject';

    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    // we only want these 5 attributes able to be filled
    protected $fillable = array('u_id', 'sub_code', 'sub_name');

    // Timestamps
    public $timestamps = false;

    // DEFINE RELATIONSHIPS --------------------------------------------------
    // each subject_db belong to user
    public function user() {
        return $this->belongsTo('App\User', 'u_id'); // this matches the Eloquent model
    }
}
