<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upload_q extends Model
{
    //Table Name
    protected $table = 'upload_q';

    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    // we only want these 7 attributes able to be filled
    protected $fillable = array('form_id', 'question_id', 'n_file', 'file_type');

    // Timestamps
    public $timestamps = false;

    // DEFINE RELATIONSHIPS --------------------------------------------------
    // each upload_q belong to form
    public function form() {
        return $this->belongsTo('Form', 'id'); // this matches the Eloquent model
    }

    // each upload_q belong to form_question
    public function form_question() {
        return $this->belongsTo('Form_question', 'id'); // this matches the Eloquent model
    }

    // each upload_q HAS many upload_ans
    public function upload_ans() {
        return $this->hasMany('Upload_ans'); // this matches the Eloquent model
    }
}
