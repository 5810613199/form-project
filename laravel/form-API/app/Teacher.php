<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    //Table Name
    protected $table = 'teacher';

    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    // we only want these 7 attributes able to be filled
    protected $fillable = array('u_id', 'person_id', 'fname_th', 'lname_th', 'titleacafullt', 'major_th', 'reg_telephone', 'email', 'department', 'positionid');

    // Timestamps
    public $timestamps = false;

    // DEFINE RELATIONSHIPS --------------------------------------------------
    // each teacher belong to user
    public function user() {
        return $this->belongsTo('App\User', 'u_id'); // this matches the Eloquent model
    }

    // each teacher HAS many student
    public function student() {
        return $this->hasMany('App\Student', 'advisor'); // this matches the Eloquent model
    }

}
