<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\User; 
use App\Student;
use App\Teacher;
use App\Config_db;
use App\Staff;
use App\Operation_lv;
use App\Sign_control;
use App\Req_status;
use Carbon\Carbon;
use App\oauth_access_tokens;
use Lcobucci\JWT\Parser;
use Illuminate\Support\Facades\Auth; 
use Validator;

class UserController extends Controller
{

    public $successStatus = 200;
    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            return response()->json(['success' => $success], $this-> successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401);
        } 
    }
 
    // check login
    public function checkIsAuth(Request $req)
    {
        $user = Auth::user();
        $check = $user->id==$req['id'];
        if($check){
            return response()->json(true , 200);
        }
        return response()->json("message: Unauthenticated." , 200);
    }

    // check role student
    public function checkIsStudent()
    {   
        $auth_user = Auth::user();
        
        $id = $auth_user->id;
        $user = User::findOrFail($id);
        $check = $user->role == 'student';
        return response()->json($check , 200);
    }

    // check role teacher

    public function checkIsTeacher()
    {   
        $auth_user = Auth::user();
        $id = $auth_user->id;
        
        $user = User::findOrFail($id);
        $check = $user->role == 'teacher';
        return response()->json($check , 200);
    }

    // check role staff

    public function checkIsStaff()
    {   
        $auth_user = Auth::user();
        $id = $auth_user->id;

        $user = User::findOrFail($id);
        $check = $user->role == 'staff';
        return response()->json($check , 200);
    }

     // check role admin

     public function checkIsAdmin()
     {   
         $auth_user = Auth::user();
         $id = $auth_user->id;
         
         $user = User::findOrFail($id);
         $check = $user->role == 'admin';
         return response()->json($check , 200);
     }

    // set role 

    public function setConfig(Request $request)
    {   
        $auth_user = Auth::user();
        if($auth_user->role!='admin'){
            return response()->json('message: unauthenticated', 401);
        }
        
        // $admin = User::where('role', 'admin')->get();
        // return response()->json($admin, 200);

        // if(checkIsAuth())
        $id = $request['id'];
        if(Config_db::find($id)){
            $config = Config_db::findOrFail($id);
            $temp = $config->u_id;
            
            $config->u_id = $request['u_id'];
            $user = User::findOrFail($request['u_id']);
            if($user->role == 'student'){
                return response()->json(false, 200);
            }
            $sign_ctrl = Sign_control::all();
            
            foreach($sign_ctrl as $sign_item){
                if($temp==$sign_item->sign_t1){
                    $sign_item->sign_t1=$request['u_id'];
                } else if($temp==$sign_item->sign_t2){
                    $sign_item->sign_t2=$request['u_id'];
                } else if($temp==$sign_item->sign_t3){
                    $sign_item->sign_t3=$request['u_id'];
                } else if($temp==$sign_item->sign_t4){
                    $sign_item->sign_t4=$request['u_id'];
                } else if($temp==$sign_item->sign_t5){
                    $sign_item->sign_t5=$request['u_id'];
                }
                $sign_item->save();
            }
            // $item1 = Config_db::where('pos_detail', 'รองคณบดี')->get();
            // return response()->json($item1, 200);
            if($request['id']==Config_db::where('pos_detail', 'รองคณบดี')->first()->id || $request['id']==Config_db::where('pos_detail', 'หัวหน้าโครงการ TU-PINE')->first()->id){
                $req_status = Req_status::where('status', 'incomplete')->get();
                
                // return response()->json($req_status , 200);
                foreach($req_status as $req_item){
                    if($temp==$req_item->sign_t1){
                        $req_item->sign_t1=$request['u_id'];
                    } else if($temp==$req_item->sign_t2){
                        $req_item->sign_t2=$request['u_id'];
                    } else if($temp==$req_item->sign_t3){
                        $req_item->sign_t3=$request['u_id'];
                    } else if($temp==$req_item->sign_t4){
                        $req_item->sign_t4=$request['u_id'];
                    } else if($temp==$req_item->sign_t5){
                        $req_item->sign_t5=$request['u_id'];
                    }
                    $req_item->save();
                }
            }
            // $array_ex = array('รองคณบดี', 'หัวหน้าโครงการ TU-PINE');
            
            $config->save();

            return response()->json(true, 200);
        }
        // $check = $user->role == $request['role'];
        return response()->json(false , 200);
    }

    // set Operate_lv
    public function setOperate_lv(Request $request){
        $auth_user = Auth::user();
        if($auth_user->role!='admin'){
            return response()->json('message: unauthenticated', 401);
        }

        if(Staff::where('u_id', $request['u_id'])->first()){
            $opr_lv = Staff::where('u_id', $request['u_id'])->first();
            $opr_lv->manage_lv = $request['manage_lv'];
            $opr_lv->save();
            return response()->json(true, 200);
        }
        // $check = $user->role == $request['role'];
        return response()->json(false , 200);
    }
    // get Student Information
    public function getStudentInfoByuid($id){

        $auth_user = Auth::user();
        if($auth_user->role!='student'){
            return response()->json('message: unauthenticated', 401);
        }
        if($auth_user->id!=$id){
            return response()->json('message: can not access api', 200);
        }

        $User = User::findOrFail($id);
        $student = $User->student->first();
        // $student = Student::findOrFail($id);
        return response()->json($student, 200);
    }

    // get All Teacher 
    public function getAllTeacher(){
        $auth_user = Auth::user();
        if($auth_user->role!='admin'){
            return response()->json('message: unauthenticated', 401);
        }
        $teacher = Teacher::all();
        $teacher_first = $teacher->first();
        $teacher = Teacher::select('*')->where('id', '>', $teacher_first->id + 1)->get();
        foreach($teacher as $teach_item){
            $user = $teach_item->user;
        }
        return response()->json($teacher, 200);
    }

    // get all staff teacher
    public function getTeacherStaff(){
        $auth_user = Auth::user();
        if($auth_user->role!='admin'){
            return response()->json('message: unauthenticated', 401);
        }
        $teacher = Teacher::all();
        $teacher_first = $teacher->first();
        $teacher = Teacher::select('*')->where('id', '>', $teacher_first->id + 1)->get();
        foreach($teacher as $teach_item){
            $user = $teach_item->user;
        }
        $staff = Staff::all();
        foreach($staff as $staff_item){
            $user = $staff_item->user;
        }
        return response()->json(array($teacher, $staff), 200);
    }

    // Create new Config
    public function createConfig(Request $request){
        $auth_user = Auth::user();
        if($auth_user->role!='admin'){
            return response()->json('message: unauthenticated', 401);
        }

        $config = new Config_db;
        $config->u_id = $request['u_id'];
        $config->pos_detail = $request['pos_detail'];
        // return response()->json(Config_db::select('*')->where('pos_detail', $request['pos_detail'])->get(), 200);
        if(count(Config_db::select('*')->where('pos_detail', $request['pos_detail'])->get())!=0){
            return response()->json(false, 200);
        }
        $config->save();
        return response()->json(true, 200);
        
        
        // $check = $user->role == $request['role'];
    }

    // get All Operate_lv
    public function getAllOperate_lv(){
        $auth_user = Auth::user();
        if($auth_user->role!='admin'){
            return response()->json('message: unauthenticated', 401);
        }

        $opr_lv = Operation_lv::all();

        return response()->json($opr_lv, 200);
    }

    // create new Operate_lv
    public function createOpr_lv(Request $request){
        $auth_user = Auth::user();
        if($auth_user->role!='admin'){
            return response()->json('message: unauthenticated', 401);
        }

        $opr_lv = new Operation_lv;
        $opr_lv->level_operate = $request['level_operate'];
        $opr_lv->name_of_position = $request['name_of_position'];
        // return response()->json(Config_db::select('*')->where('pos_detail', $request['pos_detail'])->get(), 200);
        if(count(Operation_lv::select('*')->where('name_of_position', $request['name_of_position'])->get())!=0){
            return response()->json(false, 200);
        }
        if(count(Operation_lv::select('*')->where('level_operate', $request['level_operate'])->get())!=0){
            return response()->json(false, 200);
        }
        $opr_lv->save();
        return response()->json(true, 200);
    }

    // get setable config
    public function getsetConfig(){
        $auth_user = Auth::user();
        if($auth_user->role!='admin'){
            return response()->json('message: unauthenticated', 401);
        }

        $config = Config_db::all();
        $con_first = $config->first();

        $config = Config_db::select('*')->where('id', '>', $con_first->id + 1)->get();

        return response()->json($config, 200);
    }

    // get Student Information
    public function getStudentInfoBystdid($id){
        // $User = User::findOrFail($id);
        // $student = $User->student->first();
        $student = Student::findOrFail($id);
        return response()->json($student, 200);
    }

    // get Teacher Information
    public function getTeacherInfo($id){
        $User = User::findOrFail($id);
        $Teacher = $User->teacher->first();
        return response()->json($Teacher, 200);
    }

    // get Staff Information
    public function getStaffInfo($id){
        $User = User::findOrFail($id);
        $Staff = $User->staff->first();
        return response()->json($Staff, 200);
    }

    //get Role By id
    public function getRoleByIdTeacher($id){
        $auth_user = Auth::user();
        if($auth_user->role!='teacher'){
            return response()->json('message: unauthenticated', 401);
        }

        $user = User::findOrFail($id);
        $config_db = $user->config_db;
        // $user = User::select('role')->where('id', $id)->get();
        return response()->json($user, 200);
    }

    public function logoutApi(Request $request)
    { 
        $value = $request->bearerToken();
        $id = (new Parser())->parse($value)->getHeader('jti');
        $token = $request->user()->tokens->find($id);
        $token->revoke();

        return response()->json(true, 200);
    }


    public function deleteConfig($id)
    {
        $auth_user = Auth::user();
        if($auth_user->role!='admin'){
            return response()->json('message: unauthenticated', 401);
        }   
        // Get article
        $config = Config_db::findOrFail($id);

        if($config->delete()){
            return response()->json(true, 200);
        }
        return response()->json(false, 200);
    }
}
