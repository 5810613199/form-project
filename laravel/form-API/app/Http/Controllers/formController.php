<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Form;
use App\User;
use App\Http\Resources\FormResource as FormResource;
use App\Form_question;
use App\Question_choice;
use App\Sign_control;
use App\Upload_q;
use Illuminate\Support\Facades\Auth; 
use Validator;

class formController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get form
        $forms = Form::orderBy('form_pos')->get();
        
        // Return collection of form as a resource
        return response()->json($forms, 200);
    }

    //Check can manage
    private function checkCanManage($u_id){
        $user = User::findOrFail($u_id);
        $staff = $user->staff->first();
        $manage_lv = $staff->manage_lv;
        if($manage_lv>0){
            return response()->json(true, 200);
        }
        return response()->json(false, 200);
    }

    /**
     * Update Form
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateForm(Request $request)
    {
        $auth_user = Auth::user();
        if($auth_user->role!='staff'){
            return response()->json('message: unauthenticated', 401);
        }
        if(!$this->checkCanManage($auth_user->id)){
            return response()->json('message: can not manage', 200);
        }

        $form = $request->isMethod('put') ? Form::findOrFail($request->id) : new Form;
        $form->id = $request->input('id');
        $form->form_name = $request->input('form_name');
        $form->mod_by = $request->input('mod_by');
        $form->description = $request->input('description');
        $form->form_pos = $request->input('form_pos');
        $form->save();

        $questionarr = $request->input('form_question');
        // return response()->json($questionarr, 200);
        $check_form = Form::findOrFail($request->id);
        $check_question = $check_form->form_question;
        foreach($check_question as $check_q){
            $check_choice=$check_q->question_choice;
            $check_exist_q = 0;
            foreach($questionarr as $question){
                if(!array_key_exists('id', $question)){
                    continue;
                }
                if($check_q->id == $question['id']){
                    $check_exist_q = 1;
                    foreach($check_choice as $check_c){
                        $check_exist_c = 0;
                        foreach($question['question_choice'] as $choice){
                            if(!array_key_exists('id', $choice)){
                                continue;
                            }
                            if($choice['id']==$check_c->id){
                                $check_exist_c =1;
                                break;
                            }
                        }
                        if($check_exist_c==0){
                            $del_choice = Question_choice::findOrFail($check_c->id);
                            // return response()->json($del_choice, 200);
                            $del_choice->delete();
                        }
                    }
                    break;
                }
            }
            if($check_exist_q==0){
                $del_question = Form_question::findOrFail($check_q->id);
                // return response()->json($check_q, 200);
                $del_question->delete();
            }

            foreach($check_choice as $check_c){
                $check_exist = 0;
                foreach($questionarr as $question){
                    if(!array_key_exists('id', $question)){
                        continue;
                    }
                    if($question['id']==$check_q->id){

                    }
                }
            }
            
        }
        
        foreach($questionarr as $question){
            if((array_key_exists('id', $question))){
                $quest_item = Form_question::findOrFail($question['id']);
                $quest_item->form_id = $form->id;
                $quest_item->question_seq = $question['question_seq'];
                $quest_item->question_type = $question['question_type'];
                $quest_item->question_detail = $question['question_detail'];
                $quest_item->n_choice = $question['n_choice'];
                $quest_item->req_ans = $question['req_ans'];
                $quest_item->other = $question['other'];
                $quest_item->save();
    
                foreach($question['question_choice'] as $choice){
                    if((array_key_exists('id', $choice))){
                        $choice_item = Question_choice::findOrFail($choice['id']);
                        $choice_item->question_id = $quest_item->id;
                        $choice_item->choice_detail = $choice['choice_detail'];
                        $choice_item->choice_seq = $choice['choice_seq'];
                        $choice_item->save();
                    } else {
                        $choice_item = new Question_choice;
                        $choice_item->question_id = $quest_item->id;
                        $choice_item->choice_detail = $choice['choice_detail'];
                        $choice_item->choice_seq = $choice['choice_seq'];
                        $choice_item->save();
                    }
                }
                foreach($question['upload_q'] as $upload){
                    if((array_key_exists('id', $upload))){

                        $upload_item = Upload_q::findOrFail($upload['id']);
                        $upload_item->form_id = $form->id;
                        $upload_item->question_id = $quest_item->id;
                        $upload_item->n_file = $upload['n_file'];
                        $upload_item->file_type = $upload['file_type'];
                        $upload_item->save();
                    } 
                }
            } else {
                $quest_item = new Form_question;
                $quest_item->form_id = $form->id;
                $quest_item->question_seq = $question['question_seq'];
                $quest_item->question_type = $question['question_type'];
                $quest_item->question_detail = $question['question_detail'];
                $quest_item->n_choice = $question['n_choice'];
                $quest_item->req_ans = $question['req_ans'];
                $quest_item->other = $question['other'];
                $quest_item->save();

                foreach($question['question_choice'] as $choice){
                    $choice_item = new Question_choice;
                    $choice_item->question_id = $quest_item->id;
                    $choice_item->choice_detail = $choice['choice_detail'];
                    $choice_item->choice_seq = $choice['choice_seq'];
                    $choice_item->save();
                }
                foreach($question['upload_q'] as $upload){
                    // return response()->json($upload, 200);
                    $upload_item = new Upload_q;
                    $upload_item->form_id = $form->id;
                    $upload_item->question_id = $quest_item->id;
                    $upload_item->n_file = $upload['n_file'];
                    $upload_item->file_type = $upload['file_type'];
                    $upload_item->save();
                }
            }
        }
        $signcontrols = $request->input('sign_control');
        foreach($signcontrols as $signcontrol){
            $sign_item = Sign_control::findOrFail($signcontrol['id']);
            $sign_item->form_id = $form->id;
            $sign_item->n_sign = $signcontrol['n_sign'];
            $sign_item->sign_t1 = $signcontrol['sign_t1'];
            $sign_item->sign_t2 = $signcontrol['sign_t2'];
            $sign_item->sign_t3 = $signcontrol['sign_t3'];
            $sign_item->sign_t4 = $signcontrol['sign_t4'];
            $sign_item->sign_t5 = $signcontrol['sign_t5'];
            $sign_item->save();
        }

        return response()->json(true, 200);
    }

    // Create Form
    public function createForm(Request $request)
    {
        $auth_user = Auth::user();
        if($auth_user->role!='staff'){
            return response()->json('message: unauthenticated', 401);
        }
        if(!$this->checkCanManage($auth_user->id)){
            return response()->json('message: can not manage', 200);
        }

        if($request->isMethod('post')){
            $form = new Form;
            // $form->id = $request->input('id');
            $form->form_name = $request->input('form_name');
            $form->mod_by = $request->input('mod_by');
            $form->create_by = $request->input('create_by');
            $form->description = $request->input('description');
            $max_pos = Form::orderBy('form_pos', 'desc')->get()->first()->form_pos;
            $form->form_pos = $max_pos+1;
            $form->save();

            $questionarr = $request->input('form_question');
            // return response()->json($questionarr,200);
            foreach($questionarr as $question){
                // return response()->json($question['id'], 200);
                $quest_item = new Form_question;
                $quest_item->form_id = $form->id;
                $quest_item->question_seq = $question['question_seq'];
                $quest_item->question_type = $question['question_type'];
                $quest_item->question_detail = $question['question_detail'];
                $quest_item->n_choice = $question['n_choice'];
                $quest_item->req_ans = $question['req_ans'];
                $quest_item->other = $question['other'];
                $quest_item->save();

                foreach($question['question_choice'] as $choice){
                    $choice_item = new Question_choice;
                    $choice_item->question_id = $quest_item->id;
                    $choice_item->choice_detail = $choice['choice_detail'];
                    $choice_item->choice_seq = $choice['choice_seq'];
                    $choice_item->save();
                }
                foreach($question['upload_q'] as $upload){
                    // return response()->json($upload, 200);
                    $upload_item = new Upload_q;
                    $upload_item->form_id = $form->id;
                    $upload_item->question_id = $quest_item->id;
                    $upload_item->n_file = $upload['n_file'];
                    $upload_item->file_type = $upload['file_type'];
                    $upload_item->save();
                }
            }
            $signcontrols = $request->input('sign_control');
            foreach($signcontrols as $signcontrol){
                $sign_item = new Sign_control;
                $sign_item->form_id = $form->id;
                $sign_item->n_sign = $signcontrol['n_sign'];
                $sign_item->sign_t1 = $signcontrol['sign_t1'];
                $sign_item->sign_t2 = $signcontrol['sign_t2'];
                $sign_item->sign_t3 = $signcontrol['sign_t3'];
                $sign_item->sign_t4 = $signcontrol['sign_t4'];
                $sign_item->sign_t5 = $signcontrol['sign_t5'];
                $sign_item->save();
            }
        }
        return response()->json(true, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $form = Form::find($id);
        return view('form.show')->with('form', $form);
    }

    // get 1 form
    public function getFormToShow($id)
    {
        $form = Form::findOrFail($id);

        $questions = $form->form_question;

        $sign_control = $form->sign_control; 

        foreach($questions as $question){
            $choice = $question->question_choice;
            $upload_file = $question->upload_q;
        }
 
        return response()->json($form, 200);
    }

    // get Question by form_id
    public function getQuestions($id)
    {
        $auth_user = Auth::user();
        if($auth_user->role!='student'){
            return response()->json('message: unauthenticated', 401);
        }

        $form = Form::findOrFail($id);

        $questions = $form->form_question;
        foreach($questions as $question){
            $choice = $question->question_choice;
            $upload_file = $question->upload_q;
        }

        return response()->json($questions, 200);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $auth_user = Auth::user();
        if($auth_user->role!='staff'){
            return response()->json('message: unauthenticated', 401);
        }
        if(!$this->checkCanManage($auth_user->id)){
            return response()->json('message: can not manage', 200);
        }
        // Get article
        $form = Form::findOrFail($id);
        $change_pos_form = Form::where('form_pos', '>', $form->form_pos)->get();
        if($form->delete()){
            foreach($change_pos_form as $form_item){
                $form_item->form_pos = $form_item->form_pos-1;
                $form_item->save();
            }
            return response()->json(true, 200);
        }
        return response()->json(false, 200);
    }

    public function updateFormName(Request $request){
        $auth_user = Auth::user();
        if($auth_user->role!='staff'){
            return response()->json('message: unauthenticated', 401);
        }
        if(!$this->checkCanManage($auth_user->id)){
            return response()->json('message: can not manage', 200);
        }

        if($request->isMethod('put')){
            $form = Form::findOrFail($request->form_id);
            $form->form_name = $request->input('form_name');
            $form->mod_by = $request->input('mod_by');
            $form->save();
            return response()->json(true, 200);
        }
        return response()->json(false, 200);
    }

    public function updatePosition(Request $request){
        $auth_user = Auth::user();
        if($auth_user->role!='staff'){
            return response()->json('message: unauthenticated', 401);
        }
        if(!$this->checkCanManage($auth_user->id)){
            return response()->json('message: can not manage', 200);
        }

        if($request->isMethod('put')){
            $form_1 = Form::findOrFail($request->previous_form_id);
            $form_2 = Form::findOrFail($request->current_form_id);
            $temp_pos = $form_1->form_pos;
            $form_1->form_pos = $form_2->form_pos;
            $form_1->save();
            $form_2->form_pos = $temp_pos;
            $form_2->save();
            return response()->json(true, 200);
        }
        return response()->json(false, 200);
    }
}
