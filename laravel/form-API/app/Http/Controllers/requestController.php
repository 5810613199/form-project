<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Req_ans;
use App\Req_status;
use App\Comment;
use App\Form;
use App\Student;
use App\Sign_control;
use App\User;
use App\Teacher;
use App\Staff;
use App\question_choice;
use App\Operation_lv;
use App\Config_db;
use App\Subject_db;
use App\Upload_ans;
use App\Http\Resources\RequestResource as ReqRsc;
use Illuminate\Support\Facades\Auth; 
use Validator;
use Mail;


class requestController extends Controller
{ 
    // get All Request in database
    public function getAllRequest($id)
    {
        $auth_user = Auth::user(); 
        if($auth_user->id!=$id){
            return response()->json('message: can not access api', 200);
        }
        $req_arr=(array) null;
        $users = User::findOrFail($id);
        if($users->role=='student'){
            $user = $users->student->first();
            $reqs = $user->req_status;
            // return response()->json($reqs, 200);
        }if($users->role=='teacher' || $users->role=='staff'){
            // return response()->json($users, 200);
            $sign_control_t1 = $users->sign_t1;
            $sign_control_t2 = $users->sign_t2;
            $sign_control_t3 = $users->sign_t3;
            $sign_control_t4 = $users->sign_t4;
            $sign_control_t5 = $users->sign_t5;
            // return response()->json($sign_control_t1, 200);
            foreach($sign_control_t1 as $sign_item){
            // return response()->json($sign_item->sign_t4_s, 200);
                if(!is_null($sign_item->sign_t1_s)){
                    array_push($req_arr, $sign_item);
                }
            }
            foreach($sign_control_t2 as $sign_item){
                if(!is_null($sign_item->sign_t2_s)){
                    array_push($req_arr, $sign_item);
                }
            }
            foreach($sign_control_t3 as $sign_item){
                if(!is_null($sign_item->sign_t3_s)){
                    array_push($req_arr, $sign_item);
                }
            }
            foreach($sign_control_t4 as $sign_item){
                if(!is_null($sign_item->sign_t4_s)){
                    array_push($req_arr, $sign_item);
                }
            }
            foreach($sign_control_t5 as $sign_item){
                if(!is_null($sign_item->sign_t5_s)){
                    array_push($req_arr, $sign_item);
                }
            }
            // return response()->json($req_arr, 200);
            $reqs=$req_arr;
        }
        // return new ReqRsc($sign_control);
        // $reqs = Req_status::all();
        $progressarr = array();
        $i=0;
        $n_sign=0;
        // return response()->json($reqs, 200);
        foreach($reqs as $req){
            $progress = 0;
            // $newDateFormat = $req->created_at->format('d/m/Y');
            // $newDateFormat2 = strtotime($newDateFormat);
            // $req->created_at = $newDateFormat2;
            // return response()->json($newDateFormat2, 200);

            $form = $req->form;
            // return response()->json($req, 200);
            $student = $req->student;
            $sign_control=$form->sign_control->first();
            // return response()->json($sign_control, 200);
            $n_sign=$sign_control->n_sign;
            $req->sign_t1_s>0 ? $progress+=1: $progress=$progress;
            $req->sign_t2_s>0 ? $progress+=1: $progress=$progress;
            $req->sign_t3_s>0 ? $progress+=1: $progress=$progress;
            $req->sign_t4_s>0 ? $progress+=1: $progress=$progress;
            $req->sign_t5_s>0 ? $progress+=1: $progress=$progress;
            $progress=$progress/$n_sign;
            $progressarr[$i]=round($progress, 2)*100;
            $i+=1;
        }
        return response()->json(array($reqs, $progressarr), 200);
    }

    // get Request by req_id
    public function getRequestById($id){

        $auth_user = Auth::user(); 
        $check_valid_user = 0;

        $req_s = Req_status::findOrFail($id);

        $std = $req_s->student;
        $user_std = $std->user;
        
        if($auth_user->id==$user_std->id)
            $check_valid_user=1;
        if(!is_null($req_s->st1)){
            $st1 = $req_s->st1;
            if($auth_user->id==$st1->id){
                $check_valid_user=1;
            }
        }
        if(!is_null($req_s->st2)){
            $st2 = $req_s->st2;
            if($auth_user->id==$st2->id){
                $check_valid_user=1;
            }
        }
        if(!is_null($req_s->st3)){
            $st3 = $req_s->st3;
            if($auth_user->id==$st3->id){
                $check_valid_user=1;
            }
        }
        if(!is_null($req_s->st4)){
            $st4 = $req_s->st4;
            if($auth_user->id==$st4->id){
                $check_valid_user=1;
            }
        }
        if(!is_null($req_s->st5)){
            $st5 = $req_s->st5;
            if($auth_user->id==$st5->id){
                $check_valid_user=1;
            }
        }
        
        if($check_valid_user!=1){
            return response()->json('message: unauthenticated', 401);
        }
        $form = $req_s->form;
        $req_ans = $req_s->req_ans;
        foreach($req_ans as $req_item){
            $choice = $req_item->question_choice;
        }
        $comment = $req_s->comment;
        if(!is_null($req_s->sign_t1)){
            $sign_t1 = $req_s->st1;
            if($sign_t1->role=='staff'){
                $staff = $sign_t1->staff;
            } else if($sign_t1->role=='teacher'){
                $teacher = $sign_t1->teacher;
            }
        }
        if(!is_null($req_s->sign_t2)){
            $sign_t2 = $req_s->st2;
            if($sign_t2->role=='staff'){
                $staff = $sign_t2->staff;
            } else if($sign_t2->role=='teacher'){
                $teacher = $sign_t2->teacher;
            }
        }
        if(!is_null($req_s->sign_t3)){
            $sign_t3 = $req_s->st3;
            if($sign_t3->role=='staff'){
                $staff = $sign_t3->staff;
            } else if($sign_t3->role=='teacher'){
                $teacher = $sign_t3->teacher;
            }
        }
        if(!is_null($req_s->sign_t4)){
            $sign_t4 = $req_s->st4;
            if($sign_t4->role=='staff'){
                $staff = $sign_t4->staff;
            } else if($sign_t4->role=='teacher'){
                $teacher = $sign_t4->teacher;
            }
        }
        if(!is_null($req_s->sign_t5)){
            $sign_t5 = $req_s->st5;
            if($sign_t5->role=='staff'){
                $staff = $sign_t5->staff;
            } else if($sign_t5->role=='teacher'){
                $teacher = $sign_t5->teacher;
            }
        }
        $sign_control = $req_s->form->sign_control;
        return response()->json($req_s, 200);
    }

    // get Comment
    public function getComment($id)
    {
        $req = Req_status::findOrFail($id);
        return response()->json($req->comment, 200);
        // $req = $comment->req_status;
    }

    // get Ans
    public function getAnswerForm($req_id)
    {
        $auth_user = Auth::user(); 
        $check_valid_user = 0;

        $req_s = Req_status::findOrFail($req_id);
        $req_ans = $req_s->req_ans;
        
        $std = $req_s->student;
        $user_std = $std->user;
        
        if($auth_user->id==$user_std->id)
            $check_valid_user=1;
        if(!is_null($req_s->st1)){
            $st1 = $req_s->st1;
            if($auth_user->id==$st1->id){
                $check_valid_user=1;
            }
        }
        if(!is_null($req_s->st2)){
            $st2 = $req_s->st2;
            if($auth_user->id==$st2->id){
                $check_valid_user=1;
            }
        }
        if(!is_null($req_s->st3)){
            $st3 = $req_s->st3;
            if($auth_user->id==$st3->id){
                $check_valid_user=1;
            }
        }
        if(!is_null($req_s->st4)){
            $st4 = $req_s->st4;
            if($auth_user->id==$st4->id){
                $check_valid_user=1;
            }
        }
        if(!is_null($req_s->st5)){
            $st5 = $req_s->st5;
            if($auth_user->id==$st5->id){
                $check_valid_user=1;
            }
        }
        
        if($check_valid_user!=1){
            return response()->json('message: unauthenticated', 401);
        }
        
        // return response()->json($req_ans, 200);
        foreach($req_ans as $req_ans_item){
            $choice = $req_ans_item->question_choice;
            $upload_ans = $req_ans_item->upload_ans;
            // return response()->json($choice);
        }
        $form = $req_s->form->form_question;

        return response()->json($req_s, 200);
    }

    // submitForm(Request)
    public function submitForm(Request $request)
    {
        $auth_user = Auth::user();
        $id = Student::findOrFail($request['student_id'])->u_id;
        // return response()->json($id, 200);

        if($auth_user->id!=$id){
            return response()->json('message: can not access api', 200);
        }

        if($request->isMethod('post')){
            $req_s = new Req_status;
            // $form->id = $request->input('id');
            $req_s->form_id = $request->input('form_id');
            $req_s->student_id = $request->input('student_id');
            $req_s->semester = $request->input('semester');
            
            $d_sub_teacher = Config_db::where('pos_detail', 'อาจารย์ประจำวิชา')->get()->first();
            $advisor_teacher = Config_db::where('pos_detail', 'อาจารย์ที่ปรึกษา')->get()->first();

            $form = Form::findOrFail($request->input('form_id'));
            $sign_control = $form->sign_control->first();

            // return response()->json($sign_control, 200);
            // if($request->subject){
            //     $subject = explode('/', $request->input('subject'));
            //     $sub_code = $subject[0];
            //     $section = $subject[1];
            //     $subject_db = Subject_db::where([
            //         ['sub_code', $sub_code],
            //         ['section', $section]
            //     ])->get()->first();
            // }

            // return response()->json($sign_control->sign_t1, 200);
            $student = Student::findOrFail($request->student_id);
            
            // return response()->json(array($request->input('sign_t2'), $d_sub_teacher->u_id), 200);
            if($sign_control->sign_t1){
                $req_s->sign_t1 = $sign_control->sign_t1;
            }
            if($sign_control->sign_t2){
                $req_s->sign_t2 = $sign_control->sign_t2;
                if($sign_control->sign_t2==$d_sub_teacher->u_id){
                    if($request->subject!=null){
                        $req_s->sign_t2 = $request->subject;
                    } else {
                        return response()->json(false, 200);
                    }
                } else if($sign_control->sign_t2==$advisor_teacher->u_id){
                    $req_s->sign_t2 = $student->advisor;
                }
            }
            if($sign_control->sign_t3){
                $req_s->sign_t3 = $sign_control->sign_t3;
                if($sign_control->sign_t3==$d_sub_teacher->u_id){
                    if($request->subject!=null){
                        $req_s->sign_t3 = $request->subject;
                    } else {
                        return response()->json(false, 200);
                    }
                } else if($sign_control->sign_t3==$advisor_teacher->u_id){
                    $req_s->sign_t3 = $student->advisor;
                }
            }
            if($sign_control->sign_t4){
                $req_s->sign_t4 = $sign_control->sign_t4;
                if($sign_control->sign_t4==$d_sub_teacher->u_id){
                    if($request->subject!=null){
                        $req_s->sign_t4 = $request->subject;
                    } else {
                        return response()->json(false, 200);
                    }
                } else if($sign_control->sign_t4==$advisor_teacher->u_id){
                    $req_s->sign_t4 = $student->advisor;
                }
            }
            if($sign_control->sign_t5){
                $req_s->sign_t5 = $sign_control->sign_t5;
                if($sign_control->sign_t5==$d_sub_teacher->u_id){
                    if($request->subject!=null){
                        $req_s->sign_t5 = $request->subject;
                    } else {
                        return response()->json(false, 200);
                    }
                } else if($sign_control->sign_t5==$advisor_teacher->u_id){
                    $req_s->sign_t5 = $student->advisor;
                }
            }
            $req_s->sign_t1_s = 0;
            $req_s->sign_t2_s = null;
            $req_s->sign_t3_s = null;
            $req_s->sign_t4_s = null;
            $req_s->sign_t5_s = null;
            $req_s->status = 'incomplete';

            $req_s->save();
            $ans_arr = $request->input('req_ans');

            $staff = $req_s->st1;
            $role = $staff->role;
            $email = $staff->email;
            $this->sendmail($email, $role, $req_s->id, 1);

            // return response()->json($staff, 200);

            $mime_type = array('video/3gpp2'                                            => '3g2',
            'video/3gp'                                                                 => '3gp',
            'video/3gpp'                                                                => '3gp',
            'application/x-compressed'                                                  => '7zip',
            'audio/x-acc'                                                               => 'aac',
            'audio/ac3'                                                                 => 'ac3',
            'application/postscript'                                                    => 'ai',
            'audio/x-aiff'                                                              => 'aif',
            'audio/aiff'                                                                => 'aif',
            'audio/x-au'                                                                => 'au',
            'video/x-msvideo'                                                           => 'avi',
            'video/msvideo'                                                             => 'avi',
            'video/avi'                                                                 => 'avi',
            'application/x-troff-msvideo'                                               => 'avi',
            'application/macbinary'                                                     => 'bin',
            'application/mac-binary'                                                    => 'bin',
            'application/x-binary'                                                      => 'bin',
            'application/x-macbinary'                                                   => 'bin',
            'image/bmp'                                                                 => 'bmp',
            'image/x-bmp'                                                               => 'bmp',
            'image/x-bitmap'                                                            => 'bmp',
            'image/x-xbitmap'                                                           => 'bmp',
            'image/x-win-bitmap'                                                        => 'bmp',
            'image/x-windows-bmp'                                                       => 'bmp',
            'image/ms-bmp'                                                              => 'bmp',
            'image/x-ms-bmp'                                                            => 'bmp',
            'application/bmp'                                                           => 'bmp',
            'application/x-bmp'                                                         => 'bmp',
            'application/x-win-bitmap'                                                  => 'bmp',
            'application/cdr'                                                           => 'cdr',
            'application/coreldraw'                                                     => 'cdr',
            'application/x-cdr'                                                         => 'cdr',
            'application/x-coreldraw'                                                   => 'cdr',
            'image/cdr'                                                                 => 'cdr',
            'image/x-cdr'                                                               => 'cdr',
            'zz-application/zz-winassoc-cdr'                                            => 'cdr',
            'application/mac-compactpro'                                                => 'cpt',
            'application/pkix-crl'                                                      => 'crl',
            'application/pkcs-crl'                                                      => 'crl',
            'application/x-x509-ca-cert'                                                => 'crt',
            'application/pkix-cert'                                                     => 'crt',
            'text/css'                                                                  => 'css',
            'text/x-comma-separated-values'                                             => 'csv',
            'text/comma-separated-values'                                               => 'csv',
            'application/vnd.msexcel'                                                   => 'csv',
            'application/x-director'                                                    => 'dcr',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document'   => 'docx',
            'application/x-dvi'                                                         => 'dvi',
            'message/rfc822'                                                            => 'eml',
            'application/x-msdownload'                                                  => 'exe',
            'video/x-f4v'                                                               => 'f4v',
            'audio/x-flac'                                                              => 'flac',
            'video/x-flv'                                                               => 'flv',
            'image/gif'                                                                 => 'gif',
            'application/gpg-keys'                                                      => 'gpg',
            'application/x-gtar'                                                        => 'gtar',
            'application/x-gzip'                                                        => 'gzip',
            'application/mac-binhex40'                                                  => 'hqx',
            'application/mac-binhex'                                                    => 'hqx',
            'application/x-binhex40'                                                    => 'hqx',
            'application/x-mac-binhex40'                                                => 'hqx',
            'text/html'                                                                 => 'html',
            'image/x-icon'                                                              => 'ico',
            'image/x-ico'                                                               => 'ico',
            'image/vnd.microsoft.icon'                                                  => 'ico',
            'text/calendar'                                                             => 'ics',
            'application/java-archive'                                                  => 'jar',
            'application/x-java-application'                                            => 'jar',
            'application/x-jar'                                                         => 'jar',
            'image/jp2'                                                                 => 'jp2',
            'video/mj2'                                                                 => 'jp2',
            'image/jpx'                                                                 => 'jp2',
            'image/jpm'                                                                 => 'jp2',
            'image/jpeg'                                                                => 'jpeg',
            'image/pjpeg'                                                               => 'jpeg',
            'application/x-javascript'                                                  => 'js',
            'application/json'                                                          => 'json',
            'text/json'                                                                 => 'json',
            'application/vnd.google-earth.kml+xml'                                      => 'kml',
            'application/vnd.google-earth.kmz'                                          => 'kmz',
            'text/x-log'                                                                => 'log',
            'audio/x-m4a'                                                               => 'm4a',
            'application/vnd.mpegurl'                                                   => 'm4u',
            'audio/midi'                                                                => 'mid',
            'application/vnd.mif'                                                       => 'mif',
            'video/quicktime'                                                           => 'mov',
            'video/x-sgi-movie'                                                         => 'movie',
            'audio/mpeg'                                                                => 'mp3',
            'audio/mpg'                                                                 => 'mp3',
            'audio/mpeg3'                                                               => 'mp3',
            'audio/mp3'                                                                 => 'mp3',
            'video/mp4'                                                                 => 'mp4',
            'video/mpeg'                                                                => 'mpeg',
            'application/oda'                                                           => 'oda',
            'audio/ogg'                                                                 => 'ogg',
            'video/ogg'                                                                 => 'ogg',
            'application/ogg'                                                           => 'ogg',
            'application/x-pkcs10'                                                      => 'p10',
            'application/pkcs10'                                                        => 'p10',
            'application/x-pkcs12'                                                      => 'p12',
            'application/x-pkcs7-signature'                                             => 'p7a',
            'application/pkcs7-mime'                                                    => 'p7c',
            'application/x-pkcs7-mime'                                                  => 'p7c',
            'application/x-pkcs7-certreqresp'                                           => 'p7r',
            'application/pkcs7-signature'                                               => 'p7s',
            'application/pdf'                                                           => 'pdf',
            'application/octet-stream'                                                  => 'psd',
            'application/x-x509-user-cert'                                              => 'pem',
            'application/x-pem-file'                                                    => 'pem',
            'application/pgp'                                                           => 'pgp',
            'application/x-httpd-php'                                                   => 'php',
            'application/php'                                                           => 'php',
            'application/x-php'                                                         => 'php',
            'text/php'                                                                  => 'php',
            'text/x-php'                                                                => 'php',
            'application/x-httpd-php-source'                                            => 'php',
            'image/png'                                                                 => 'png',
            'image/x-png'                                                               => 'png',
            'application/powerpoint'                                                    => 'ppt',
            'application/vnd.ms-powerpoint'                                             => 'ppt',
            'application/vnd.ms-office'                                                 => 'ppt',
            'application/msword'                                                        => 'ppt',
            'application/vnd.openxmlformats-officedocument.presentationml.presentation' => 'pptx',
            'application/x-photoshop'                                                   => 'psd',
            'image/vnd.adobe.photoshop'                                                 => 'psd',
            'audio/x-realaudio'                                                         => 'ra',
            'audio/x-pn-realaudio'                                                      => 'ram',
            'application/x-rar'                                                         => 'rar',
            'application/rar'                                                           => 'rar',
            'application/x-rar-compressed'                                              => 'rar',
            'audio/x-pn-realaudio-plugin'                                               => 'rpm',
            'application/x-pkcs7'                                                       => 'rsa',
            'text/rtf'                                                                  => 'rtf',
            'text/richtext'                                                             => 'rtx',
            'video/vnd.rn-realvideo'                                                    => 'rv',
            'application/x-stuffit'                                                     => 'sit',
            'application/smil'                                                          => 'smil',
            'text/srt'                                                                  => 'srt',
            'image/svg+xml'                                                             => 'svg',
            'application/x-shockwave-flash'                                             => 'swf',
            'application/x-tar'                                                         => 'tar',
            'application/x-gzip-compressed'                                             => 'tgz',
            'image/tiff'                                                                => 'tiff',
            'text/plain'                                                                => 'txt',
            'text/x-vcard'                                                              => 'vcf',
            'application/videolan'                                                      => 'vlc',
            'text/vtt'                                                                  => 'vtt',
            'audio/x-wav'                                                               => 'wav',
            'audio/wave'                                                                => 'wav',
            'audio/wav'                                                                 => 'wav',
            'application/wbxml'                                                         => 'wbxml',
            'video/webm'                                                                => 'webm',
            'audio/x-ms-wma'                                                            => 'wma',
            'application/wmlc'                                                          => 'wmlc',
            'video/x-ms-wmv'                                                            => 'wmv',
            'video/x-ms-asf'                                                            => 'wmv',
            'application/xhtml+xml'                                                     => 'xhtml',
            'application/excel'                                                         => 'xl',
            'application/msexcel'                                                       => 'xls',
            'application/x-msexcel'                                                     => 'xls',
            'application/x-ms-excel'                                                    => 'xls',
            'application/x-excel'                                                       => 'xls',
            'application/x-dos_ms_excel'                                                => 'xls',
            'application/xls'                                                           => 'xls',
            'application/x-xls'                                                         => 'xls',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'         => 'xlsx',
            'application/vnd.ms-excel'                                                  => 'xlsx',
            'application/xml'                                                           => 'xml',
            'text/xml'                                                                  => 'xml',
            'text/xsl'                                                                  => 'xsl',
            'application/xspf+xml'                                                      => 'xspf',
            'application/x-compress'                                                    => 'z',
            'application/x-zip'                                                         => 'zip',
            'application/zip'                                                           => 'zip',
            'application/x-zip-compressed'                                              => 'zip',
            'application/s-compressed'                                                  => 'zip',
            'multipart/x-zip'                                                           => 'zip',
            'text/x-scriptzsh'                                                          => 'zsh');

            foreach($ans_arr as $ans_item){
                $ans_data = new Req_ans;
                $ans_data->req_id = $req_s->id;
                $ans_data->question_id = $ans_item['question_id'];
                $ans_data->ans_text = $ans_item['ans_text'];
                $ans_data->ans_choice = $ans_item['ans_choice'];
                $ans_data->ans_other = $ans_item['ans_other'];

                $ans_data->save();

                if(array_key_exists('upload_ans', $ans_item)){
                    if(count($ans_item['upload_ans'])>0){
                        // return response()->json($ans_item['upload_ans'], 200);
                        // return response()->json($ans_data->id, 200);
                        foreach($ans_item['upload_ans'] as $upload_item){
                            // return response()->json($upload_item['image'], 200);
                            $explode = explode(',', $upload_item['image']);

                            $filename = $upload_item['filename'];

                            $filename = explode('.', $filename);

    
                            $decoded = base64_decode($explode[1]);
    
                            // if(str_contains($explode[0], 'jpeg')){
                            //     $extension = 'jpg';
                            // } else if(str_contains($explode[0], 'png')){
                            //     $extension = 'png';
                            // } else if(str_contains($explode[0], 'pdf')){
                            //     $extension = 'pdf';
                            // } else {
                                $ext1 = explode(';base64', $upload_item['image']);
                                $ext2 = substr($ext1[0], 5);
                                $extension = $mime_type[$ext2];
                            // }
                            $filename = date("YmdHis", time()).$filename[0].'.'.$extension;
    
                            $path = public_path().'/'.$filename;
    
                            file_put_contents($path, $decoded);
                            // $path = $decoded->storeAs('public/', $filename);
                            
                            $upload_ans = new Upload_ans;
                            $upload_ans->upload_id = $upload_item['upload_id'];
                            $upload_ans->ans_id = $ans_data->id;
                            $upload_ans->upload_seq = $upload_item['upload_seq'];
                            $upload_ans->upload_file = $filename;
                            $upload_ans->save();
                        }
                    } 
                }
                
                $ans_data->save();
            }

            $comment = new Comment;
            $comment->req_id = $req_s->id;
            $comment->save();


        }
        // return response()->json($req_s, 200);
        return response()->json(true, 200);
    }

    private function sendmail($email, $role, $req_id, $status){

        // return response()->json(array($email, $role, $req_id), 200);
        // $data = null;
        if($role=='staff' || $role=='teacher'){
            $data = array(
                'email' => $email,
                'subject' => 'แจ้งเตือน: คำร้องใหม่',
                'bodyMessage' => 'ท่านได้รับคำร้องใหม่จากนักศึกษาพิจารณาคำร้องได้ที่ลิ้งค์ด้านล่าง',
                'url' => "localhost:4200/$role/request/$req_id"
            );
        } else if($role=='student'){
            if($status==0){
                $data = array(
                    'email' => $email,
                    'subject' => 'แจ้งเตือน: สถานะคำร้องนักศึกษา',
                    'bodyMessage' => 'คำร้องของท่านไม่ผ่านการพิจารณาจากเจ้าหน้าที่โปรดตรวจสอบข้อมูล และทำการส่งคำร้องใหม่อีกครั้ง',
                    'url' => "localhost:4200/$role/history/$req_id"
                );
            }else{
                $data = array(
                    'email' => $email,
                    'subject' => 'แจ้งเตือน: สถานะคำร้องนักศึกษา',
                    'bodyMessage' => 'ได้ผ่านการพิจารณาแล้วโปรดตรวจสอบได้ที่',
                    'url' => "localhost:4200/$role/history/$req_id"
                );
            }
            
        }

        // return response()->json($email, 200);
        Mail::send('emails.mailform', $data, function($message) use ($data){
            $message->from('tupine@engr.tu.ac.th', 'ระบบบริหารจัดการคำร้องนักศึกษา');
            $message->to($data['email']);
            $message->subject($data['subject']);
        });
    }

    // submitComment
    public function submitComment(Request $request){
        if($request->isMethod('post')){
            $req = Req_status::findOrFail($request->req_id);
            // return response()->json($req, 200);
            $form = $req->form;
            $sign_control = $form->sign_control->first();
            $comment = $req->comment->first();

            $std = $req->student;
            $stdemail = $std->email;
            $stdrole = $std->user->first()->role;
            // return response()->json($comment, 200);
            if($req->sign_t1==$request->user_id){
                if($comment->comment_t1!=null){
                    return response()->json(false, 200);
                }
                $req->sign_t1_s = $request->status;
                if($req->sign_t1_s==2){
                    $req->status = "complete";
                    // return response()->json(array($stdemail, $stdrole, $req->id), 200);
                    $this->sendmail($stdemail, $stdrole, $req->id, 0);


                }else if($sign_control->sign_t2==null){
                    $req->status = "complete";
                    $this->sendmail($stdemail, $stdrole, $req->id, 1);
                } else {
                    $req->sign_t2_s = 0;
                }

                // return response()->json($comment->sign_time_1, 200);
                $req->save();
                $comment->comment_t1 = $request->comment;
                $comment->sign_time_1 = date("Y-m-d H:i:s", time());
                // return response()->json($comment->sign_time_1, 200);
                $comment->save();

                if($sign_control->sign_t2!=null){
                    $teacher = $req->st2;
                    $email = $teacher->email;
                               
                    if($req->status != "complete"){
                        $this->sendmail($email, $teacher->role, $req->id, 1);
                    }
                }
            } else if($req->sign_t2==$request->user_id){
                if($comment->comment_t2!=null){
                    return response()->json(false, 200);
                }
                $req->sign_t2_s = $request->status;
                if($sign_control->sign_t3==null){
                    $req->status = "complete";
                    $this->sendmail($stdemail, $stdrole, $req->id, 1);
                } else {
                    $req->sign_t3_s = 0;
                }
                $req->save();
                $comment->comment_t2 = $request->comment;
                $comment->sign_time_2 = date("Y-m-d H:i:s", time());
                $comment->save();

                if($sign_control->sign_t3!=null){
                    $teacher = $req->st3;
                    $email = $teacher->email;
                               
                    if($req->status != "complete"){
                        $this->sendmail($email, $teacher->role, $req->id, 1);
                    }
                }

            } else if($req->sign_t3==$request->user_id){
                if($comment->comment_t3!=null){
                    return response()->json(false, 200);
                } 
                $req->sign_t3_s = $request->status;
                if($sign_control->sign_t4==null){
                    $req->status = "complete";
                    $this->sendmail($stdemail, $stdrole, $req->id, 1);
                } else {
                    $req->sign_t4_s = 0;
                }
                $req->save();
                $comment->comment_t3 = $request->comment;
                $comment->sign_time_3 = date("Y-m-d H:i:s", time());
                $comment->save();

                if($sign_control->sign_t4!=null){
                    $teacher = $req->st4;
                    $email = $teacher->email;
                               
                    if($req->status != "complete"){
                        $this->sendmail($email, $teacher->role, $req->id, 1);
                    }
                }
            } else if($req->sign_t4==$request->user_id){

                if($comment->comment_t4!=null){
                    return response()->json(false, 200);
                }
                $req->sign_t4_s = $request->status;
                if($sign_control->sign_t5==null){
                    $req->status = "complete";
                    $this->sendmail($stdemail, $stdrole, $req->id, 1);
                } else {
                    $req->sign_t5_s = 0;
                }
                $req->save();
                $comment->comment_t4 = $request->comment;
                $comment->sign_time_4 = date("Y-m-d H:i:s", time());
                $comment->save();

                if($sign_control->sign_t5!=null){
                    $teacher = $req->st5;
                    $email = $teacher->email;
                               
                    if($req->status != "complete"){
                        $this->sendmail($email, $teacher->role, $req->id, 1);
                    }
                }
            } else if($req->sign_t5==$request->user_id){
                $req->sign_t5_s = $request->status;
                $req->status = "complete";
                $req->save();
                if($comment->comment_t5!=null){
                    return response()->json(false, 200);
                }
                $this->sendmail($stdemail, $stdrole, $req->id, 1);
                $comment->comment_t5 = $request->comment;
                $comment->sign_time_5 = date("Y-m-d H:i:s", time());
                $comment->save();


            } else {
                return response()->json(false, 200);
            }
            // return response()->json($sel_t, 200);
            // $req->sign_.$sel_t._s = '3';
            return response()->json(true, 200);
        }
    }

    //Check can manage
    public function checkCanManage(){
        $auth_user = Auth::user();
        $id = $auth_user->id;
        $user = User::findOrFail($id);
        $staff = $user->staff->first();
        $manage_lv = $staff->manage_lv;
        if($manage_lv>0){
            return response()->json(true, 200);
        }
        return response()->json(false, 200);
    }

    // get Signer
    public function getSigner(){
        // $config_db = Config_db::select('pos_detail')->get();
        $config_db = Config_db::all();
        return response()->json($config_db, 200);
    }

    //get Subject
    public function getSubject()
    {
        $auth_user = Auth::user();
        if($auth_user->role!='student'){
            return response()->json('message: unauthenticated', 401);
        }
        $subject_db = Subject_db::select('u_id')->get();
        $teacher_arr = (array)null;
        foreach($subject_db as $sub_item){
            $user = $sub_item->user;
            $teacher = $user->teacher->first();
            array_push($teacher_arr, $teacher);
        } 
        // $subject_db = Subject_db::all();
        return response()->json($teacher_arr, 200);
    }

    // get Subject detail
    public function getSubjectDetail(Request $Request){
        $auth_user = Auth::user();
        if($auth_user->role!='student'){
            return response()->json('message: unauthenticated', 401);
        }

        $subject_db = Subject_db::where([
            ['sub_code', $Request->sub_code],
            ['section', $Request->section]
        ])->get();
        foreach($subject_db as $sub_item){
            $user = $sub_item->user;
            $teacher = $user->teacher;
        }
        return response()->json($subject_db, 200);
    }

    // get Advisor 
    public function getAdvisor($id){
        $user = User::findOrFail($id);
        $student = $user->student->first();
        // $user_t = User::findOrFail($student->advisor);
        // $teacher = $user_t->teacher;
        // return response()->json($user_t, 200);
        $teacher = Teacher::where('u_id', $student->advisor)->get()->first();
        return response()->json($teacher, 200);
    }

    // upload img
    public function upload(Request $request){
        $explode = explode(',', $request->image);

        $decode = base64_decode($explode[1]);

        if(str_contains($explode[0], 'jpeg')){
            $extension = 'jpg';
        } else if(str_contains($explode[0], 'png')){
            $extension = 'png';
        } else if(str_contains($explode[0], 'pdf')){
            $extension = 'pdf';
        } else {
            $extension = 'txt';
        }

        $filename = str_random().'.'.$extension;

        $path = public_path().'/'.$filename;

        file_put_contents($path, $decoded);

        $upload_ans = new Upload_ans;
        $upload->upload_id = $request->upload_id;
        $upload->ans_id = $request->ans_id;
        $upload->upload_seq = $request->upload_seq;
        $upload->upload_file = $filename;
        $upload->save();

        return response()->json(true, 200);
    }


    //get Signerwrong
    public function getSignerworng($id){
        $form = Form::findOrFail($id);
        $signcontrol = $form->sign_control->first();

        if($signcontrol->sign_t1!=null){
            $st1 = $signcontrol->st1;
        }
        if($signcontrol->sign_t2!=null){
            $st2 = $signcontrol->st2;
        }
        if($signcontrol->sign_t3!=null){
            $st3 = $signcontrol->st3;
        }
        if($signcontrol->sign_t4!=null){
            $st4 = $signcontrol->st4;
        }
        if($signcontrol->sign_t5!=null){
            $st5 = $signcontrol->st5;
        }
        // $r_st1 = $st1->user;
        return response()->json($signcontrol, 200);
    }

    public function getInfoReq(){
        $auth_user = Auth::user();
        if($auth_user->role!='staff'){
            return response()->json('message: unauthenticated', 401);
        }

        $req = Req_status::all();
        foreach($req as $req_item){
            $form = $req_item->form;
            $stu = $req_item->student;
        }
    
        return response()->json($req, 200);
    }

    public function downloadfile($filename){
        $path = public_path().'/'.$filename;

        return response()->download($path, $filename);
    }
}
