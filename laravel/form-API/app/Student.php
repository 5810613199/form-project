<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    //Table Name
    protected $table = 'student';

    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    // we only want these 7 attributes able to be filled
    protected $fillable = array('u_id', 'stu_code', 'fname_th', 'lname_th', 'degree_id', 'major_th', 'reg_telephone', 'email', 'department', 'advisor');

    // Timestamps
    public $timestamps = false;

    // DEFINE RELATIONSHIPS --------------------------------------------------
    // each student belong to user
    public function user() {
        return $this->belongsTo('App\User', 'u_id'); // this matches the Eloquent model
    }

    // each student belong to teacher
    public function teacher() {
        return $this->belongsTo('App\Teacher', 'id'); // this matches the Eloquent model
    }

    // each student HAS many req
    public function req_status() {
        return $this->hasMany('App\Req_status'); // this matches the Eloquent model
    }
}
