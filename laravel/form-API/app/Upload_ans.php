<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upload_ans extends Model
{
    //Table Name
    protected $table = 'upload_ans';

    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    // we only want these 7 attributes able to be filled
    protected $fillable = array('upload_id', 'ans_id', 'upload_seq', 'upload_file');

    // Timestamps
    public $timestamps = false;

    // DEFINE RELATIONSHIPS --------------------------------------------------
    // each upload_ans belong to upload_q
    public function upload_q() {
        return $this->belongsTo('App\Upload_q'); // this matches the Eloquent model
    }

    // each upload_ans belong to req_ans
    public function req_ans() {
        return $this->belongsTo('App\Req_ans'); // this matches the Eloquent model
    }
}
