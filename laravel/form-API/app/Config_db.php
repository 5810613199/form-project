<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config_db extends Model
{
    //Table Name
    protected $table = 'config';

    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    // we only want these 5 attributes able to be filled
    protected $fillable = array('u_id', 'pos_detail');

    // Timestamps
    public $timestamps = false;

    // DEFINE RELATIONSHIPS --------------------------------------------------
    // each config belong to user
    public function form_question() {
        return $this->belongsTo('App\User', 'id'); // this matches the Eloquent model
    }
}
