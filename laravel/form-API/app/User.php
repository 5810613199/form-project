<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApitokens;

class User extends Authenticatable
{
    use HasApitokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // each user HAS many staff
    public function staff() {
        return $this->hasMany('App\Staff', 'u_id'); // this matches the Eloquent model
    }

    // each user HAS many student
    public function student() {
        return $this->hasMany('App\Student', 'u_id'); // this matches the Eloquent model
    }

    // each user HAS many teach
    public function teacher() {
        return $this->hasMany('App\Teacher', 'u_id'); // this matches the Eloquent model
    }

    // each users HAS many sign_t1
    public function sign_t1() {
        return $this->hasMany('App\Req_status', 'sign_t1'); // this matches the Eloquent model
    }

    // each users HAS many sign_t2
    public function sign_t2() {
        return $this->hasMany('App\Req_status', 'sign_t2'); // this matches the Eloquent model
    }

    // each users HAS many sign_t3
    public function sign_t3() {
        return $this->hasMany('App\Req_status', 'sign_t3'); // this matches the Eloquent model
    }

    // each users HAS many sign_t4
    public function sign_t4() {
        return $this->hasMany('App\Req_status', 'sign_t4'); // this matches the Eloquent model
    }

    // each users HAS many sign_t5
    public function sign_t5() {
        return $this->hasMany('App\Req_status', 'sign_t5'); // this matches the Eloquent model
    }

    // each users HAS many config
    public function config_db() {
        return $this->hasMany('App\Config_db', 'u_id'); // this matches the Eloquent model
    }

    // each users HAS many subject_db
    public function subject_db() {
        return $this->hasMany('App\Subject_db', 'u_id'); // this matches the Eloquent model
    }


}
