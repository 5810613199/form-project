<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Req_ans extends Model
{
    //Table Name
    protected $table = 'req_ans';

    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    // we only want these 5 attributes able to be filled
    protected $fillable = array('question_id', 'req_id', 'ans_text', 'ans_choice', 'ans_other');

    // Timestamps
    public $timestamps = false;

    // DEFINE RELATIONSHIPS --------------------------------------------------
    // each req_ans belong to form_question
    public function form_question() {
        return $this->belongsTo('App\Form_question', 'id'); // this matches the Eloquent model
    }

    // each req_ans belong to req_status
    public function req_status() {
        return $this->belongsTo('App\Req_status', 'id'); // this matches the Eloquent model
    }

    // each req_ans belong to question_choice
    public function question_choice() {
        return $this->belongsTo('App\Question_choice', 'ans_choice'); // this matches the Eloquent model
    }

    // each req_ans has many upload_ans
    public function upload_ans(){
        return $this->hasmany('App\Upload_ans', 'ans_id');
    }
}
