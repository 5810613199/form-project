<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Staff;
use App\Student;
use App\Teacher;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // clear our database ------------------------------------------
        DB::table('users')->delete();
        DB::table('student')->delete();
        DB::table('staff')->delete();
        DB::table('teacher')->delete();

        // seed our form table -----------------------
        // we'll create three different User

        // user 1
        $user1 = User::create(array(
            'name'         => 'Thanat Boonkong',
            'email'         => 'thanat@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
            'role' => 'student',
        ));

        // user 2
        $user2 = User::create(array(
            'name'         => 'krisnatep Boon',
            'email'         => 'kris@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
            'role' => 'student',
        ));

        // user 3 
        $user3 = User::create(array(
            'name'         => 'Teaher1',
            'email'         => 'teacher1@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
            'role' => 'teacher',
        ));

        // user 4 
        $user4 = User::create(array(
            'name'         => 'Teaher2',
            'email'         => 'teacher2@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
            'role' => 'teacher',
        ));

        // user 5 
        $user5 = User::create(array(
            'name'         => 'Teaher3',
            'email'         => 'teacher3@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
            'role' => 'teacher',
        ));

        // user 6
        $user6 = User::create(array(
            'name'         => 'Staff1',
            'email'         => 'staff1@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
            'role' => 'Staff',
        ));

        // user 7
        $user7 = User::create(array(
            'name'         => 'Staff2',
            'email'         => 'staff2@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
            'role' => 'Staff',
        ));

        $this->command->info('Create user Success!!');

        // we'll create three different Teacher
        // teacher 1
        $teacher1 = Teacher::create(array(
            'u_id'         => $user3->id,
            'personid' => '111111',
            'fname_th'         => 'ครู1',
            'lname_th' => 'เทส',
            'titleacafullt' => 'ดร',
            'major_th' => 'คอมพิวเตอร์',
            'reg_telephone' => '0967144322',
            'email' => $user3->email,
            'department_th' => 'ปกติ',
            'positionid' => '1111111111',
        ));

        // teacher 2
        $teacher2 = Teacher::create(array(
            'u_id'         => $user4->id,
            'personid' => '111112',
            'fname_th'         => 'ครู2',
            'lname_th' => 'เทส',
            'titleacafullt' => 'ดร',
            'major_th' => 'คอมพิวเตอร์',
            'reg_telephone' => '0967144322',
            'email' => $user4->email,
            'department_th' => 'ปกติ',
            'positionid' => '1111111112',
        ));

        // teacher 3
        $teacher3 = Teacher::create(array(
            'u_id'         => $user5->id,
            'personid' => '111113',
            'fname_th'         => 'ครู3',
            'lname_th' => 'เทส',
            'titleacafullt' => 'ดร',
            'major_th' => 'คอมพิวเตอร์',
            'reg_telephone' => '0967144322',
            'email' => $user3->email,
            'department_th' => 'ปกติ',
            'positionid' => '1111111113',
        ));

        $this->command->info('Create Teacher Success!!');

        // we'll create three different Student
        // student 1
        $student1 = Student::create(array(
            'u_id'         => $user1->id,
            'stu_code'         => '5810680354',
            'fname_th'         => 'ธนัท',
            'lname_th' => 'บุญคง',
            'degree_id' => '4',
            'major_th' => 'คอมพิวเตอร์',
            'reg_telephone' => '0967144322',
            'email' => $user1->email,
            'department_th' => 'ปกติ',
            'advisor' => $teacher1->id,
        ));

        // student 2
        $student2 = Student::create(array(
            'u_id'         => $user2->id,
            'fname_th'         => 'กฤษณเทพ',
            'stu_code'         => '5810610000',
            'lname_th' => 'บุญพรหมมา',
            'degree_id' => '4',
            'major_th' => 'คอมพิวเตอร์',
            'reg_telephone' => '1234567890',
            'email' => $user2->email,
            'department_th' => 'ปกติ',
            'advisor' => $teacher1->id,
        ));

        $this->command->info('Create Student Success!!');

        // we'll create three different Staff
        // staff 1
        $staff1 = Staff::create(array(
            'u_id'         => $user6->id,
            'personid' => '222221',
            'fname_th'         => 'เจ้าหน้าที่1',
            'lname_th' => 'เทส',
            'phone_number' => '0967144322',
            'email' => $user6->email,
            'positionid' => '2222222221',
        ));

        // staff 2
        $staff2 = Staff::create(array(
            'u_id'         => $user7->id,
            'personid' => '222222',
            'fname_th'         => 'เจ้าหน้าที่2',
            'lname_th' => 'เทส',
            'phone_number' => '0967144322',
            'email' => $user7->email,
            'positionid' => '2222222222',
        ));

        $this->command->info('Create Staff Success!!');
    }
}
