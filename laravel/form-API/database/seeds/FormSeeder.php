<?php

use Illuminate\Database\Seeder;
use App\Form;
use App\Form_question;
use App\Question_choice;
use App\Upload_q;
use App\Sign_control;
use App\User;
use App\Staff;
use App\Student;
use App\Teacher;
use App\Req_status;
use App\Req_ans;
use App\Comment;
use App\Config_db;
use App\Subject_db;
use App\Upload_ans;

class FormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        // clear our database ------------------------------------------
        DB::table('users')->delete();
        DB::table('student')->delete();
        DB::table('staff')->delete();
        DB::table('teacher')->delete();
        DB::table('config')->delete();
        DB::table('subject')->delete();

        // we'll create three different User

        // user 1
        $user1 = User::create(array(
            'name'         => 'Thanat Boonkong',
            'email'         => 'tailove123@windowslive.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
            'role' => 'student',
        ));

        // user 2
        $user2 = User::create(array(
            'name'         => 'krisnatep Boon',
            'email'         => 'kriszoza.007@gmail.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
            'role' => 'student',
        ));

        // user 13
        $user13 = User::create(array(
            'name'         => 'soften tupine',
            'email'         => 'soften@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
            'role' => 'student',
        ));

        // user 3 
        $user3 = User::create(array(
            'name'         => 'Teaher1',
            'email'         => 'teacher1@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
            'role' => 'teacher',
        ));

        // user 4 
        $user4 = User::create(array(
            'name'         => 'Teaher2',
            'email'         => 'teacher2@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
            'role' => 'teacher',
        ));

        // user 5 
        $user5 = User::create(array(
            'name'         => 'Teaher3',
            'email'         => 'teacher3@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
            'role' => 'teacher',
        ));

        // user 6
        $user6 = User::create(array(
            'name'         => 'Staff1',
            'email'         => 'staff1@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
            'role' => 'staff',
        ));

        // user 7
        $user7 = User::create(array(
            'name'         => 'Staff2',
            'email'         => 'staff2@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
            'role' => 'staff',
        ));

        // user 8
        $user8 = User::create(array(
            'name'         => 'Teaher4',
            'email'         => 'teacher4@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
            'role' => 'teacher',
        ));

        // user 9
        $user9 = User::create(array(
            'name'         => 'Teaher5',
            'email'         => 'teacher5@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
            'role' => 'teacher',
        ));

        // user 10
        $user10 = User::create(array(
            'name'         => 'Teaher6',
            'email'         => 'teacher6@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
            'role' => 'teacher',
        ));

        // user 11
        $user11 = User::create(array(
            'name'         => 'Teaher7',
            'email'         => 'teacher7@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
            'role' => 'teacher',
        ));

        // user 12
        $user12 = User::create(array(
            'name'         => 'Teaher8',
            'email'         => 'teacher8@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
            'role' => 'teacher',
        ));

        // user 999
        $user999 = User::create(array(
            'name'         => 'admin',
            'email'         => 'admin@test.com',
            'password' => '$2y$10$sV/ODrsJ0a9ODHt/cSBKG.u14qnGiTYxpJTWlN8XMlu2YsUyVCSd.',
            'role' => 'admin',
        ));

        $this->command->info('Create user Success!!');

        // we'll create three different Teacher
        // teacher 4
        $teacher4 = Teacher::create(array(
            'u_id'         => $user8->id,
            'personid' => '111114',
            'fname_th'         => 'อาจารย์ที่ปรึกษา Dummy',
            'lname_th' => 'เทส',
            'titleacafullt' => 'ดร',
            'major_th' => 'คอมพิวเตอร์',
            'reg_telephone' => '0967144322',
            'email' => $user3->email,
            'department_th' => 'ปกติ',
            'positionid' => '1111111114',
        ));

        // teacher 1
        $teacher1 = Teacher::create(array(
            'u_id'         => $user3->id,
            'personid' => '111111',
            'fname_th'         => 'อาจารย์ประจำวิชา Dummy',
            'lname_th' => 'เทส',
            'titleacafullt' => 'ดร',
            'major_th' => 'คอมพิวเตอร์',
            'reg_telephone' => '0967144322',
            'email' => $user3->email,
            'department_th' => 'ปกติ',
            'positionid' => '1111111111',
        ));

        // teacher 2
        $teacher2 = Teacher::create(array(
            'u_id'         => $user4->id,
            'personid' => '111112',
            'fname_th'         => 'ครู2',
            'lname_th' => 'เทส',
            'titleacafullt' => 'ดร',
            'major_th' => 'คอมพิวเตอร์',
            'reg_telephone' => '0967144322',
            'email' => $user4->email,
            'department_th' => 'ปกติ',
            'positionid' => '1111111112',
        ));

        // teacher 3
        $teacher3 = Teacher::create(array(
            'u_id'         => $user5->id,
            'personid' => '111113',
            'fname_th'         => 'ครู3',
            'lname_th' => 'เทส',
            'titleacafullt' => 'ดร',
            'major_th' => 'คอมพิวเตอร์',
            'reg_telephone' => '0967144322',
            'email' => $user3->email,
            'department_th' => 'ปกติ',
            'positionid' => '1111111113',
        ));

        // teacher 5
        $teacher5 = Teacher::create(array(
            'u_id'         => $user9->id,
            'personid' => '111115',
            'fname_th'         => 'อาจารย์วิชา Calculus1 sec 1',
            'lname_th' => 'เทส',
            'titleacafullt' => 'ดร',
            'major_th' => 'I-Pen',
            'reg_telephone' => '0967144322',
            'email' => $user3->email,
            'department_th' => 'TU-PINE',
            'positionid' => '1111111115',
        ));

        // teacher 6
        $teacher6 = Teacher::create(array(
            'u_id'         => $user10->id,
            'personid' => '111116',
            'fname_th'         => 'อาจารย์วิชา Physic1 sec 1',
            'lname_th' => 'เทส',
            'titleacafullt' => 'ดร',
            'major_th' => 'EMB',
            'reg_telephone' => '0967144322',
            'email' => $user3->email,
            'department_th' => 'TU-PINE',
            'positionid' => '1111111116',
        ));

        // teacher 7
        $teacher7 = Teacher::create(array(
            'u_id'         => $user11->id,
            'personid' => '111117',
            'fname_th'         => 'อาจารย์วิชา C Language sec 1',
            'lname_th' => 'เทส',
            'titleacafullt' => 'ดร',
            'major_th' => 'คอมพิวเตอร์',
            'reg_telephone' => '0967144322',
            'email' => $user3->email,
            'department_th' => 'ปกติ',
            'positionid' => '1111111117',
        ));

        // teacher 8
        $teacher8 = Teacher::create(array(
            'u_id'         => $user12->id,
            'personid' => '111118',
            'fname_th'         => 'อาจารย์วิชา Calculus1 sec 2',
            'lname_th' => 'เทส',
            'titleacafullt' => 'ดร',
            'major_th' => 'อุต',
            'reg_telephone' => '0967144322',
            'email' => $user3->email,
            'department_th' => 'ปกติ',
            'positionid' => '1111111118',
        ));


        $this->command->info('Create Teacher Success!!');

        // subject1 
        $subject1 = Subject_db::create(array(
            'u_id'         => $user9->id,
            'sub_code' => 'MA111',
            'sub_name' => 'Calculus1',
            'section' => 1
        ));

        // subject2
        $subject2 = Subject_db::create(array(
            'u_id'         => $user10->id,
            'sub_code' => 'PHY111',
            'sub_name' => 'Physic1',
            'section' => 1
        ));

        // subject3 
        $subject3 = Subject_db::create(array(
            'u_id'         => $user11->id,
            'sub_code' => 'TU156',
            'sub_name' => 'C Language',
            'section' => 1
        ));

        // subject4 
        $subject4 = Subject_db::create(array(
            'u_id'         => $user12->id,
            'sub_code' => 'MA111',
            'sub_name' => 'Calculus1',
            'section' => 2
        ));

        $this->command->info('Create Sunject Success!!');


        // config 5
        $config5 = Config_db::create(array(
            'u_id'         => $user8->id,
            'pos_detail' => 'อาจารย์ที่ปรึกษา',
        ));

        // config 3
        $config3 = Config_db::create(array(
            'u_id'         => $user3->id,
            'pos_detail' => 'อาจารย์ประจำวิชา',
        ));
                
        // config 1
        $config1 = Config_db::create(array(
            'u_id'         => $user5->id,
            'pos_detail' => 'รองคณบดี',
        ));

        // config 2
        $config2 = Config_db::create(array(
            'u_id'         => $user4->id,
            'pos_detail' => 'หัวหน้าโครงการ TU-PINE',
        ));

        // config 4
        $config4 = Config_db::create(array(
            'u_id'         => $user6->id,
            'pos_detail' => 'เจ้าหน้าที่ตรวจสอบ',
        ));

        

        $this->command->info('Create Config Success!!');

        // we'll create three different Student
        // student 1
        $student1 = Student::create(array(
            'u_id'         => $user1->id,
            'stu_code'         => '5810680354',
            'fname_th'         => 'ธนัท',
            'lname_th' => 'บุญคง',
            'degree_id' => '4',
            'major_th' => 'TU-PINE',
            'reg_telephone' => '0967144322',
            'email' => $user1->email,
            'department_th' => 'EBM',
            'advisor' => $user11->id,
        ));

        // student 2
        $student2 = Student::create(array(
            'u_id'         => $user2->id,
            'fname_th'         => 'กฤษณเทพ',
            'stu_code'         => '5810610000',
            'lname_th' => 'บุญพรหมมา',
            'degree_id' => '4',
            'major_th' => 'TU-PINE',
            'reg_telephone' => '1234567890',
            'email' => $user2->email,
            'department_th' => 'IPen',
            'advisor' => $user10->id,
        ));

        // student 3
        $student3 = Student::create(array(
            'u_id'         => $user13->id,
            'fname_th'         => 'ซอฟต์เอนต์',
            'stu_code'         => '5810612340',
            'lname_th' => 'ทียูพาย',
            'degree_id' => '1',
            'major_th' => 'TU-PINE',
            'reg_telephone' => '1234567890',
            'email' => $user13->email,
            'department_th' => 'Soften',
            'advisor' => $user10->id,
        ));

        $this->command->info('Create Student Success!!');

        // we'll create three different Staff
        // staff 1
        $staff1 = Staff::create(array(
            'u_id'         => $user6->id,
            'personid' => '222221',
            'fname_th'         => 'เจ้าหน้าที่1',
            'lname_th' => 'เทส',
            'phone_number' => '0967144322',
            'email' => $user6->email,
            'positionid' => '2222222221',
            'manage_lv' => '2'
        ));

        // staff 2
        $staff2 = Staff::create(array(
            'u_id'         => $user7->id,
            'personid' => '222222',
            'fname_th'         => 'เจ้าหน้าที่2',
            'lname_th' => 'เทส',
            'phone_number' => '0967144322',
            'email' => $user7->email,
            'positionid' => '2222222222',
            'manage_lv' => '1'
        ));

        $this->command->info('Create Staff Success!!');

        // clear our database ------------------------------------------
        DB::table('form')->delete();
        DB::table('form_question')->delete();
        DB::table('question_choice')->delete();
        DB::table('sign_control')->delete();
    
        // seed our form table -----------------------
        // we'll create three different bears
    
        // form 1
        $form_req1 = Form::create(array(
            'form_name'         => 'request for subject',
            'mod_by'         => 'thanat',
            'create_by' => 'thanat',
            'form_pos' => 3
        ));
    
        // form 2
        $form_req2 = Form::create(array(
            'form_name'         => 'leave off the study',
            'mod_by'         => 'kris',
            'create_by' => 'kris',
            'form_pos' => 2
        ));
    
        // form 3
        $form_req3 = Form::create(array(
            'form_name'         => 'resign form study',
            'mod_by'         => 'teacher1',
            'create_by' => 'teacher1',
            'form_pos' => 1
        ));
    
        $this->command->info('Create From Success!!');
    
        // seed our form_question table ------------------------
    
        // form 1 question 1
        $form_question11 = Form_question::create(array(
            'form_id'         => $form_req1->id,
            'question_seq'         => 1,
            'question_type' => 'checkboxes',
            'question_detail' => 'what is that',
            'n_choice' => 2,
            'req_ans' => true,
            'other' => true,
        ));
    
        // form 1 question 2
        $form_question12 = Form_question::create(array(
            'form_id'         => $form_req1->id,
            'question_seq'         => 2,
            'question_type' => 'subject',
            'question_detail' => 'what subject',
            'n_choice' => 2,
            'req_ans' => true,
            'other' => false,
        ));
    
        // form 2 question 1
        $form_question21 = Form_question::create(array(
            'form_id'         => $form_req2->id,
            'question_seq'         => 1,
            'question_type' => 'short answer',
            'question_detail' => 'what is your name',
            'n_choice' => 0,
            'req_ans' => true,
            'other' => false,
        ));
    
        // form 2 question 2
        $form_question22 = Form_question::create(array(
            'form_id'         => $form_req2->id,
            'question_seq'         => 2,
            'question_type' => 'short answer',
            'question_detail' => 'what is your fav',
            'n_choice' => 0,
            'req_ans' => true,
            'other' => false,
        ));
    
        // form 3 question 1
        $form_question31 = Form_question::create(array(
            'form_id'         => $form_req3->id,
            'question_seq'         => 1,
            'question_type' => 'Radio Button',
            'question_detail' => 'what is your pet',
            'n_choice' => 2,
            'req_ans' => true,
            'other' => false,
        ));
    
        // form 3 question 2
        $form_question32 = Form_question::create(array(
            'form_id'         => $form_req3->id,
            'question_seq'         => 2,
            'question_type' => 'file upload',
            'question_detail' => 'your paper',
            'n_choice' => 0,
            'req_ans' => true,
            'other' => false,
        ));
    
        $this->command->info('Create Question Success');
    
        // seed our choice table ---------------------
    
        // form 1 question 1 choice 1
        $choice111 = Question_choice::create(array(
            'question_id'         => $form_question11->id,
            'choice_detail'         => 'dog',
            'choice_seq' => 1,
        ));
    
        // form 1 question 1 choice 2
        $choice112 = Question_choice::create(array(
            'question_id'         => $form_question11->id,
            'choice_detail'         => 'cat',
            'choice_seq' => 2,
        ));
    
        // form 3 question 1 choice 1
        $choice131 = Question_choice::create(array(
            'question_id'         => $form_question31->id,
            'choice_detail'         => 'fish',
            'choice_seq' => 1,
        ));
    
        // form 3 question 1 choice 2
        $choice132 = Question_choice::create(array(
            'question_id'         => $form_question31->id,
            'choice_detail'         => 'bear',
            'choice_seq' => 2,
        ));
    
        $this->command->info('Create choice success!!');
    
        // seed our file upload table ---------------------
    
        // form 3 file upload 1
        $fileupload31 = Upload_q::create(array(
            'question_id'         => $form_question32->id,
            'form_id'         => $form_req3->id,
            'n_file' => 1,
            'file_type' => 'jpg',
        ));
    
        $this->command->info('Create file upload success!!');
        

        // seed our sign control table ---------------------

        // form 1 sign control
        $signcontrol1 = Sign_control::create(array(
            'form_id'         => $form_req1->id,
            'n_sign'         => 3,
            'sign_t1' => $user6->id,
            'sign_t2' => $user3->id,
            'sign_t3' => $user4->id,
        ));

        // form 2 sign control
        $signcontrol2 = Sign_control::create(array(
            'form_id' => $form_req2->id,
            'n_sign'  => 3,
            'sign_t1' => $user6->id,
            'sign_t2' => $user8->id,
            'sign_t3' => $user4->id,
        ));

        // form 3 sign control
        $signcontrol3 = Sign_control::create(array(
            'form_id' => $form_req3->id,
            'n_sign'  => 1,
            'sign_t1' => $user6->id,

        ));

        // clear our database ------------------------------------------
        DB::table('req_ans')->delete();
        DB::table('req_status')->delete();
        DB::table('comment')->delete();
        DB::table('upload_ans')->delete();

        // seed our Req_status table ------------------------
        // Req_status1 
        $reqstatus1 = Req_status::create(array(
            'form_id'         => $form_req1->id,
            'student_id'         => $student1->id,
            'sign_t1' => $user6->id,
            'sign_t2' => $user9->id,
            'sign_t3' => $user4->id,
            'sign_t1_s' => 1,
            'sign_t2_s' => 2,
            'sign_t3_s' => 0,
            'status' => 'incomplete',
            'semester' => '2/61'
        ));

        // Req_status2 
        $reqstatus2 = Req_status::create(array(
            'form_id'         => $form_req2->id,
            'student_id'         => $student2->id,
            'sign_t1' => $user6->id,
            'sign_t2' => $student2->advisor,
            'sign_t3' => $user4->id,
            'sign_t1_s' => 0,
            'status' => 'incomplete',
            'semester' => '1/61'
        ));

        // Req_status3
        $reqstatus3 = Req_status::create(array(
            'form_id'         => $form_req3->id,
            'student_id'         => $student2->id,
            'sign_t1' => $user6->id,
            'sign_t1_s' => 0,
            'status' => 'incomplete',
            'semester' => '1/61'
        ));

        $this->command->info('Create Req_status Success!!');

        // seed our Req_ans table ------------------------
        // Req_ans1 
        $reqans1 = Req_ans::create(array(
            'question_id'         => $form_question11->id,
            'req_id'         => $reqstatus1->id,
            'ans_choice' => $choice111->id,
        ));

        // Req_ans1 
        $reqans5 = Req_ans::create(array(
            'question_id'         => $form_question11->id,
            'req_id'         => $reqstatus1->id,
            'ans_choice' => $choice112->id,
        ));

        // Req_ans2
        $reqans2 = Req_ans::create(array(
            'question_id'         => $form_question12->id,
            'req_id'         => $reqstatus1->id,
            'ans_text' => 'MA111/1',
        ));

        // Req_ans3
        $reqans3 = Req_ans::create(array(
            'question_id'         => $form_question21->id,
            'req_id'         => $reqstatus2->id,
            'ans_text' => 'krisanatep',
        ));

        // Req_ans4
        $reqans4 = Req_ans::create(array(
            'question_id'         => $form_question22->id,
            'req_id'         => $reqstatus2->id,
            'ans_text' => 'football',
        ));

        // Req_ans5
        $reqans5 = Req_ans::create(array(
            'question_id'         => $form_question31->id,
            'req_id'         => $reqstatus3->id,
            'ans_choice' => $choice131->id,
        ));

        // Req_ans6
        $reqans6 = Req_ans::create(array(
            'question_id'         => $form_question31->id,
            'req_id'         => $reqstatus3->id,
            'ans_other' => 'Upload_file',
        ));

        $this->command->info('Create Req_ans Success!!');

        // Req_ans6
        $upload_ans1 = Upload_ans::create(array(
            'upload_id'         => $fileupload31->id,
            'ans_id'         => $reqans6->id,
            'upload_seq' => 1,
            'upload_file' => 'fileupload.jpg'
        ));


        // seed our Comment table ------------------------
        // comment1 
        $comment1 = Comment::create(array(
            'req_id'         => $reqstatus1->id,
            'comment_t1' => 'should approve',
            'comment_t2' => 'should not approve',
            'sign_time_1' => date("Y-m-d H:i:s", time()),
            'sign_time_2' => date("Y-m-d H:i:s", time()),
        ));

        // comment2
        $comment2 = Comment::create(array(
            'req_id'         => $reqstatus2->id,
        ));

        // comment3
        $comment3 = Comment::create(array(
            'req_id'         => $reqstatus3->id,
        ));

        $this->command->info('Create Comment Success!!');
    }
}
