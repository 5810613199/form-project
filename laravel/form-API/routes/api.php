<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return  $request->user();
});

Route::middleware('auth:api')->get('/checktoken', function (Request $request) {
    return  response()->json(true, 200);
});


Route::group(['middleware' => 'auth:api'], function(){

//------------------------------ Form --------------------------
// List form
Route::get('getAllForms', 'FormController@index');

// get form
Route::get('getFormByID/{id}', 'FormController@getFormToShow');

// get form question
Route::get('getQuestions/{id}', 'FormController@getQuestions');

// update form
Route::put('formupdate', 'FormController@updateForm');

// create form
Route::post('formcreate', 'FormController@createForm');

// Delete form
Route::delete('form/{id}', 'FormController@destroy');

// Update form name
Route::put('updateFormName', 'FormController@updateFormName');

// Update form pos (swap)
Route::put('updatePosition', 'FormController@updatePosition');

//------------------------------ Request --------------------------

// Check can mange
Route::post('checkCanManage', 'RequestController@checkCanManage');

// List All Request
Route::get('getAllRequests/{id}', 'RequestController@getAllRequest');

// get RequestByid
Route::get('getRequestById/{id}', 'RequestController@getRequestById');

// get Comment
Route::get('getComment/{id}', 'RequestController@getComment');

// get Req_ans
Route::get('getAnswerForm/{req_id}', 'RequestController@getAnswerForm');

// submitForm(Request) 
Route::post('submitForm', 'RequestController@submitForm');

// submit Comment
Route::post('submitComment', 'RequestController@submitComment');

// get Signer
Route::get('getSigner', 'RequestController@getSigner');

// get Subject
Route::get('getSubject', 'RequestController@getSubject');

// get Subject_detail
Route::post('getSubjectDetail', 'RequestController@getSubjectDetail');

// get Advisor
Route::get('getAdvisor/{id}', 'RequestController@getAdvisor');

// get All Req
Route::get('getInfoReq', 'RequestController@getInfoReq');



// -------------------------- UserController ----------------------------

// checkstudent
Route::get('checkstudent', 'UserController@checkIsStudent');

// check teacher
Route::get('checkteacher', 'UserController@checkIsTeacher');

// check staff
Route::get('checkstaff', 'UserController@checkIsStaff');

// check admin
Route::get('checkadmin', 'UserController@checkIsAdmin');

// set role
Route::post('setConfig', 'UserController@setConfig');

// set operation_lv
Route::post('setOperate_lv', 'UserController@setOperate_lv');

// get Student info
Route::get('getStudentInfoByuid/{id}', 'UserController@getStudentInfoByuid');

// get Student info
Route::get('getStudentInfoBystdid/{id}', 'UserController@getStudentInfoBystdid');

// get Teacher info
Route::get('getTeacherInfo/{id}', 'UserController@getTeacherInfo');

// get Staff info
Route::get('getStaffInfo/{id}', 'UserController@getStaffInfo');

// get Role By ID
Route::get('getRoleByIdTeacher/{id}', 'UserController@getRoleByIdTeacher');

// get All Teacher
Route::get('getAllTeacher', 'UserController@getAllTeacher');

// get setable config
Route::get('getsetConfig', 'UserController@getsetConfig');

// create new config
Route::post('createConfig', 'UserController@createConfig');

// get all operate_lv
Route::get('getAllOperate_lv', 'UserController@getAllOperate_lv');

// create new operation_lv
Route::post('createOpr_lv', 'UserController@createOpr_lv');

// get all teacher staff
Route::get('getTeacherStaff', 'UserController@getTeacherStaff');

// download file with file name
Route::get('downloadfile/{filename}', 'RequestController@downloadfile');

// check valid token
Route::post('checkvalidtoken', 'UserController@checkvalidtoken');

// download file with file name
Route::delete('deleteConfig/{id}', 'UserController@deleteConfig');

// logout api


Route::post('checkauth', 'UserController@checkIsAuth');
Route::post('logout','UserController@logoutApi');
});




