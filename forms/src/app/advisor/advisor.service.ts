import { Injectable } from '@angular/core';
import { AppService } from '../app.service';
import { Http, Headers, RequestOptions, ResponseContentType } from '@angular/http';
import { Observable, of } from 'rxjs';
import { UserInfo, icomment, iperson } from '../utils/interfaces';
import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AdvisorService {
  api: string = 'http://formapi.pro/'
  options: RequestOptions
  constructor(private app: AppService, private _http: HttpClient) {
  }

  private _setHeader(): HttpHeaders {
    let username = this.app.getLocal('username')
    let header = new HttpHeaders({
      'Content-Type': 'application/json',
      'username': username + ''
    }
    )
    return header;
  }

  checkteacher(): Observable<boolean> {
    let url = this.api + 'api/checkteacher'
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url, httpOptions).pipe(map(res => {
      if (res) {
        return true;
      } else {
        return false;
      }
    }), catchError(err => {
      return this.recheckteacher()
    }))
  }

  private recheckteacher(): Observable<boolean> {
    let url = this.api + 'api/checkteacher'
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get<boolean>(url, httpOptions).pipe(
      map(res => { return res }),
      catchError(err => {
        return of(false)
      })
    )
  }

  getAllRequests(): Observable<any> {
    let person: iperson = JSON.parse(this.app.getLocal('personalinfo'))
    let url = this.api + 'api/getAllRequests/' + person.person.personid
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url, httpOptions).pipe(map(res => {
      return res;
    }), catchError(err => {
      return of([[], []])
    }));

  }

  getSigner(): Observable<any> {
    let url = this.api + 'api/getSigner'
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url, httpOptions).pipe(map(res => {
      return res;
    }), catchError(err => {
      return of([])
    }));
  }

  getRequestByID(req_id: number): Observable<any> {
    let url = this.api + 'api/getRequestById/' + req_id
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url, httpOptions)
      .pipe(map(res => {
        return res;
      }), catchError(err => {
        return of(null)
      }));
  }

  getAnswerForm(req_id: number): Observable<any> {
    let url = this.api + 'api/getAnswerForm/' + req_id
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url, httpOptions)
      .pipe(map(res => {
        return res;
      }), catchError(err => {
        return of(null)
      }));
  }

  getTeacherInfo(user_id: number): Observable<any> {
    let url = this.api + 'api/getTeacherInfo/' + user_id;
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url, httpOptions)
      .pipe(map(res => {
        return res;
      }), catchError(err => {
        return of(null)
      }));
  }

  getTeacherStaff(): Observable<any> {
    let url = this.api + 'api/getTeacherStaff';
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url, httpOptions)
  }

  getStaffInfo(user_id: number): Observable<any> {
    let url = this.api + 'api/getStaffInfo/' + user_id;
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url,httpOptions)
      .pipe(map(res => {
        return res;
      }), catchError(err => {
        return of(null)
      }));
  }

  getFile(filename: string): Observable<any> {
    let url = this.api + 'api/downloadfile/' + filename;
    return this._http.get(url, {
      headers: this._setHeader(),
      responseType: 'blob'
    })
  }

  submitComment(comment: icomment): Observable<any> {
    let url = this.api + 'api/submitComment'
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.post(url, JSON.stringify(comment), httpOptions)
      .pipe(map(res => {
        return res;
      }), catchError(err => {
        return of(false)
      }));
  }
}
