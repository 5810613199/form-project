import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FormControl, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from "@angular/material";
import {
  iform,
  ireqans,
  istudent_info,
  icomment,
  isignctrl,
  User,
  UserInfo,
  icommentadv,
  iposition,
  iupload_ans,
  ireq_status,
  iSigner,
  iperson
} from "../../utils/interfaces";
import { DialogComponent } from "../dialog/dialog.component";
import { AdvisorService } from "../advisor.service";
import { AppService } from "src/app/app.service";

@Component({
  selector: "app-request",
  templateUrl: "./request.component.html",
  styleUrls: ["./request.component.css"]
})
export class RequestComponent implements OnInit {
  id;
  user: UserInfo;
  req_id: any;
  form_id: any;
  student_info: istudent_info = {};
  question_info = {};
  form_info = {};
  form: iform = {};
  req_ans: ireqans[] = [];
  request: ireq_status = {};
  payload: icomment = {};
  comments: icomment[] = [];
  issigned: boolean = false;
  lastTeacherApprove: boolean = false;
  lastTeacherIsSigned: boolean = false;
  errCommentEmpty: boolean = false;
  semestervalue: string = "";


  formControl = new FormControl("");

  constructor(
    private router: ActivatedRoute,
    private navrouter: Router,
    public dialog: MatDialog,
    private as: AdvisorService,
    private app: AppService
  ) { }

  ngOnInit() {
    this.id = this.router.snapshot.url[1].path;
    this.user = JSON.parse(this.app.getLocal("userinfo"));
    this.getRequestInfo();
    this.getFormInfo();
  }

  getRequestInfo() {
    this.as.getRequestByID(this.id).subscribe(res => {
      if (res) {
        let signctrl = res.req_s.form.sign_control[0];
        let result = res.req_s;
        this.request = result;
        let tempcomment: icommentadv = result.comment[0];
        this.semestervalue = result.semester;
        let person: iperson = JSON.parse(this.app.getLocal('personalinfo'))
        // null ไม่เห็น 0 เห็นแต่ยังไม่ได้ทำ 1 อนุมัติ 2 ไม่อนุมัติ
        if (tempcomment != undefined) {
          let countsigner = 0;
          for (let i = 1; i <= 5; i++) {
            let sign = `sign_t${i}`;
            //if staff
            if (person.person.personid === result[sign] && result[sign + "_s"] != null) {
              if (result[sign + "_s"] != 0) {
                //check already sign
                this.issigned = true;
              }
            }

            if (result[sign] != null) {
              countsigner++;
            }
          }
          this.as.getTeacherStaff().subscribe(personss => {
            this.as.getSigner().subscribe(out => {
              let persons: iperson[] = personss
              let op: iSigner[] = out;
              for (let i = 1; i <= countsigner; i++) {
                let cmnt = `comment_t${i}`;
                let sign = `sign_t${i}`;
                let sign_time = `sign_time_${i}`;
                let advisor: iSigner = op.find(
                  element => signctrl[sign] === element.personid
                );
                let objcomment: icomment = {
                  req_id: tempcomment.req_id,
                  user_id: result[sign],
                  comment: tempcomment[cmnt],
                  role: advisor.pos_detail,
                  status: result[sign + "_s"],
                  timestamp: tempcomment[sign_time],
                  teacher_info: persons.find(person => person.person.personid === result[sign])
                };
                this.comments.push(objcomment);
              }
              let findStaffApprove = this.comments.find(
                el => el.status == 1 && el.role === "เจ้าหน้าที่ตรวจสอบ"
              );
              this.comments = this.comments.filter(el => {
                if (el.role == "เจ้าหน้าที่ตรวจสอบ" && el.status == 2) {
                  //can see only staff not approve
                  return true;
                } else if (findStaffApprove) {
                  //if staff approve
                  return true;
                } else {
                  return false;
                }
              });
              this.lastTeacherApprove = this.comments[this.comments.length - 1].status === 1
              this.lastTeacherIsSigned = this.comments[this.comments.length - 1].status !== 0 &&
                this.comments[this.comments.length - 1].status !== undefined
            });
          })

        }
      } else {
        window.alert("Unauthorized Access");
        this.navrouter.navigate(["teacher/home"]);
      }
    });
  }

  checkInvalidComment(): boolean {
    //check submit if empty comment show error, then hide after touch comment
    return this.formControl.invalid && this.errCommentEmpty;
  }

  downloadfile(filename: string) {
    this.as.getFile(filename).subscribe(
      res => {
        var url = window.URL.createObjectURL(res);
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = url;
        a.download = filename;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove(); // remove the element
      },
      error => {
      },
      () => {
      }
    );
  }

  getCommentInfo() { }

  isImage(file: iupload_ans): boolean {
    let imageType = [".png", ".jpg", "jpeg"];
    if (imageType.indexOf(file.upload_file) != -1) {
      return true;
    }
    return false;
  }

  getFormInfo() {
    this.as.getAnswerForm(this.id).subscribe(res => {
      if (res) {
        let temp: any = res.req_s;
        this.form = temp.form;
        this.req_ans = temp.req_ans;
        this.student_info = res.std[0];
        this.student_info.degree = res.std.degree;
        this.student_info.title = res.std.title;
        this.student_info.department = res.std.department;
      } else {
        window.alert("Unauthorized Access");
        this.navrouter.navigate(["teacher/home"]);
      }
    });
  }

  onDialog(status: number) {
    if (this.formControl.valid) {
      const dialogRef = this.dialog.open(DialogComponent, {
        width: "300px",
        data: { status: status, form: this.formControl, req_id: this.id }
      });
      dialogRef.afterClosed().subscribe(result => {
        let self = this;
        if (result != "") {
          self.navrouter.navigate(["/teacher/home"]);
        }
      });
    } else {
      this.errCommentEmpty = true;
    }
  }

  back() {
    this.navrouter.navigate(["/teacher/home"]);
  }
}
