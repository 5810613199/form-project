import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule,FormsModule } from "@angular/forms";
import { AdvisorRoutingModule } from './advisor-routing.module';
import { MaterialModule } from '../app.module';
import { HttpClientModule } from "@angular/common/http";

import { HomeComponent } from './home/home.component';
import { NavComponent } from "../shared/nav/nav.component";
import { RequestComponent } from './request/request.component';
import { DialogComponent } from './dialog/dialog.component';
import { SafehtmlComponent } from "../shared/safehtml/safehtml.component";
import { SharedModule } from '../shared/shared.module';
import { TokenInterceptorService } from '../http-interceptors/token-interceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    AdvisorRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    HttpClientModule
  ],
  declarations: [HomeComponent, RequestComponent, DialogComponent],
  entryComponents:[DialogComponent],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true },

  ]
})
export class AdvisorModule { }
