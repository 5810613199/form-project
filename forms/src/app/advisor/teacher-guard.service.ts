import { Injectable } from '@angular/core';
import { AdvisorService } from './advisor.service';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AppService } from '../app.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TeacherGuardService {

  constructor(public service: AdvisorService, public router: Router, public app: AppService) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.service.checkteacher().pipe(map
      (data => {
        if (data) {
          return true;
        } else {
          this.router.navigate(['auth/login'], { queryParams: { returnUrl: state.url }})
          return false;
        }
      }))
  }
}
