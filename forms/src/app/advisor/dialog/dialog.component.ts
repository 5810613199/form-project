import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from "@angular/material";
import { FormControl } from '@angular/forms';
import { icomment, UserInfo, iperson } from "../../utils/interfaces";
import { AdvisorService } from '../advisor.service';
import { AppService } from 'src/app/app.service';

export interface payload {
  status: number,
  form: FormControl,
  req_id: any
}

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {
  confirm: boolean = false;
  finish: boolean = false;
  constructor(@Inject(MAT_DIALOG_DATA) public data: payload,
    public dialogRef: MatDialogRef<DialogComponent>,
    private as: AdvisorService,
    private app: AppService,
  ) { }
  color = 'primary';
  mode = 'indeterminate';
  value = 20;
  isLoading: boolean = false;
  complete: boolean;


  ngOnInit() {
  }

  onConfirm() {
    this.isLoading = true;
    let person: iperson = JSON.parse(this.app.getLocal('personalinfo'))
    this.confirm = true;
    let payload: icomment = {
      user_id: person.person.personid,
      req_id: this.data.req_id,
      comment: this.data.form.value,
      status: this.data.status

    }
    this.as.submitComment(payload).subscribe(res => {
      if (res) {
        this.isLoading = false;
        this.complete = res == true ? true : false;
      }

    })

  }

  onCancel() {
    this.dialogRef.close('');
  }



}
