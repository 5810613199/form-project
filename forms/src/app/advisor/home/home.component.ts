import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { Router } from "@angular/router";
import { animate, state, style, transition, trigger } from '@angular/animations';
import { isObject } from 'util';
import { AdvisorService } from '../advisor.service';
import { iSign_ctrl, iSigner, UserInfo, iperson } from 'src/app/utils/interfaces';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class HomeComponent implements OnInit {

  dataSource = new MatTableDataSource<RequestElement>();
  isDataSourceEmpty: boolean = false;
  selection = new SelectionModel<RequestElement>(true, []);
  ELEMENT_DATA: RequestElement[] = [];
  expandedElement: RequestElement | null;
  columnsToDisplay = ['No', 'Name', 'Semester', 'Date', 'Status'];
  badge: number = 10;
  isLinear: boolean = true;
  completed: any[] = []

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort

  constructor(private router: Router,
    private ts: AdvisorService,
    private apps: AppService) { }

  ngOnInit() {
    this.dataSource.sort = this.sort
    this.paginator._intl.itemsPerPageLabel = "จำนวนรายการ"
    this.dataSource.paginator = this.paginator
    this.getLoadingRequest()
    this.getAllRequests()

  }

  getLoadingRequest() {
    const item: RequestElement = { 'No': NaN, 'Name': null, 'Semester': null, 'Date': null, 'Status': null }
    let element_dat = [item, item, item]
    this.dataSource = new MatTableDataSource<RequestElement>(element_dat)
  }

  refreshData() {
    this.isDataSourceEmpty = false;
    this.getLoadingRequest();
    this.getAllRequests();
  }

  getAllRequests() {
    this.ts.getAllRequests().subscribe(res => {
      let self = this;
      let SignControl: iSign_ctrl[] = [];
      let SignComplete: any[] = [];
      this.ELEMENT_DATA = [];
      let form: any[] = (res)[0]
      form.forEach((el, index) => {
        el.form.Id = el.id
        el.form.Name = el.form.form_name
        el.form.Date = el.created_at
        el.form.No = index + 1
        el.form.Semester = el.semester
        SignControl.push(el.form.sign_control[0])
        this.ELEMENT_DATA.push(el.form)
      })
      // loop for insert all role
      this.ts.getSigner().subscribe(signer => {
        let _sign: iSigner[] = signer
        this.ELEMENT_DATA.forEach((el, index) => {
          let obj = { id: el.Id, completed: [] }
          let n_sign: number = SignControl[index].n_sign;
          let findsigner: iSigner = {}
          let person: iperson = JSON.parse(this.apps.getLocal('personalinfo'))
          el.SignControl = [];
          for (let i = 1; i <= n_sign; i++) {
            findsigner = _sign.find(el => el.personid === SignControl[index][`sign_t${i}`])
            //check own sign
            if (form[index][`sign_t${i}`] === person.person.personid) {
              el.Status = form[index][`sign_t${i}_s`]
            }
            obj.completed.push(form[index][`sign_t${i}_s`])
            el.SignControl.push(findsigner)

          }
          self.completed.push(obj)
        })
        self.ELEMENT_DATA.sort((a, b) => {
          return (new Date(b.Date).getTime() - new Date(a.Date).getTime());
        })
        self.ELEMENT_DATA.sort((a, b) => {
          return a.Status - b.Status;
        })
        self.ELEMENT_DATA.map((el, index) => el.No = index + 1)
        self.ELEMENT_DATA.forEach((dat_el, dat_index) => {
          if (dat_el.Id != this.completed[dat_index].id) {
            let temp = this.completed[dat_index]
            let com_index = this.completed.findIndex(el => el.id == dat_el.Id)
            this.completed[dat_index] = this.completed[com_index]
            this.completed[com_index] = temp
          }
        })
        if (self.ELEMENT_DATA.length) {
          this.isDataSourceEmpty = false;
          self.dataSource = new MatTableDataSource<RequestElement>(self.ELEMENT_DATA)
          self.dataSource.sort = self.sort
          this.dataSource.paginator = this.paginator
        } else {
          this.isDataSourceEmpty = true;
        }
      })


    });
  }

  ngAfterViewChecked() {
    const list = document.getElementsByClassName('mat-paginator-range-label');
    list[0].innerHTML = 'หน้า: ' + (this.paginator.length === 0 ? 0 : this.paginator.pageIndex + 1) + '/' + (Math.ceil(this.paginator.length / this.paginator.pageSize));

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

  }

  disableBadge() {
    this.badge = 0;
  }

  select(row) {
    let url = '/teacher/request/' + row.Id
    this.router.navigate([url])
  }

  ElementHtml(data, type, element): string {
    return data + '%%' + type + '%%' + element.Status;
  }

}
export interface RequestElement {
  Name?: string;
  No?: number;
  Id?: number;
  Student?: string;
  Date?: string;
  Status?: number;
  Semester?: string;
  SignControl?: any[];
  description?: string;
}
