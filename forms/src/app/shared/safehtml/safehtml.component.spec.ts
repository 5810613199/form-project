import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SafehtmlComponent } from './safehtml.component';

describe('SafehtmlComponent', () => {
  let component: SafehtmlComponent;
  let fixture: ComponentFixture<SafehtmlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SafehtmlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SafehtmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
