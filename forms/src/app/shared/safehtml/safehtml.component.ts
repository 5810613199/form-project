import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
  selector: 'safe-html',
  templateUrl: './safehtml.component.html',
  styleUrls: ['./safehtml.component.css']
})
export class SafehtmlComponent implements OnInit {

  @Input() element: string;
  thisHtml: string;
  progress: number;
  safeHtml: SafeHtml;
  progColor: string;
  status: string;
  date: Date;
  type: string;
  isLoading: boolean = false;
  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit() {
    let column: any[] = this.element.split('%%')
    if (column[2] === "null" || column[2] === "undefined") {
      this.isLoading = true;
      this.thisHtml = ""
    }
    else {
      this.isLoading = false;
      this.type = column[0];
      this.progress = Number.parseInt(column[1].toString());

      switch (this.type) {
        case 'Progress': this.thisHtml = ""; break;
        case 'Status': { this.thisHtml = ""; this.status = column[1] }; break;
        case 'Date': { this.thisHtml = ""; this.date = column[1] }; break;
        default: this.thisHtml = column[1]; break;
      }
      this.safeHtml = this.sanitizer.bypassSecurityTrustHtml(
        this.thisHtml
      )

      if (column[2] === '1') {
        this.progColor = 'accent'
      } else if (column[2] === '2') {
        this.progColor = 'warn'
      } else {
        this.progColor = 'primary'
      }
    }
  }

  checkProgress() {
    // if()
  }

}
