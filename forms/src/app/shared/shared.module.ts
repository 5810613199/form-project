import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";

import { SharedRoutingModule } from './shared-routing.module';
import { NavComponent } from './nav/nav.component';
import { SafehtmlComponent } from './safehtml/safehtml.component';
import { MaterialModule } from "../app.module";
import { LoaderComponent } from './loader/loader.component';

@NgModule({
  imports: [
    CommonModule,
    SharedRoutingModule,
    // BrowserModule,
    MaterialModule,
    FormsModule
  ],
  declarations: [NavComponent, SafehtmlComponent, LoaderComponent,  ],
  exports:[
    NavComponent,
    SafehtmlComponent,
    LoaderComponent
  
  ]
})
export class SharedModule { }
