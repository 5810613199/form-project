import { Component, OnInit } from "@angular/core";
import {
  Router,
  ActivatedRoute,
  RoutesRecognized,
  NavigationEnd
} from "@angular/router";
import { AppService } from "../../app.service";
import { User, UserInfo } from "../../utils/interfaces";
import { AuthService } from "src/app/auth/auth.service";

@Component({
  selector: "app-nav",
  templateUrl: "./nav.component.html",
  styleUrls: ["./nav.component.css"]
})
export class NavComponent implements OnInit {
  links: any[];
  linkTeacher: any[];
  linkStudent: any[];
  linkAdmin: any[];
  activeLink;
  currentLink = "";
  prevRoute: string;
  user: UserInfo;
  submenu: boolean = false;
  toggle: boolean = false;

  constructor(
    private app: AppService,
    private router: Router,
    private activedRouter: ActivatedRoute,
    private authservice: AuthService
  ) { }

  ngOnInit() {
    this.links = [
      { label: "หน้าหลัก", url: "staff/home" },
      { label: "แบบคำร้อง", url: "staff/manage" },
      { label: "สถิติ", url: "staff/stat" }
    ];
    this.linkTeacher = [{ label: "หน้าหลัก", url: "teacher/home" }];
    this.linkStudent = [
      { label: "หน้าหลัก", url: "student/home" },
      { label: "ประวัติคำร้อง", url: "student/history" }
    ];
    this.linkAdmin = [
      { label: "รายชื่อผู้พิจารณา", url: "admin/home" },
      { label: "สิทธิ์การจัดการ", url: "admin/oplv" }
    ];
    this.user = JSON.parse(this.app.getLocal('userinfo'));
    let role = this.router.url;
    switch (this.user.role) {
      case 'teacher': { this.links = this.linkTeacher }; break;
      case 'student': { this.links = this.linkStudent }; break;
      case 'staff': break;      
      default: { this.links = this.linkAdmin }; break;
    }
    if (!!this.links.find(el => '/' + el.url === this.app.prevUrl)) {
      this.toggle = true;

    } else {
      this.toggle = false;
    }
    this.currentLink = this.activedRouter.snapshot.url[0].path;
    this.activeLink = this.links.find(el => {
      return el.url.indexOf(this.currentLink) != -1;
    });
  }

  navigate(link) {
    if (link != this.activeLink) {
      this.router.navigate([link.url]);
    }
  }

  logout() {
    this.router.navigate(["auth/login"]);
  }

  toggled() {
    this.toggle = !this.toggle;
  }

  log(a) {
  }
}
