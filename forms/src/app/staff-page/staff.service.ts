import { Injectable } from '@angular/core';
import { AppService } from "../app.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { iform, UserInfo, User, icomment, iperson } from "../utils/interfaces";
import { Http, Headers, RequestOptions, ResponseContentType } from "@angular/http";
// import {  } from "@angular/";
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StaffService {

  api: string = 'http://formapi.pro/'
  options: RequestOptions
  constructor(private app: AppService, private _http: HttpClient) {
  }

  private _setHeader(): HttpHeaders {
    let username = this.app.getLocal('username')
    let header = new HttpHeaders({
      'Content-Type': 'application/json',
      'username': username + ''
    }
    )
    return header;
  }

  checkstaff(): Observable<boolean> {
    let url = this.api + 'api/checkstaff'
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get<boolean>(url, httpOptions).pipe(
      map(res => { return res }),
      catchError(err => {
        return this.recheckstaff()
      }))
  }

  private recheckstaff(): Observable<boolean> {
    let url = this.api + 'api/checkstaff'
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get<boolean>(url, httpOptions).pipe(
      map(res => { return res }),
      catchError(err => {
        return of(false)
      })
    )
  }

  checkCanManage(): Observable<any> {
    let person: iperson = JSON.parse(this.app.getLocal('personalinfo'))
    let url = this.api + 'api/checkCanManage/' + person.person.personid
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.post(url, '', httpOptions).pipe(map(res => {
      if (res) {
        return true;
      } else {
        return false;
      }
    }), catchError(err => {
      return of(false).pipe(map(res => { return false }));
    }))
  }

  createForm(form: iform): Observable<any> {
    let url = this.api + 'api/formcreate'
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.post(url, JSON.stringify(form), httpOptions)
      .pipe(map(res => {
        return res
      }), catchError(err => {
        return of(false)
      }));
  }

  getFormById(form_id: number): Observable<any> {
    let url = this.api + 'api/getFormByID/' + form_id;
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url, httpOptions).pipe(map(res => {
      return res;
    }), catchError(err => {
      return of(null)
    }));;
  }

  getAllRequests(): Observable<any> {
    let staff: iperson = JSON.parse(this.app.getLocal('personalinfo'))
    let url = this.api + 'api/getAllRequests/' + staff.person.personid
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url, httpOptions).pipe(map(res => {
      return res;
    }), catchError(err => {
      return of([[], []])
    }));;

  }

  getTeacherStaff(): Observable<any> {
    let url = this.api + 'api/getTeacherStaff'
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url)
  }

  getSigner(): Observable<any> {
    let url = this.api + 'api/getSigner'
    let token = JSON.parse(this.app.getLocal('accessToken'))
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url, httpOptions).pipe(map(res => {
      return res;
    }), catchError(err => {
      return of([])
    }));
  }

  getRequestByID(req_id: number): Observable<any> {
    let url = this.api + 'api/getRequestById/' + req_id
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url, httpOptions)
      .pipe(map(res => {
        return res
      }), catchError(err => {
        return of(null)
      }));
  }

  getAnswerForm(req_id: number): Observable<any> {
    let url = this.api + 'api/getAnswerForm/' + req_id
    let httpOptions = { headers: this._setHeader() }
    return this._http.get(url, httpOptions)
      .pipe(map(res => {
        return res
      }), catchError(err => {
        return of(null)
      }));
  }

  getAllForms(): Observable<any> {
    let url = this.api + 'api/getAllForms'
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url, httpOptions)
      .pipe(map(res => {
        return res
      }), catchError(err => {
        return of([])
      }));
  }

  getInfoRequest(): Observable<any> {
    let url = this.api + 'api/getInfoReq'
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url, httpOptions)
      .pipe(map(res => {
        return res
      }), catchError(err => {
        return of([])
      }));
  }

  getFile(filename: string): Observable<any> {
    let url = this.api + 'api/downloadfile/' + filename
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url, {
      headers: this._setHeader(),
      responseType: 'blob'
    })

  }

  updateForm(form: iform): Observable<any> {
    let url = this.api + 'api/formupdate'
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.put(url, JSON.stringify(form), httpOptions)
      .pipe(map(res => {
        return res
      }), catchError(err => {
        return of(false)
      }));
  }

  updateFormName(form_id: number, form_name: string): Observable<any> {
    let reitem = { form_id: form_id, form_name: form_name }
    let url = this.api + 'api/updateFormName'
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.put(url, JSON.stringify(reitem), httpOptions)
      .pipe(map(res => {
        return res
      }), catchError(err => {
        return of(false)
      }));
  }

  updatePosition(prev: number, curr: number): Observable<any> {
    let repos = {
      "previous_form_id": prev,
      "current_form_id": curr
    }
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let options = new RequestOptions({ headers: headers });
    let url = this.api + 'api/updatePosition'
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.put(url, JSON.stringify(repos), httpOptions)
      .pipe(map(res => {
        return res
      }), catchError(err => {
        return of(false)
      }));

  }

  deleteForm(form_id: number): Observable<any> {
    let url = this.api + 'api/form/' + form_id
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.delete(url, httpOptions)
      .pipe(map(res => {
        return res
      }), catchError(err => {
        return of(false)
      }));
  }


  submitComment(comment: icomment): Observable<any> {
    let url = this.api + 'api/submitComment'
    let formData = new FormData;
    formData.append('user_id', comment.user_id.toString())
    formData.append('req_id', comment.req_id.toString())
    formData.append('comment', comment.comment.toString())
    formData.append('status', comment.status.toString())
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.post(url, JSON.stringify(comment), httpOptions)
      .pipe(map(res => {
        return res
      }), catchError(err => {
        return of(false)
      }));
  }

}
