import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { question, option, options, iform, iquestion, ioption, iupload, iposition, isignctrl, iSigner } from "../../utils/interfaces";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { AppService } from "../../app.service";
import { StaffOrderComponent } from '../staff-order/staff-order.component';
import { CreateConfirmComponent } from '../create-confirm/create-confirm.component';
import { FormControl, Validators, FormArray, FormGroup } from '@angular/forms';
import { StaffService } from '../staff.service';
import { formArrayNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';
import { of } from 'rxjs';

@Component({
  selector: 'app-create-form',
  templateUrl: './create-form.component.html',
  styleUrls: ['./create-form.component.css']
})
export class CreateFormComponent implements OnInit {

  formGroup: FormGroup = new FormGroup({
    form_name: new FormControl('', Validators.required),
    description: new FormControl('')
  })
  form: iform = {};
  questions: question[] = [];
  signcontrol: isignctrl = {};
  options: options[] = [];
  listType: string[] = [];
  fileType: string[] = [];
  orders: any[] = [];
  numfile: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  formid: any;
  isquestion: number = NaN;
  url: string;
  positions: iposition[] = [];
  countSbj: number = 0;
  disabledSubject: boolean = false;
  errFormNotValid: boolean = false;
  errOrders: boolean = false;




  constructor(public dialog: MatDialog,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private app: AppService, private ss: StaffService) {
    this.listType = ['radio button', 'checkboxes', 'dropdown', 'short answer', 'large answer', 'file upload', 'subject'];
    this.fileType = ['.pdf', '.jpg,.jpeg', '.png', '.xls,.xlsx', '.doc,.docx', 'อื่น ๆ'];
    // Microsoft Excel Document (.xls)
    // Microsoft Excel Open XML Document (.xlsx)
    // Microsoft Word Document (.doc)
    // Microsoft Word Open XML Document (.docx)
  }

  ngOnInit() {
    this.url = this.activeRoute.snapshot.url[0].path;
    if (this.url === 'edit') {
      this.formid = this.activeRoute.snapshot.url[1].path;
      this.ss.getFormById(this.formid).subscribe(res => {
        this.form = res
        this.formGroup = new FormGroup({
          form_name: new FormControl(this.form.form_name, Validators.required),
          description: new FormControl(this.form.description || '')
        })
        this.formBackConvert()
      })
    }
    this.getSigner();
    for (let i = 1; i <= 5; i++) {
      this.signcontrol['sign_t' + i] = null;
    }
  }

  cancel() {
    this.router.navigate(['staff/manage'])
  }

  onOrder() {
    let dialogBoxSettings = {
      height: '400px',
      width: '300px',
      margin: '0 auto',
      data: { order: this.orders }
    };

    const dialogRef = this.dialog.open(StaffOrderComponent, dialogBoxSettings);

    dialogRef.afterClosed().subscribe(result => {
      this.orders = result === undefined ? this.orders : result;

      this.orders = this.orders.filter(el => {
        if (el.selectFormControl.value === "" || el.selectFormControl.value === undefined) {
          return false
        }
        return true
      })
      // this.canSelectTypeSubject()
      let foundTeacher = this.positions.find(el => el.pos_detail === 'อาจารย์ประจำวิชา')
      if (this.disabledSubject) {
        if (!this.orders.find(el => el.selectFormControl.value === foundTeacher.pos_detail)) {
          let obj = { selectFormControl: new FormControl(foundTeacher.pos_detail, Validators.required) };
          this.orders.push(obj);
        }
      }

      if (this.countSbj == 0) {
        this.orders = this.orders.filter(el => el.selectFormControl.value != foundTeacher.pos_detail)
      } else {
        // 'อาจารย์ประจำวิชา' เลือกมาหลายคน 
        let tempOrders = []
        let countTeacher = 0
        this.orders.forEach(el => {
          if (el.selectFormControl.value === foundTeacher.pos_detail) {
            countTeacher++;
            if (countTeacher === 1) {
              tempOrders.push(el)
            }
          } else {
            tempOrders.push(el)
          }
        })
        this.orders = tempOrders
      }

      this.mapOrders()
    });

  }

  isQuestion(qno: number) {
    this.isquestion = qno;
    let elmnt = document.getElementById('qno' + qno)
    // elmnt.scrollIntoView({ block: 'center', behavior: 'smooth' });
  }

  mapOrders() {
    this.ss.getSigner().subscribe(res => {
      let positions: iSigner[] = res
      if (this.orders.length) {
        this.signcontrol.n_sign = this.orders.length
        this.orders.forEach((el, index) => {
          let item: iSigner = positions.find(x => x.pos_detail == el.selectFormControl.value)
          let s = `sign_t${index + 1}`
          this.signcontrol[s] = item.personid
        })
      } else {
        let n_sign = this.signcontrol.n_sign        
        for (let i = 1; i <= n_sign; i++) {
          let s = `sign_t${i}`
          let role = positions.find(el => el.personid === this.signcontrol[s]).pos_detail
          let obj = { selectFormControl: new FormControl(role, Validators.required) };
          this.orders.push(obj);
        }
      }

    })
  }

  formBackConvert() {

    let formquestion: iquestion[] = this.form.form_question;
    formquestion.forEach(el => {
      // {"no":1,"title":null,"type":"radio button","ansreq":false,"hasOther":false} -----questions
      let question: question = {
        id: el.id,
        no: el.question_seq,
        title: new FormControl(el.question_detail, Validators.required),
        type: el.question_type.toLocaleLowerCase(),
        ansreq: el.req_ans == 1 ? true : false,
        hasOther: el.other == 1 ? true : false,
        numfile: el.upload_q.length > 0 ? el.upload_q[0].n_file : NaN,
        filetype: el.upload_q.length > 0 ? el.upload_q[0].file_type.split("/") : [],
        fileControl: el.upload_q.length > 0 ? new FormControl(el.upload_q[0].file_type.split("/")) : new FormControl([])
      }
      // if (question.type === 'subject') {
      // }
      this.questions.push(question)
      if (el.question_choice.length) {
        let optarr: ioption[] = el.question_choice
        let optionobj: option[] = []
        optarr.forEach(op => {
          optionobj.push({ no: op.choice_seq, answer: new FormControl(op.choice_detail, Validators.required) })
        })
        this.options.push({ qno: el.question_seq, option: optionobj })
      }


    })
    this.signcontrol = this.form.sign_control[0]
    this.canSelectTypeSubject();
    this.mapOrders();
  }

  check() {
  }



  submit() {
    this.mapOrders()
    let backup: iquestion[] = this.url === 'edit' ? Object.assign([], this.form.form_question) : [];
    this.form.form_name = this.formGroup.get('form_name').value
    this.form.description = this.formGroup.get('description').value
    // this.form.create_by = JSON.parse(this.app.getLocal('userinfo')).name
    this.form.create_by = "kriszz"
    // this.form.mod_by = JSON.parse(this.app.getLocal('userinfo')).name
    this.form.mod_by = "kriszz"
    this.form.form_question = [];
    this.form.sign_control = [];
    this.form.sign_control.push(this.signcontrol);
    this.questions.forEach((el, index) => {
      let q: iquestion = {};
      let optionarr: ioption[] = [];

      this.form.form_question.push(q)
      this.form.form_question[index].id = backup.length > 0 ? backup[index].id : undefined
      this.form.form_question[index].question_seq = el.no
      this.form.form_question[index].question_detail = el.title.value
      this.form.form_question[index].question_type = el.type
      this.form.form_question[index].req_ans = el.ansreq === true || el.question_type === 'subject' ? 1 : 0;
      this.form.form_question[index].other = el.hasOther === true ? 1 : 0;
      this.form.form_question[index].upload_q = [];
      this.form.form_question[index].n_choice = 0;
      this.form.form_question[index].question_choice = [];

      let opval: options = this.options.find(op => op.qno === el.no)
      if (opval != undefined) {
        opval.option.forEach((op, opIndex) => {
          let ioption: ioption = {};
          let newobj: option = Object.assign({}, op);
          ioption.id = backup.length >0? (!backup[index].question_choice[opIndex])  ? undefined:backup[index].question_choice[opIndex].id : undefined;
          ioption.question_id = backup.length > 0 && (backup[index].question_choice.length > 0) ? backup[index].id : undefined
          ioption.choice_seq = op.no
          ioption.choice_detail = op.answer.value
          optionarr.push(ioption)
        })
        this.form.form_question[index].n_choice = optionarr.length
        this.form.form_question[index].question_choice = optionarr
      }
      if (el.type == 'file upload') {
        let fileType: string = "";
        el.fileControl.value.forEach((f, index) => { fileType += (index !== el.fileControl.value.length - 1) ? f + "/" : f })
        let upload: iupload = {
          file_type: fileType,
          n_file: el.numfile,
          question_id: backup.length > 0 && (backup[index].upload_q.length > 0) ? backup[index].id : undefined,
          form_id: this.formid,
          id: (backup.length > 0) && (backup[index].upload_q.length > 0) ? backup[index].upload_q[0].id : undefined,
        };
        this.form.form_question[index].upload_q.push(upload)
      }


    })
    
    if (this.checkValidForm()) {
      const dialogRef = this.dialog.open(CreateConfirmComponent, {
        width: '350px',
        data: { order: this.url, form: this.form }
      })

      dialogRef.afterClosed().subscribe(result => {
        if (result != "") {
          this.router.navigate(['staff/manage'])
        }
      })
    } else {
      this.errFormNotValid = true;
    }

  }

  checkValidForm(): boolean {
    let noQuestion = this.questions.find(el => el.title.invalid == true)
    let option: boolean = true;
    //no question If undefined = valid
    //no option If true = valid
    this.options.forEach(el => {
      let findInvalidOption = el.option.find(x => x.answer.invalid == true)
      if (findInvalidOption) {
        option = false
      }
    })
    return (noQuestion === undefined) && option && this.formGroup.valid;
  }



  addQuestion() {
    let questionObj = { no: this.questions.length + 1, title: new FormControl('', Validators.required), type: 'radio button', ansreq: false, hasOther: false, };
    this.questions.push(questionObj);
    this.options.push({ qno: questionObj.no, option: [] });
    this.addOption(questionObj.no);

  }
  addOption(qno: number) {
    let newobj = Object.assign({}, this.options.find(el => el.qno === qno))
    newobj.option.push({ no: newobj.option.length + 1, answer: new FormControl('', Validators.required) })
  }

  addOTHER(qno: number) {
    this.questions[qno - 1].hasOther = !this.questions[qno - 1].hasOther
  }

  getSigner() {
    this.ss.getSigner().subscribe(res => {
      this.positions = (res)
    })
  }

  isSubject(no: number): boolean {
    return !!this.questions.find(el => {
      if (el.type === 'subject' && el.no === no) {
        return true;
      }
      return false;
    })
  }

  canSelectTypeSubject() {
    let count = 0;
    this.questions.forEach(el => {
      if (this.isSubject(el.no)) {
        count++;
      }
    }
    )
    this.countSbj = count;
    this.disabledSubject = count >= 1 && this.orders.length===5 ? true : false
    // return count > 1 ? false : true;
  }


  selectType(type: string, no: number) {
    this.canSelectTypeSubject()
    let type$ = this.listType.find(el => el === type);
    let question = this.questions.find(el => el.no === no)
    let canSetType = true;
    this.errOrders = false;
    let foundTeacher = this.positions.find(el => el.pos_detail === 'อาจารย์ประจำวิชา')
    if (type$ === 'short answer' || type$ === 'large answer' || type$ === 'file upload' || type$ === 'subject') {
      this.options = (this.options.filter(ele => ele.qno !== no));
      this.questions[no - 1].hasOther = false;
      if (type$ === 'file upload') {
        this.questions[no - 1].numfile = NaN
        this.questions[no - 1].filetype = []
        this.questions[no - 1].fileControl = new FormControl(this.questions[no - 1].filetype)

      }
      else if (type$ === 'subject') {
        if (this.countSbj === 1 && this.orders.length < 5) {
          if (!this.orders.find(el => el.selectFormControl.value === foundTeacher.pos_detail)) {
            let obj = { selectFormControl: new FormControl(foundTeacher.pos_detail, Validators.required) };
            this.orders.push(obj);
          }
          this.disabledSubject = true;
          canSetType = false;
          this.questions[no - 1].type = type$;
          this.questions[no - 1].ansreq = true;
        } else {
          this.errOrders = true;
          this.questions[no - 1].type = 'radio button';
          canSetType = false;
        }
      }

    }



    if (type$ === 'dropdown' || type$ === 'checkboxes' || type$ === 'radio button') {
      //change from subject to hasoption

      if (this.countSbj < 1) {
        let index = this.orders.findIndex(el => el.selectFormControl.value === foundTeacher.pos_detail);
        if (!isNaN(index)) {
          this.orders.splice(index, 1);
        }
      }

      if (!this.checkHasOption(no)) {
        let newobj: options = { qno: no, option: [] };
        this.options.push(newobj);
        this.addOption(no);
        this.options.sort((a, b) => a.qno - b.qno)
        this.questions[no - 1].numfile = NaN
        this.questions[no - 1].filetype = []
        this.questions[no - 1].fileControl = new FormControl([])

      }
    }
    if (canSetType) {
      this.questions[no - 1].type = type$;
      // this.disabledSubject = false;
    }



  }

  copyQuestion(no: number) {
    // check one type subject please
    if (!this.isSubject(no)) {
      let newObj = { 'no': null };
      newObj = Object.assign(newObj, this.questions[no - 1]);
      let temp = this.questions.slice(no);
      this.questions.forEach(itemq => {
        temp.forEach(itemt => {
          if (itemt === itemq) {
            itemt.no++;
          }
        })
      })

      newObj.no = no + 1;
      this.questions.push(newObj);
      this.questions.sort((a, b) => a.no - b.no)
      //copy option
      this.copyAllOption(no)
    } else {
    }


  }

  copyAllOption(no: number) {

    let seloption = this.options.find(el => el.qno === no);
    if (seloption) {
      let newarr: option[] = [];
      seloption.option.forEach(el => {
        let item = Object.assign({}, el)
        newarr.push(item)
      })
      let newobj: options = Object.assign({}, seloption);
      this.options.forEach(el => {
        if (el.qno > no) {
          el.qno++
        }
      })
      newobj.qno = seloption.qno + 1;
      newobj.option = newarr;
      this.options.push(newobj);


    } else {
      this.options.forEach(el => {
        if (el.qno > no) {
          el.qno++
        }
      })
    }
    this.options.sort((a, b) => a.qno - b.qno)
  }

  checkHasOption(no) {
    let listNoOption = ['short answer', 'large answer', 'file upload']
    // return !listNoOption.includes(type);
    return this.options.find(el => el.qno === no)
  }

  removeQuestion(no: number) {

    if (this.isSubject(no)) {
      this.countSbj--;
      let index = this.orders.findIndex(el => el.selectFormControl.value === 'อาจารย์ประจำวิชา')
      this.orders.splice(index, 1)
    }
    let temp = this.questions.slice(no);
    this.questions.forEach(itemq => {
      temp.forEach(itemt => {
        if (itemt === itemq) {
          itemt.no--;
        }
      })
    })
    this.questions.splice(no - 1, 1);

    let findIndex = this.options.findIndex(el => el.qno === no)
    if (findIndex != -1) {
      this.options.splice(findIndex, 1)

    }
    this.options.forEach((el, index) => {
      if (el.qno > no) {
        this.options[index].qno--;
      }
    })
    this.canSelectTypeSubject()

  }

  deleteOption(qno: number, optno: number) {
    let obj = this.options.find(el => el.qno === qno)
    let temp = obj.option.slice(optno)
    obj.option.forEach(el => {
      temp.forEach(elt => {
        if (elt.no === el.no) {
          el.no--
        }
      })
    })
    obj.option.splice(optno - 1, 1)
    if (obj.option.length === 0) {
      this.questions[qno - 1].hasOther = false;
    }

  }

  hasOther(qno: number) {
    if (this.options.find(el => el.qno === qno).option.length === 0) {
      this.questions[qno - 1].hasOther = false;
    } else {
      this.questions[qno - 1].hasOther = !this.questions[qno - 1].hasOther
    }

  }

  deleteAllOption(qno: number) {
    let obj = this.options.find(el => el.qno === qno)
    obj.option = []

  }

  sorting() {
    this.questions.sort((a, b) => a.no - b.no);
  }

  log(item) {
  }

  trackByFn(index, value) {
    return index;
  }

  get diagnostic() { return JSON.stringify(this.questions); }


}
