import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { form } from "../../utils/interfaces";
import { StaffService } from '../staff.service';
import { FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-manage-request',
  templateUrl: './manage-request.component.html',
  styleUrls: ['./manage-request.component.css']
})
export class ManageRequestComponent implements OnInit {
  drag: boolean = false;

  forms: form[] = []
  isRename: boolean = false;
  backup: string = "";
  backupIndex: number = NaN;
  editIndex = NaN;
  faCoffee = 'faCoffee';
  form_rename: FormControl;
  errUpdatePosition: boolean = false;



  constructor(private ss: StaffService) { }

  ngOnInit() {
    this.form_rename = new FormControl('')
    this.getAllForms()
  }

  getAllForms() {
    this.ss.getAllForms().subscribe(res => {
      this.forms = res
    })
  }



  drop(event: CdkDragDrop<string[]>) {
    if (event.previousIndex != event.currentIndex) {
      let pre_id = this.forms[event.previousIndex].id
      let curr_id = this.forms[event.currentIndex].id
      moveItemInArray(this.forms, event.previousIndex, event.currentIndex);
      this.ss.updatePosition(pre_id, curr_id).subscribe(res => {
        if (!res) {
          this.errUpdatePosition = false
          moveItemInArray(this.forms, event.currentIndex, event.previousIndex);
        }
      })
    }

    if (this.isRename) {
      this.checkEditIndex(event.previousIndex, event.currentIndex);
    }




  }

  checkEditIndex(prevIndex, currIndex) {
    if (this.editIndex === prevIndex) {
      this.editIndex = currIndex;
    }
  }

  trackByFn(index, item) {
    return index
  }

  changeOrder() {
    this.drag = !this.drag
  }

  delform(id: number) {
    this.ss.deleteForm(id).subscribe(res => {
      //return true/false
      if (res)
        this.forms = this.forms.filter(el => el.id != id)
    })
  }

  toggleRename(index: number) {
    if (isNaN(this.backupIndex)) {
      this.isRename = true;
    } else {
      this.cancelRename(this.backupIndex)
      this.backupIndex = NaN;
    }
    if (this.isRename) {
      this.editIndex = index;
      this.form_rename = new FormControl(this.forms[index].form_name, Validators.required)
      this.backup = this.forms[index].form_name
      this.backupIndex = index;
    }
  }

  confirmRename(index: number) {
    this.isRename = false;
    this.backupIndex = NaN;
    if (!this.form_rename.invalid) {
      this.ss.updateFormName(this.forms[index].id, this.form_rename.value).subscribe(res => {
        if (res) {
          this.forms[index].form_name = this.form_rename.value
        }
      })
    } else {
      this.cancelRename(index)
    }

  }

  rename(newvalue: string, index: number) {
    this.forms[index].form_name = newvalue;
  }

  cancelRename(i) {
    this.forms[i].form_name = this.backup;
    this.backupIndex = NaN;
    // this.backup = "";
    this.isRename = false;
  }


  get diagnostic() {
    return JSON.stringify(this.forms)
  }
}
