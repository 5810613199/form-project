import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { Router } from "@angular/router";
import { FormControl } from '@angular/forms';
import { StaffService } from '../staff.service';
import { icomment } from 'src/app/utils/interfaces';

export interface payload {
  status: string;
  data: icomment
}

@Component({
  selector: 'app-request-confirm',
  templateUrl: './request-confirm.component.html',
  styleUrls: ['./request-confirm.component.css']
})
export class RequestConfirmComponent implements OnInit {
  isLoading: boolean = false;
  complete: boolean;
  submit: boolean = false;

  constructor(public dialogRef: MatDialogRef<RequestConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: payload, private router: Router,
    private ss: StaffService) { }

  ngOnInit() {
  }

  onCancel() {
    this.dialogRef.close('')
  }

  onConfirm() {
    this.isLoading = true;
    this.submit = true;
    // status ? this.router.navigate(['/staff/home']) : null;
    this.ss.submitComment(this.data.data).subscribe(res => {
      this.isLoading = false;
      this.complete = res === true ? true : false;
    }, err => {
      this.isLoading = false;
      this.complete = false
    })

  }

  status() {
    let colorgreen = "'color:greenyellow'"
    let colorred = "'color:red'"
    // <div style="color:greenyellow">ผ่าน</div>
    return this.data ? '<div style=' + colorgreen + '>ผ่าน</div>' : '<div style=' + colorred + '>แก้ไข</div>'
  }
}
