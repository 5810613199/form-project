import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";

import { StaffPageRoutingModule } from './staff-page-routing.module';
import { CreateFormComponent } from './create-form/create-form.component';
import { StaffOrderComponent } from './staff-order/staff-order.component';
import { MaterialModule } from '../app.module';
import { CreateConfirmComponent } from './create-confirm/create-confirm.component';
import { RequestComponent } from './request/request.component';
import { HomeComponent } from './home/home.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { RequestConfirmComponent } from './request-confirm/request-confirm.component';
import { StatComponent } from './stat/stat.component';
// import { ChartsModule } from "ng2-charts";
import { ChartModule } from "angular2-chartjs";
import { SharedModule } from "../shared/shared.module";

import { ManageRequestComponent } from './manage-request/manage-request.component';
import { DragDropModule } from "@angular/cdk/drag-drop";
import { TokenInterceptorService } from '../http-interceptors/token-interceptor.service';




@NgModule({
  imports: [
    CommonModule,
    StaffPageRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    ChartModule,
    DragDropModule,
    SharedModule,
    HttpClientModule
  ],
  declarations: [CreateFormComponent, StaffOrderComponent, CreateConfirmComponent, RequestComponent, HomeComponent, RequestConfirmComponent, StatComponent, ManageRequestComponent,],
  entryComponents: [
    StaffOrderComponent,
    CreateConfirmComponent,
    RequestConfirmComponent,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true },

  ]
})
export class StaffPageModule { }
