import { Component, OnInit, ViewChild } from '@angular/core';
// import { ChartOptions, ChartType, ChartDataSets, Chart } from 'chart.js';
// import * as pluginDataLabels from 'chartjs-plugin-datalabels';
// import { BaseChartDirective } from 'ng2-charts/ng2-charts';
import { StaffService } from '../staff.service';
import { ireq_status, ichartData, iresult_con, form, ireq_stat, iform } from 'src/app/utils/interfaces';


@Component({
  selector: 'app-stat',
  templateUrl: './stat.component.html',
  styleUrls: ['./stat.component.css']
})
export class StatComponent implements OnInit {
  // @ViewChild(BaseChartDirective) chart: BaseChartDirective;
  typeChart: any;
  dataChart: any;
  optionsChart: any;
  reqstatus: ireq_status[] = []
  termValue: string = '';
  yearValue: string = '';
  formValue: string = '';
  typeValue: any = '';
  semesters: any[] = [];
  terms: string[] = []
  years: string[] = [];
  forms: form[] = [];
  types: string[] = [
    'สถิติการดำเนินการ',
    'สถิติการพิจารณาคำร้อง'
  ];


  constructor(private ss: StaffService) { }

  ngOnInit() {
    this.typeChart = 'bar';
    this.optionsChart = {
      responsive: true,
      maintainAspectRatio: false
    };

    this.terms.push('ทั้งหมด')
    this.years.push('ทั้งหมด')
    this.forms.push({ form_name: 'ทั้งหมด' })
    this.getInfoRequest()
    this.reset()
  }

  onChangeSelect(event) {
    if (this.typeValue === 'สถิติการดำเนินการ') {
      this.getPeriodOfConsiderPetition(this.yearValue, this.termValue, this.formValue)
    } else if (this.typeValue === 'สถิติการพิจารณาคำร้อง') {
      this.getResultOfConsiderPetition(this.yearValue, this.termValue, this.formValue)
    }
  }

  getInfoRequest() {
    this.ss.getInfoRequest().subscribe(res => {
      let reqstat: ireq_stat[] = res
      reqstat.forEach(el => {
        if (el.req_item.status === "complete") {
          this.reqstatus.push(el)
          //store all term
          let term = el.req_item.semester.split('/')
          let sterm = this.terms.find(t => t === term[0])
          if (!sterm) {
            this.terms.push(term[0])
            this.terms.sort((a, b) => {
              return ('' + b).localeCompare('' + a)
            })
          }

          //store all year
          let create_year = new Date(el.req_item.updated_at).getFullYear() + 543
          let syear = this.years.find(year => year === create_year + '')
          if (!syear) {
            this.years.push((create_year).toString())
            this.years.sort((a, b) => {
              return ('' + b).localeCompare('' + a)
            })
          }

          //store all form
          let el_from: iform = el.req_item.form
          let sform = this.forms.find(f => f.id === el_from.id)
          if (!sform) {
            this.forms.push(el_from)
          }
        }
      })
      this.getPeriodOfConsiderPetition(this.yearValue, this.termValue, this.formValue)
    })
  }

  private setChartData(datachart: ichartData[], labels: string[], labely: string = "# No.", labelx: string = "# No.") {
    let maxvalue = 0
    datachart.forEach(el => {
      el.data.forEach(x => {
        if (maxvalue < Number.parseInt(x.toString())) {
          maxvalue = Number.parseInt(x.toString())
        }
      })
    })
    this.dataChart = {
      labels: labels,
      datasets: datachart,

    };
    this.optionsChart = {
      responsive: true,
      maintainAspectRatio: false,
      legend: {
        labels: {
          // This more specific font property overrides the global property

          fontFamily: 'thai-sanslite',
          fontSize: 17,

        },

      },
      scales: {
        xAxes: [{
          gridLines: {
            display: false,
            offsetGridLines: true
          },
          scaleLabel: {
            display: true,
            labelString: labelx,
            fontFamily: 'thai-sanslite',
            fontSize: 17,
          },
          barPercentage: 0.5,
          barThickness: 40,
          maxBarThickness: 40,


        }],
        yAxes: [{
          gridLines: {
            display: false,
          },
          scaleLabel: {
            display: true,
            labelString: labely,
            fontFamily: 'thai-sanslite',
            fontSize: 17,
          },
          minBarLength: 10,
          ticks: {
            max: maxvalue + (5 - maxvalue % 10),
            padding: 20,
          },

        }]
      },
      animation: {
        duration: 1,
        onComplete: function () {
          var chartInstance = this.chart,
            ctx = chartInstance.ctx;
          ctx.textAlign = 'center';
          ctx.textBaseline = 'bottom';

          this.data.datasets.forEach(function (dataset, i) {
            var meta = chartInstance.controller.getDatasetMeta(i);
            meta.data.forEach(function (bar, index) {
              var data = dataset.data[index];
              ctx.fillText(data, bar._model.x, bar._model.y - 5);
            });
          });
        }
      }
    };


  }

  getResultOfConsiderPetition(selYear: string, selTerm: string, selForm: string) {
    let labelsYear = []
    let dat_sign: iresult_con[] = []
    let filterStat = Object.assign([], this.reqstatus)
    if (selYear != "ทั้งหมด") {
      filterStat = filterStat.filter(el => (new Date(el.req_item.created_at).getFullYear() + 543).toString() === selYear)
    }
    if (selTerm != "ทั้งหมด") {
      filterStat = filterStat.filter(el => {
        let term = el.req_item.semester.split('/')
        return term[0] === selTerm
      })
    }
    if (selForm != "ทั้งหมด") {
      filterStat = filterStat.filter(el => {
        let form: iform = el.req_item.form
        return form.form_name === selForm
      })
    }
    filterStat.forEach(el => {
      let year = new Date(el.req_item.updated_at).getFullYear()
      if (!labelsYear.find(x => x == year + 543)) {
        labelsYear.push((year + 543).toString())
        dat_sign.push({ approve: 0, unapprove: 0, label: (year).toString() })
      }
      for (let i = 5; i >= 1; i--) {
        let find = dat_sign.find(item => item.label == year.toString())
        if (el.req_item[`sign_t${i}_s`] != null && el.req_item[`sign_t${i}_s`] == 1) {
          find.approve++;
          break;
        } else if (el.req_item[`sign_t${i}_s`] != null && el.req_item[`sign_t${i}_s`] == 2) {
          find.unapprove++;
          break;
        }
      }
    })

    let Data: ichartData[] = [
      {
        data: [], label: "อนุมัติ",
        backgroundColor: ['rgba(54, 162, 235, 0.2)',],
        borderColor: ['rgba(54, 162, 235, 1)'],
        borderWidth: 1
      },
      {
        data: [], label: "ไม่อนุมัติ",
        backgroundColor: ['rgba(255, 99, 132, 0.2)'],
        borderColor: ['rgba(255, 99, 132, 1)'],
        borderWidth: 1
      }
    ]

    dat_sign.forEach(el => {
      Data[0].data.push(el.approve)
      Data[1].data.push(el.unapprove)
    })
    this.setChartData(Data, labelsYear, 'จำนวนคำร้อง', 'ปีการศึกษา')

  }

  getPeriodOfConsiderPetition(selYear: string, selTerm: string, selForm: string) {

    let leastOne = 0;
    let OneThree = 0;
    let FourSeven = 0;
    let MoreSeven = 0;
    let label = "คำร้องทั้งหมด"
    let filterStat = Object.assign([], this.reqstatus)
    if (selYear != "ทั้งหมด") {
      filterStat = filterStat.filter(el => (new Date(el.req_item.created_at).getFullYear() + 543).toString() === selYear)
    }
    if (selTerm != "ทั้งหมด") {
      filterStat = filterStat.filter(el => {
        let term = el.req_item.semester.split('/')
        return term[0] === selTerm
      })
    }
    if (selForm != "ทั้งหมด") {
      filterStat = filterStat.filter(el => {
        let form: iform = el.req_item.form
        return form.form_name === selForm
      })
      label = selForm
    }
    filterStat.forEach(el => {
      let diff = Math.abs((new Date(el.req_item.updated_at)).getTime() - (new Date(el.req_item.created_at)).getTime());
      let diffDays = Math.floor(diff / (1000 * 3600 * 24));
      if (diffDays < 1) {
        leastOne++;
      }
      else if (diffDays >= 1 && diffDays <= 3) {
        OneThree++;
      }
      else if (diffDays >= 4 && diffDays <= 7) {
        FourSeven++;
      } else {
        MoreSeven++;
      }
    })
    let Data: ichartData[] = [
      {
        data: [leastOne, OneThree, FourSeven, MoreSeven],
        label: label,
        backgroundColor: ['rgba(255, 99, 132, 0.2)', 'rgba(255, 99, 132, 0.2)', 'rgba(255, 99, 132, 0.2)', 'rgba(255, 99, 132, 0.2)'],
        borderColor: ['rgba(255, 99, 132, 1)', 'rgba(255, 99, 132, 1)', 'rgba(255, 99, 132, 1)', 'rgba(255, 99, 132, 1)'],
        borderWidth: 1
      }
    ]
    let labels = ['< 1', '1 - 3', '4 - 7', '> 7']
    this.setChartData(Data, labels, 'จำนวนคำร้อง', 'ระยะเวลา (วัน)')

  }


  reset() {
    this.termValue = 'ทั้งหมด'
    this.yearValue = 'ทั้งหมด'
    this.formValue = 'ทั้งหมด'
    this.typeValue = 'สถิติการดำเนินการ'
    this.onChangeSelect(null)
  }

  public chartClicked(e: any): void {
  }

}
