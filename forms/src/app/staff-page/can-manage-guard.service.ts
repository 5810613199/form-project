import { Injectable } from '@angular/core';
import { StaffService } from './staff.service';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { AppService } from '../app.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CanManageGuardService {

  constructor(public service: StaffService, public router: Router, public app: AppService) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.service.checkCanManage().pipe(map
      (data => {
        if (data) {
          return true;
        } else {
          this.router.navigate(['staff/home'])
          window.alert("Unauthorized Access")
          return false;
        }
      }))
  }
}
