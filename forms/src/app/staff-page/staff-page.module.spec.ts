import { StaffPageModule } from './staff-page.module';

describe('StaffPageModule', () => {
  let staffPageModule: StaffPageModule;

  beforeEach(() => {
    staffPageModule = new StaffPageModule();
  });

  it('should create an instance', () => {
    expect(staffPageModule).toBeTruthy();
  });
});
