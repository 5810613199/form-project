import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from "@angular/material";
import { RequestConfirmComponent } from '../request-confirm/request-confirm.component';
import { StaffService } from '../staff.service';
import { iform, ireqans, istudent_info, icomment, isignctrl, User, UserInfo, icommentadv, iposition, iupload_ans, ireq_status, iSigner, iperson } from "../../utils/interfaces";
import { AppService } from "../../app.service";

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {

  id: number = NaN;
  student_info: istudent_info = {};
  question_info = {};
  form_info = {};
  form: iform = {};
  req_ans: ireqans[] = [];
  request: ireq_status = {};
  payload: icomment = {};
  comments: icomment[] = [];
  issigned: boolean = false;
  errCommentEmpty: boolean = false;
  submit: boolean = false;
  semestervalue: string = '';
  lastTeacherApprove: boolean = false;
  lastTeacherIsSigned: boolean = false;
  // @Input() request:any;

  formControl = new FormControl('');


  constructor(private router: ActivatedRoute,
    private dialog: MatDialog,
    private ss: StaffService,
    private app: AppService,
    private _router: Router
  ) { }

  ngOnInit() {
    this.id = Number.parseInt(this.router.snapshot.url[1].path)
    this.getRequestInfo()
    this.getFormInfo()


  }

  getRequestInfo() {
    this.ss.getRequestByID(this.id).subscribe(res => {
      if (res != null) {
        let signctrl = res.req_s.form.sign_control[0];
        let result = res.req_s;
        this.request = result
        this.semestervalue = result.semester
        let user: UserInfo = JSON.parse(this.app.getLocal('userinfo'))
        let person: iperson = JSON.parse(this.app.getLocal('personalinfo'))
        let tempcomment: icommentadv = result.comment[0]
        // null ไม่เห็น 0 เห็นแต่ยังไม่ได้ทำ 1 อนุมัติ 2 ไม่อนุมัติ
        if (tempcomment != undefined) {
          let countsigner = 0;
          for (let i = 1; i <= 5; i++) {
            let sign = `sign_t${i}`
            //if staff
            if (person.person.personid === result[sign] && result[sign + '_s'] != null) {
              if (result[sign + '_s'] != 0) { //check already sign
                this.issigned = true;
              }
            }

            if (result[sign] != null) {
              countsigner++;
            }
          }
          this.ss.getTeacherStaff().subscribe(personss => {
            this.ss.getSigner().subscribe(out => {
              let persons: iperson[] = personss
              let op: iSigner[] = out
              for (let i = 1; i <= countsigner; i++) {
                let cmnt = `comment_t${i}`
                let sign = `sign_t${i}`
                let sign_time = `sign_time_${i}`
                let advisor: iSigner = op.find(element => signctrl[sign] === element.personid)
                let teacher: UserInfo = result[`st${i}`]
                let objcomment: icomment = {
                  req_id: tempcomment.req_id,
                  user_id: result[sign],
                  comment: tempcomment[cmnt],
                  role: advisor.pos_detail,
                  status: result[sign + '_s'],
                  timestamp: tempcomment[sign_time],
                  teacher_info: persons.find(person => person.person.personid === result[sign]),
                }
                this.comments.push(objcomment)


              }
              let findStaffApprove = this.comments.find(el => (el.status == 1) && el.role === 'เจ้าหน้าที่ตรวจสอบ')
              this.comments = this.comments.filter(el => {
                if (el.role == 'เจ้าหน้าที่ตรวจสอบ' && el.status == 2) { //can see only staff not approve
                  return true
                } else if (findStaffApprove) { //if staff approve
                  return true
                } else {
                  return false
                }

              })
              this.lastTeacherApprove = this.comments[this.comments.length - 1].status === 1
              this.lastTeacherIsSigned = this.comments[this.comments.length - 1].status !== 0 &&
                this.comments[this.comments.length - 1].status !== undefined
              // this.comments[this.comments.length - 1].user_id === person.person.personid : true;
            })
          })


        }
      } else {
        window.alert("Unauthorized Access")
        this._router.navigate(['staff/home'])
      }


    })
  }

  back() {
    this._router.navigate(['staff/home'])
  }

  isImage(file: iupload_ans): boolean {
    let imageType = ['.png', '.jpg', 'jpeg']
    if (imageType.indexOf(file.upload_file) != -1) {
      return true;
    }
    return false;
  }

  getFormInfo() {
    this.ss.getAnswerForm(this.id).subscribe(res => {
      if (res != null) {
        let temp: any = (res.req_s)
        this.form = temp.form
        this.req_ans = temp.req_ans
        this.student_info = res.std[0]
        this.student_info.degree = res.std.degree
        this.student_info.title = res.std.title
        this.student_info.department = res.std.department
      } else {
        window.alert("Unauthorized Access")
        this._router.navigate(['staff/home'])
      }

    })
  }

  downloadfile(filename: string) {
    this.ss.getFile(filename).subscribe(res => {
      var url = window.URL.createObjectURL(res);
      var a = document.createElement('a');
      document.body.appendChild(a);
      a.setAttribute('style', 'display: none');
      a.href = url;
      a.download = filename;
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove(); // remove the element
    }, error => {
    }, () => {
    });

  }

  onDialog(status: number) {
    this.payload.comment = this.formControl.value;
    this.payload.status = status;
    this.payload.req_id = this.id;
    this.payload.user_id = JSON.parse(this.app.getLocal('personalinfo')).person.personid
    if (this.formControl.valid) {
      let dialogRef = this.dialog.open(RequestConfirmComponent, {
        width: '300px',
        data: { status: status, data: this.payload }
      })
      dialogRef.afterClosed().subscribe(res => {
        if (res != '') {
          this._router.navigate(['staff/home'])
        }
      })
    } else {
      this.errCommentEmpty = true;
    }





  }

  checkInvalidComment(): boolean {
    //check submit if empty comment show error, then hide after touch comment 
    return this.formControl.invalid && this.errCommentEmpty;
  }

  setComment() {
  }

}
