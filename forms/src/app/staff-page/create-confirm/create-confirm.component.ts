import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { StaffService } from '../staff.service';
import { iform } from 'src/app/utils/interfaces';


@Component({
  selector: 'app-create-confirm',
  templateUrl: './create-confirm.component.html',
  styleUrls: ['./create-confirm.component.css']
})
export class CreateConfirmComponent implements OnInit {
  order: string = '';
  form: iform;
  isLoading: boolean = false;
  complete: boolean;
  confirm: boolean = false;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    private ss: StaffService, public dialogRef: MatDialogRef<CreateConfirmComponent>) { }

  ngOnInit() {
    this.order = this.data.order
    this.form = this.data.form

  }

  onCancel() {
    this.dialogRef.close('')
  }

  onConfirm() {
    this.confirm = true;
    this.isLoading = true;
    if (this.order === 'create') {
      this.ss.createForm(this.form).subscribe(res => {
        this.isLoading = false;
        this.complete = res === true ? true : false;
      })
    } else if (this.order === 'edit') {
      this.ss.updateForm(this.form).subscribe(res => {
        this.isLoading = false;
        this.complete = res === true ? true : false;
      })
    }
  }

}
