import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { StaffService } from './staff.service';
import { AppService } from '../app.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StaffGuardService implements CanActivate {

  constructor(public service: StaffService, public router: Router, public app: AppService) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.service.checkstaff().pipe(map
      (data => {
        if (data) {
          return true;
        } else {
          this.router.navigate(['auth/login'], { queryParams: { returnUrl: state.url } })
          return false;
        }
      }))
  }
}
