import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { StaffService } from '../staff.service';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { iposition } from "../../utils/interfaces";

@Component({
  selector: 'app-staff-order',
  templateUrl: './staff-order.component.html',
  styleUrls: ['./staff-order.component.css']
})


export class StaffOrderComponent implements OnInit {
  movies = [
    'Episode I - The Phantom Menace',
    'Episode II - Attack of the Clones',
    'Episode III - Revenge of the Sith',
    'Episode IV - A New Hope',
    'Episode V - The Empire Strikes Back',
    'Episode VI - Return of the Jedi',
    'Episode VII - The Force Awakens',
    'Episode VIII - The Last Jedi'
  ];
  positions: iposition[] = [];
  orders: any[] = [];
  isEmpty: boolean;
  cancel: any[] = [];
  drag: boolean = false;
  selected: boolean = false;
  selectIndex: number = NaN;
  dragOrder: boolean = false;
  errLimitOrder: boolean = false;

  constructor(public dialogRef: MatDialogRef<StaffOrderComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ss: StaffService) { }
    

  ngOnInit() {
    this.orders = this.data.order
    this.cancel = Object.assign([], this.data.order)
    this.checkEmpty();
    this.getSigner();
  }

  getSigner() {
    this.ss.getSigner().subscribe(res => {
      this.positions = (res)
    })
  }

  addorder() {
    let obj = { selectFormControl: new FormControl('', Validators.required) };
    if(this.orders.length < 5){
      this.orders.push(obj);
    }else{
      this.errLimitOrder = true;
    }
    
    this.checkEmpty()
  }
  

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.orders, event.previousIndex, event.currentIndex);
  }

  move(index: number) {
    this.selected = !this.selected;
    if (!this.selected) {
      let temp = this.orders[this.selectIndex]
      this.orders[this.selectIndex] = this.orders[index]
      this.orders[index] = temp
      this.selectIndex = NaN
    } else {
      this.selectIndex = index
    }
  }

  checkEmpty() {
    //กรณีที่ บางตัวยังไม่ได้ใส่ค่า
    this.isEmpty = !this.orders.find(el => el.selectFormControl.dirty === false) || !this.orders.length
  }

  delete(index: number) {
    this.orders.splice(index, 1)
    this.errLimitOrder = false;
  }






}
