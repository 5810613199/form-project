import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CreateFormComponent } from "./create-form/create-form.component";
import { HomeComponent } from "./home/home.component";
import { RequestComponent } from "./request/request.component";
import { StatComponent } from "./stat/stat.component";
import { ManageRequestComponent } from "./manage-request/manage-request.component";
import { CanManageGuardService } from "./can-manage-guard.service";

const routes: Routes = [
  {
    path: "create",
    component: CreateFormComponent,
    canActivate: [CanManageGuardService]
  },
  {
    path: "edit/:id",
    component: CreateFormComponent,
    canActivate: [CanManageGuardService]
  },
  { path: "home", component: HomeComponent },
  { path: "request/:id", component: RequestComponent },
  {
    path: "stat",
    component: StatComponent,
    canActivate:  [CanManageGuardService]
  },
  {
    path: "manage",
    component: ManageRequestComponent,
    canActivate: [CanManageGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StaffPageRoutingModule {}
