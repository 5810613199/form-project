import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppComponent } from './app.component'
import {
  RouterTestingModule
} from '@angular/router/testing';
import { MatDialogModule, MatDialog, MatDialogRef } from '@angular/material/dialog'

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      providers: [{ provide: MatDialog, useValue: {} }],
      imports: [RouterTestingModule, MatDialogModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
