import { FormControl } from "@angular/forms";

export interface Tokens {
  token_type?: string;
  expires_in?: number;
  access_token?: string;
  refresh_token?: string;
}

export interface question {
  //in app
  no?: number;
  title?: FormControl;
  type?: string;
  ansreq?: boolean;
  hasOther?: boolean;
  numfile?: number;
  filetype?: string[];
  fileControl?: FormControl;
  //api
  form_id?: number;
  id?: number;
  n_choice?: number;
  other?: number;
  question_choice?: string[];
  question_detail?: string;
  question_seq?: number;
  question_type?: string;
  req_ans?: number;
  upload_q?: string[];
}

export interface iSigner {
  id?: number;
  u_id?: number;
  pos_detail?: string;
  completed?: number;
  personid?: string;
}

export interface options {
  qno?: number;
  option?: option[];
}

export interface isubject {
  id?: number;
  section?: number;
  sub_code?: string;
  sub_name?: string;
  u_id?: number;
  //     id: 201
  // section: 1
  // sub_code: "MA111"
  // sub_name: "Calculus1"
  // u_id: 832
}

export interface isection {
  section?: number;
  subcode?: string;
  u_id?: number;
}

export interface iSign_ctrl {
  form_id?: number;
  id?: number;
  n_sign?: number;
  // form_id: 188
  // id: 121
  // n_sign: 3
  // sign_t1: 200
  // sign_t2: 197
  // sign_t3: 198
  // sign_t4: null
  // sign_t5: null
}

export interface iform {
  id?: number;
  form_name?: string;
  description?: string;
  mod_by?: string;
  create_by?: string;
  form_question?: iquestion[];
  sign_control?: isignctrl[];
}

export interface iquestion {
  id?: number;
  question_seq?: number;
  question_type?: string;
  question_detail?: string;
  n_choice?: number;
  req_ans?: number;
  other?: number;
  question_choice?: ioption[];
  upload_q?: iupload[];
}

export interface isubmitform {
  form_id?: number;
  student_id?: number;
  semester?: string;
  subject?: number;
  req_ans?: ianswer[];
}

export interface ianswer {
  question_id?: number;
  ans_text?: string;
  ans_choice?: number;
  ans_other?: string;
  upload_ans?: iupload_ans[];
}

export interface iupload_ans {
  image?: string;
  upload_id?: number;
  upload_seq?: number;
  filename?: string;
  //api
  ans_id?: number;
  upload_file?: string;
  // id: 11, upload_id: 133, ans_id: 448, upload_seq: 1, upload_file:
}

export interface iother {
  id?: number;
  value?: string;
}

export interface ireqans {
  ans_choice?: number;
  ans_other?: string;
  ans_text?: string;
  id?: number;
  question_choice?: iquestion_choice[];
  question_id?: number;
  req_id?: number;
}

export interface istudent_info {
  advisor?: number;
  degree_id?: string;
  department_id?: string;
  email?: string;
  fname_th?: string;
  fname_en?: string;
  lname_th?: string;
  lname_en?: string;
  id?: number;
  major_th?: string;
  reg_telephone?: string;
  stu_code?: string;
  title_id?: string;
  u_id?: number;
  department?: department;
  degree?: degree;
  title?: title;
}

export interface title {
  id?: number;
  title_name_fen?: string;
  title_name_fth?: string;
  title_name_len?: string;
  title_name_lth?: string;
}

export interface department {
  dep_refer?: string;
  department_en?: string;
  department_th?: string;
  depid?: number;
  major_en?: string;
  major_th?: string;
}

export interface degree {
  degree_name_en?: string;
  degree_name_th?: string;
  id?: number;
}
export interface iteacher_info {
  advisor?: number;
  degree_id?: number;
  department_th?: string;
  email?: string;
  fname_th?: string;
  lname_th?: string;
  id?: number;
  major_th?: string;
  reg_telephone?: string;
  stu_code?: string;
  u_id?: number;
}
export interface iquestion_choice {
  id?: number;
  question_id?: number;
  choice_detail?: string;
  choice_seq?: number;
}

export interface iperson_role {
  Role?: string;
  Name?: string;
  Privilege?: string;
}

export interface icomment {
  req_id?: number;
  user_id?: string;
  status?: number;
  comment?: string;
  role?: string;
  timestamp?: string;
  teacher_info?: iperson;
}

export interface iteacher_subject {
  department?: t_department;
  teacher?: t_teacher;
  titleaca?: t_titleaca;
}

export interface t_department {
  departmentid?: number;
  departmentname?: string;
}

export interface t_teacher {
  ad_user?: string;
  department?: number;
  fname_e?: string;
  fname_t?: string;
  lname_e?: string;
  lname_t?: string;
  personid?: string;
  title?: number;
  title_academic?: number;
}

export interface t_titleaca {
  titleacafulle?: string;
  titleacafullt?: string;
  titleacaid?: number;
  titleacalite?: string;
  titleacalitt?: string;
}

export interface icommentadv {
  comment_t1?: string;
  comment_t2?: string;
  comment_t3?: string;
  comment_t4?: string;
  comment_t5?: string;
  id?: number;
  req_id?: number;
}

export interface isignctrl {
  // "sign_control": [
  //     {
  //         "n_sign": 2,
  //         "sign_t1": 176,
  //         "sign_t2": 177,

  //         "sign_t3": null,
  //         "sign_t4": null,
  //         "sign_t5": null
  //     }
  // ]
  n_sign?: number;
  sign_t1?: number;
  sign_t2?: number;
  sign_t3?: number;
  sign_t4?: number;
  sign_t5?: number;
}
// id: 144, u_id: 540, pos_detail: "รองคณบดี"
export interface iposition {
  id?: number;
  u_id?: number;
  pos_detail?: string;
}

export interface ioperate_lv {
  id?: number;
  level_operate?: number;
  name_of_position?: string;
  personid?: string;
}

export interface istaff {
  staff?: iperson;
  form_manage_lv?: FormControl;
  edit?: boolean;
}
// export interface ichart{
//     valuex?:any;
//     valuey?:any;

// }
export interface ichartData {
  data?: number[];
  label?: string;
  backgroundColor?: string[];
  borderColor?: string[];
  borderWidth?: number;
}

export interface iresult_con {
  approve?: number;
  unapprove?: number;
  label?: string;
}

export interface ireq_status {
  form?: iform;
  form_id?: number;
  id?: number;
  semester?: string;
  sign_t1?: number;
  sign_t1_s?: number;
  sign_t2?: number;
  sign_t2_s?: number;
  sign_t3?: number;
  sign_t3_s?: number;
  sign_t4?: number;
  sign_t4_s?: number;
  sign_t5?: number;
  sign_t5_s?: number;
  status?: string;
  student?: istudent_info;
  student_id?: number;
  created_at?: string;
  updated_at?: string;
}

export interface req_status {
  created_at?: string;
  form?: iform;
  form_id?: number;
  id?: number;
  semester?: string;
  sign_t1?: string;
  sign_t1_s?: number;
  sign_t2?: string;
  sign_t2_s?: number;
  sign_t3?: string;
  sign_t3_s?: number;
  sign_t4?: string;
  sign_t4_s?: number;
  sign_t5?: string;
  sign_t5_s?: number;
  status?: string;
  student_id?: string;
  updated_at?: string;

}

export interface ireq_stat {
  req_item?: req_status;
  student?: istudent_info
}

export interface iteacher {
  department_th?: string;
  email?: string;
  fname_th?: string;
  id?: number;
  lname_th?: string;
  major_th?: string;
  personid?: number;
  positionid?: string;
  reg_telephone?: string;
  titleacafullt?: string;
  u_id?: number;
}

export interface iperson {
  person?: t_teacher;
  department?: department;
  title?: p_title;
  opr_lv?: opr_lv;
}

export interface opr_lv{
  id?: number;
  level_operate?: number;
  name_of_position?: string;
  personid?: string;
}

export interface p_title {
  //----- staff -----//
  title_name_e?: string;
  title_name_t?: string;
  titleid?: number;

  //----- teacher -----//
  titleacafulle?: string;
  titleacafullt?: string;
  titleacaid?: number;
  titleacalite?: string;
  titleacalitt?: string;
}

export interface ians_config {
  id?: string;
  u_id?: number;
}

export interface iconfig {
  id?: number;
  personid?: string;
  pos_detail?: string;
  teacher_uid?: FormControl;
  manage_lv?: FormControl;
  edit?: boolean;
}

export interface icreate_config {
  u_id?: number;
  pos_detail?: string;
}

export interface ioperate {
  u_id?: number;
  manage_lv?: number;
}

export interface ioption {
  question_id?: number;
  id?: number;
  choice_detail?: string;
  choice_seq?: number;
}

export interface iupload {
  id?: number;
  question_id?: number;
  form_id?: number;
  n_file?: number;
  file_type?: string;
}

export interface ifiledetail {
  id?: number;
  form_id?: number;
  question_id?: number;
  n_file?: number;
  file_type?: string;
}

export interface option {
  no?: number;
  answer?: FormControl;
}

export interface comment {
  role?: string;
  reason?: string;
  status?: string;
  timestamp?: Date;
}
export interface form {
  form_name?: string;
  id?: number;
  description?: string;
  create_by?: string;
  created_at?: Date;
  mod_by?: string;
  form_pos?: number;
  //     create_by: "kris"
  // created_at: "2019-04-06 15:56:33"
  // description: null
  // form_name: "leave off the study"
  // id: 285
  // mod_by: "kris"
}

export class User {
  id: number;
  username: string;
  password: string;
}

export interface UserInfo {
  id?: number;
  username?: string;
  email?: string;
  email_verified_at?: string;
  created_at?: string;
  updated_at?: string;
  role?: string;
}

export interface IRoute {
  Prev?: string;
  Current?: string;
  Refresh?: boolean;
}

export interface subject {
  sub_code?: string;
  section?: number;
  // "sub_code": "MA111",
  // "section"
}
