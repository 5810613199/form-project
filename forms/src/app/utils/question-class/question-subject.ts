import { QuestionBase } from './question-base';

export class SubjectQuestion extends QuestionBase<string> {
  controlType = 'subject';
  type: string;

  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
  }
}