import { QuestionBase } from './question-base';
import { ifiledetail } from "../interfaces";

export class FileQuestion extends QuestionBase<string> {
  controlType = 'file';
  type: string;
  filedetail: ifiledetail ;
  // id: 35, form_id: 132, question_id: 219, n_file: 1, file_type: "jpg"

  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
    this.filedetail=options['filedetail'] || {};
  }
}