import { QuestionBase } from './question-base';
import { iquestion_choice } from "../interfaces";
export class CheckboxQuestion extends QuestionBase<string> {
  controlType = 'checkbox';
  options: iquestion_choice[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || [];
  }
}