import { QuestionBase } from './question-base';
import { iquestion_choice } from "../interfaces";
export class DropdownQuestion extends QuestionBase<string> {
  controlType = 'dropdown';
  options: iquestion_choice[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || [];
  }
}