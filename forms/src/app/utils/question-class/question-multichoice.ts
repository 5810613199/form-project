import { QuestionBase } from './question-base';
import { iquestion_choice } from "../interfaces";
export class MultichoiceQuestion extends QuestionBase<string> {
  controlType = 'multichoice';
  options: iquestion_choice[] = [];
  // id: 636, question_id: 677, choice_detail: "bear", choice_seq:
  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || [];
  }
}