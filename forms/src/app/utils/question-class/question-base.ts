import { iquestion_choice,ifiledetail } from "../interfaces";
export class QuestionBase<T> {
    value: T;
    key:string;
    label: string;
    required: boolean;
    order: number;
    controlType: string;
    options: iquestion_choice[];
    other: boolean;
    filedetail?: ifiledetail;
   
   
    constructor(options: {
        value?: T,
        key?: string,
        label?: string,
        required?: boolean,
        order?: number,
        controlType?: string,
        options?: iquestion_choice[],
        other?: boolean
        
      } = {}) {
      this.value = options.value;
      this.key = options.key || '';
      this.label = options.label || '';
      this.required = !!options.required;
      this.order = options.order === undefined ? 1 : options.order;
      this.controlType = options.controlType || '';
      this.other = options.other === undefined ? false : options.other;
     }
  }