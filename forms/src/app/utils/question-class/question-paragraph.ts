import { QuestionBase } from './question-base';

export class ParagraphQuestion extends QuestionBase<string> {
  controlType = 'paragraph';
  type: string;

  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
  }
}