import { AuthModule } from './auth.module';

describe('AuthModule', () => {
  let staffPageModule: AuthModule;

  beforeEach(() => {
    staffPageModule = new AuthModule();
  });

  it('should create an instance', () => {
    expect(staffPageModule).toBeTruthy();
  });
});
