import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';
import { AppService } from "../app.service";
import { Observable, of } from "rxjs";
import { map } from 'rxjs/operators';
@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(public service: AuthService, public router: Router, public app: AppService, public auth: AuthService) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
     if(this.auth.isLoggedIn()){
       return true;
     }else{
      this.router.navigate(['/auth/login'], { queryParams: { returnUrl: state.url } });
      return false;
     }
    
  }

}
