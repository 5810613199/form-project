import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { LoginadminComponent } from "./loginadmin/loginadmin.component";

const routes: Routes = [
  { path: "login", component: LoginComponent },
  { path: "admin", component: LoginadminComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {}
