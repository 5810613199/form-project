import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AuthService } from "./auth.service";
import {
  HttpClient,
  HTTP_INTERCEPTORS,
  HttpClientModule
} from "@angular/common/http";
// import { AuthInterceptor } from '../http-interceptors/auth-interceptor';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AuthRoutingModule } from "./auth-routing.module";
import { LoginComponent } from "./login/login.component";
import { TokenInterceptorService } from "../http-interceptors/token-interceptor.service";
import { LoginadminComponent } from './loginadmin/loginadmin.component';

@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  declarations: [LoginComponent, LoginadminComponent],
  providers: [
    AuthService,
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: TokenInterceptorService,
    //   multi: true
    // }
    // { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptorService, multi: true },
  ],
  exports: [LoginComponent]
})
export class AuthModule {}
