import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AuthService } from '../auth.service';
import { inject, DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpModule } from '@angular/http';
import { By } from '@angular/platform-browser';



describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      providers: [
        AuthService,
        // { provide: ActivatedRoute, useValue: {} }
      ],
      imports: [
        ReactiveFormsModule, FormsModule,
        RouterTestingModule,
        HttpClientTestingModule,
        HttpModule,
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should login pass', () => {
    component.loginForm.controls['username'].setValue('user');
    component.loginForm.controls['password'].setValue('123456');
    expect(component.checkLoginFormValid()).toBe(true)
  })

  it('should login fail', () => {
    component.loginForm.controls['username'].setValue('user');
    component.loginForm.controls['password'].setValue('1234');
    expect(component.checkLoginFormValid()).toBe(false)
  })

  it('should call auth login method', async(() => {
    let loginElement: DebugElement;
    const debugElement = fixture.debugElement;
    let authService = debugElement.injector.get(AuthService);
    let loginSpy = spyOn(authService, 'login').and.callThrough();
    loginElement = fixture.debugElement.query(By.css('form'));
    // to set values
    component.loginForm.controls['username'].setValue('user');
    component.loginForm.controls['password'].setValue('123456');
    loginElement.triggerEventHandler('ngSubmit', null);
    expect(loginSpy).toHaveBeenCalledTimes(1); 
  }));
});
