import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { User, UserInfo, istudent_info, iperson } from "../../utils/interfaces";
import { AuthService } from "../../auth/auth.service";
import { AppService } from "../../app.service";
import { catchError } from "rxjs/operators";

@Component({
  selector: "login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  isAuth: boolean;
  isAdminLogin: boolean;
  user: User;
  alertMessage: boolean = false;
  error: string = "";
  errRequired: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService,
    private app: AppService
  ) { }

  ngOnInit() {
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/";
    this.isAdminLogin = this.router.url === "/auth/admin" ? true : false;
    if (!this.isAdminLogin) {
      this.auth.logout().subscribe(
        res => {
          this.app.setUnAuth();
        },
        err => { }
      );
    } else {
      this.auth.logoutAdmin().subscribe(
        res => {
          this.app.setUnAuth();
        },
        err => { }
      )
    }


    this.loginForm = this.formBuilder.group({
      username: ["", [Validators.maxLength(30), Validators.required]],
      password: [
        ,
        [Validators.required, Validators.maxLength(30), Validators.minLength(6)]
      ]
    });
    // this.onSubmit();
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  getUserAdmin() {
    this.auth.getUserAdmin().subscribe(res => {
      if (res) {
        this.app.setLocal('userinfo', JSON.stringify(res))
        this.router.navigate(['admin/home'])
      }
    }, (err => {
    }))
  }

  getUser() {
    this.auth.getUser().subscribe(
      res => {
        if (res) {
          let userinfo: UserInfo = JSON.parse(this.app.getLocal("userinfo"));
          let role: string;
          if (res.advisor != undefined) {
            let student_info: istudent_info;
            role = "student";
            student_info = res[0];
            student_info.degree = res.degree;
            student_info.title = res.title;
            student_info.department = res.department;
            this.app.setLocal("personalinfo", JSON.stringify(student_info));
          } else {
            let person_info: iperson = res;
            role =
              person_info.person.personid.charAt(0) === "1"
                ? "teacher"
                : "staff";
            this.app.setLocal("personalinfo", JSON.stringify(person_info));
          }
          userinfo.role = role;
          this.app.setLocal("userinfo", JSON.stringify(userinfo));

          userinfo.role;
          let homeurl = "/" + userinfo.role + "/home";
          let isCorrectRole = this.returnUrl.indexOf(userinfo.role);
          if (isCorrectRole != -1 && this.returnUrl != "/") {
            this.router.navigate([this.returnUrl]);
          } else {
            this.router.navigate([homeurl]);
          }
        }
      },
      err => {
      }
    );
  }

  checkLoginFormValid(): boolean {
    this.loginForm.markAsPristine();
    return this.loginForm.valid;
  }

  onSubmit() {
    if (this.checkLoginFormValid()) {
      this.loading = true;
      if (!this.isAdminLogin) {
        this.auth.login(this.loginForm.value).subscribe(
          res => {
            if (res) {
              this.error = "";
              this.loading = false;
              localStorage.setItem("username", res.username);
              localStorage.setItem("userinfo", JSON.stringify(res));
              this.getUser();
            } else {
              this.error = "Invalid Username or Password. Please try again.";
              this.alertMessage = true;
              this.loading = false;
            }
          },
          error => {
            this.error = "Login failed. Please try again.";
            this.alertMessage = true;
            this.loading = false;
          }
        );
      } else {
        this.auth.loginAdmin(this.loginForm.value).subscribe(
          res => {
            if (res) {
              this.error = "";
              this.loading = false;
              localStorage.setItem('access_token', res.access_token)
              localStorage.setItem('refresh_token', res.refresh_token)
              this.getUserAdmin();
            } else {
              this.error = "Invalid Username or Password. Please try again.";
              this.alertMessage = true;
              this.loading = false;
            }
          },
          error => {
            this.error = "Login failed. Please try again.";
            this.alertMessage = true;
            this.loading = false;
          });
      }

    }
  }
}
