import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient, HttpParams, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { Http, Headers, RequestOptions } from "@angular/http";
import { of } from "rxjs";
import { User, UserInfo, Tokens } from "./../utils/interfaces";
import { throwError } from 'rxjs';
import { catchError, map, tap, mapTo, } from "rxjs/operators";
import { AppService } from "../app.service";

@Injectable()
export class AuthService {

  private readonly USERNAME = 'username';
  private readonly REFRESH_TOKEN = 'REFRESH_TOKEN';
  private loggedUser: string;
  options: RequestOptions
  api: string = 'http://formapi.pro/'
  public isAuth: boolean;
  constructor(private _http: HttpClient, private app: AppService, private http: Http) {
    this.options = new RequestOptions({
      'headers': this._getHeaders((this.app.getLocal('accessToken')))
    })
    this.isAuth = false;

  }

  private _getHeaders(token: string): Headers {
    let header = new Headers({
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });

    return header;
  }

  private handleError(operation: String) {
    return (err: any) => {
      let errMsg = `error in ${operation}() retrieving ${this.api}`;
      if (err instanceof HttpErrorResponse) {
      }
      return throwError(errMsg)
    }
  }

  loginAd(user: User): Observable<any> {
    let url = this.api + 'api/login'
    return;
  }

  login(user: User): Observable<any> {
    let urltoken = this.api + 'api/loginAd'
    let formData = new FormData;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this._http.post(urltoken, JSON.stringify(user), httpOptions)
      .pipe(
        catchError(error => {
          return of(false);
        }));
  }

  logout(): Observable<any> {
    let url = this.api + 'api/logoutAd'
    return this._http.post(url, {})
      .pipe(
        // tap(() => this.doLogoutUser()),
        map(res => res),
        catchError(err => {
          return of(false).pipe(map(res => { return false }));
        }),

      )

  }

  refreshToken() {
    let urltoken = this.api + 'oauth/token'
    let formData = new FormData;
    formData.append('grant_type', 'refresh_token')
    formData.append('client_id', '2')
    formData.append('client_secret', 'nnivBSwt1mpDhJr3HwExbaC3BTVAA3RYf1M6WIo2')
    formData.append('refresh_token', this.getRefreshToken())
    return this.http.post(urltoken, formData)
      .pipe(tap((tokens) => {
        let act: Tokens = tokens.json()
        // this.storeTokens(act);
      }), catchError(err => {
        return of(false)
      }));
  }

  checktoken(): Observable<boolean> {
    let url = this.api + 'api/checktoken'
    return this._http.get(url).pipe(
      map(res => res === true),
      tap(res => {}),
      catchError(err => of(false))
    )
  }

  isLoggedIn(): boolean {
    return !!this.getUsername()
  }

  getUsername() {
    return localStorage.getItem(this.USERNAME);
  }

  private doLoginUser(username: string, tokens: Tokens) {
    this.loggedUser = username;
    // this.storeTokens(tokens);
  }

  private doLogoutUser() {
    this.loggedUser = null;
    // this.removeTokens();
  }

  private getRefreshToken() {
    return localStorage.getItem(this.REFRESH_TOKEN);
  }

  getUserAdmin(): Observable<any> {
    let urluser = this.api + 'api/user'
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.app.getLocal('access_token')}`
      })
    };
    return this._http.get(urluser, httpOptions)

  }

  getUser(): Observable<any> {
    let username = localStorage.getItem('username')
    let url = this.api + 'api/getUserInfo'

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'username': username + ''
      })
    };
    return this._http.post(url, null, httpOptions)
      .pipe(
        map(res => res),
        catchError(err => {
          return of(false).pipe(map(res => { return false }));
        }),

      )
  }

  loginAdmin(user: User): Observable<any> {
    let urltoken = this.api + 'oauth/token'
    let formData = new FormData;
    formData.append('grant_type', 'password')
    formData.append('client_id', '6')
    formData.append('client_secret', '2SErqsikC2vUgTAFXltooySmAwyw8VRxtVzxNki7')
    formData.append('username', user.username)
    formData.append('password', user.password)
    return this._http.post(urltoken, formData)
      .pipe(
        map(res => {
          return res
        }
        ),
        catchError(err => {
          return of(false).pipe(map(res => { return false }));
        }),

      )


  }


  logoutAdmin(): Observable<any> {
    let url = this.api + 'api/logout'
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.app.getLocal('access_token')}`
      })
    };
    return this._http.post(url, {}, httpOptions)
      .pipe(
        map(res => res),
        catchError(err => {
          return of(false).pipe(map(res => { return false }));
        }),

      )

  }

  checkAuth(): Observable<any> {
    let url = this.api + 'api/checkauth'
    let user: UserInfo = JSON.parse(this.app.getLocal('userinfo'))

    return this._http.post(url, { id: user.id }).pipe(map(res => {
      return res
    }), catchError(err => {
      return of(false);
    }))
  }


}
