import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from "@angular/router";
// import { LocalStorageService } from "ngx-webstorage";

@Injectable({
  providedIn: 'root'
})
export class AppService {

  private previousUrl: string;
  private currentUrl: string;


  constructor(router: Router) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.previousUrl = this.currentUrl;
        this.currentUrl = event.url;
      };
    });
  }

  public get prevUrl() {
    return this.previousUrl;
  }

  getLocal(key: string) {
    return localStorage.getItem(key)
  }

  setLocal(key: string, value) {
    localStorage.setItem(key, value)
  }

  private removeLocal() {
    localStorage.clear()
  }

  setUnAuth() {
    this.removeLocal()
    // this.setLocal('isAuth', false)
  }
}
