import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { AuthGuardService } from "./auth/auth-guard.service";
import { StudentGuardService } from "./student/student-guard.service";
import { TeacherGuardService } from './advisor/teacher-guard.service';
import { StaffGuardService } from './staff-page/staff-guard.service';
import { AdminGuardService } from './admin/admin-guard.service';
import { NotFoundComponent } from './not-found/not-found.component';

const routes: Routes = [
  {
    path: '', redirectTo: '/auth/login', pathMatch: 'full', 
    // canActivate: [AuthGuardService]
  },
  {
    path: 'auth', loadChildren: '../app/auth/auth.module#AuthModule'
  },
  {
    path: 'staff',
    loadChildren: '../app/staff-page/staff-page.module#StaffPageModule',
    canActivate: [AuthGuardService, StaffGuardService]

  },
  {
    path: 'teacher', loadChildren: '../app/advisor/advisor.module#AdvisorModule',
    canActivate: [AuthGuardService, TeacherGuardService]

  },
  {
    path: 'student', loadChildren: '../app/student/student.module#StudentModule',
    canActivate: [AuthGuardService, StudentGuardService]

  },
  {
    path: 'admin', loadChildren: '../app/admin/admin.module#AdminModule',
    canActivate: [ AdminGuardService]
  },
  {
    path: '**', redirectTo: '/NotFound404'
  },
  {
    path: 'NotFound404', component: NotFoundComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
