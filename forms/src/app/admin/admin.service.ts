import { Injectable } from "@angular/core";
import {
  Http,
  Headers,
  RequestOptions,
  ResponseContentType
} from "@angular/http";
import { Observable, of } from "rxjs";
import {
  iconfig,
  ioperate,
  ians_config,
  icreate_config,
  UserInfo
} from "../utils/interfaces";
import { map, tap, catchError } from "rxjs/operators";
import { AppService } from "../app.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AuthService } from "../auth/auth.service";

@Injectable({
  providedIn: "root"
})
export class AdminService {
  api: string = "http://formapi.pro/";
  options: RequestOptions;

  constructor(
    private _http: HttpClient,
    private app: AppService,
    private auth: AuthService
  ) {
    this.options = new RequestOptions({
      headers: this._getHeaders(JSON.parse(this.app.getLocal("accessToken")))
    });
  }

  private _getHeaders(token: string): Headers {
    let header = new Headers({
      Accept: "application/json",
      Authorization: "Bearer " + token,
      "Content-Type": "application/json"
    });

    return header;
  }

  checkadmin(): Observable<boolean> {
    let url = this.api + "api/checktoken";
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.app.getLocal('access_token')}`
      })
    };
    return this._http.get<boolean>(url, httpOptions).pipe(
      map(res => {
        return res;
      }),
      catchError(err => {
        return this.recheckadmin();
      })
    );
  }

  private recheckadmin(): Observable<boolean> {
    let url = this.api + "api/checkadmin";
    return this._http.get<boolean>(url).pipe(
      map(res => {
        return res;
      }),
      catchError(err => {
        return of(false);
      })
    );
  }

  getSigner(): Observable<any> {
    let url = this.api + "api/getSigner";
    let options = new RequestOptions({
      headers: this._getHeaders(this.app.getLocal("accessToken"))
    });
    return this._http.get(url);
  }

  getAllTeacher(): Observable<any> {
    let url = this.api + "api/getAllTeacher";
    return this._http.get(url);
  }

  getTeacherStaff(): Observable<any> {
    let url = this.api + "api/getTeacherStaff";
    return this._http.get(url).pipe(
      map(res => res),
      catchError(err => {
        return of([[], []]);
      })
    );
  }

  getConfig(): Observable<any> {
    let url = this.api + "api/getsetConfig";
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.app.getLocal('access_token')}`
      })
    };
    return this._http.get(url, httpOptions).pipe(
      map(res => res),
      catchError(err => {
        return of(null);
      })
    );
  }

  getAllStaff(): Observable<any> {
    let url = this.api + "api/getAllStaff";
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.app.getLocal('access_token')}`
      })
    };
    return this._http.post(url, {}, httpOptions).pipe(
      map(res => res),
      catchError(err => {
        return of(null);
      })
    );
  }

  getAllOperateLv(): Observable<any> {
    let url = this.api + "api/getAllOperate_lv";
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.app.getLocal('access_token')}`
      })
    };
    return this._http.get(url, httpOptions).pipe(
      map(res => res),
      catchError(err => {
        return of(null);
      })
    );
  }

  setConfig(config: ians_config): Observable<any> {
    let url = this.api + "api/setConfig";
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.app.getLocal('access_token')}`
      })
    };
    return this._http.post(url, JSON.stringify(config), httpOptions).pipe(
      map(res => {
        if (res) {
          return true;
        } else {
          return false;
        }
      }),
      catchError(err => {
        return of(false).pipe(
          map(res => {
            return false;
          })
        );
      })
    );
  }

  setOperateLv(lv: ioperate): Observable<any> {
    let url = this.api + "api/setOperate_lv";
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.app.getLocal('access_token')}`
      })
    };
    return this._http.post(url, JSON.stringify(lv), httpOptions).pipe(
      map(res => {
        if (res) {
          return true;
        } else {
          return false;
        }
      }),
      catchError(err => {
        return of(false).pipe(
          map(res => {
            return false;
          })
        );
      })
    );
  }

  createConfig(cc: icreate_config): Observable<any> {
    let url = this.api + "api/createConfig";
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.app.getLocal('access_token')}`
      })
    };
    return this._http.post(url, JSON.stringify(cc), httpOptions).pipe(
      map(res => {
        if (res) {
          return true;
        } else {
          return false;
        }
      }),
      catchError(err => {
        return of(false).pipe(
          map(res => {
            return false;
          })
        );
      })
    );
  }

  deleteConfig(config_id: number): Observable<any> {
    let url = this.api + "api/deleteConfig/" + config_id;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.app.getLocal('access_token')}`
      })
    };
    return this._http.delete(url, httpOptions).pipe(
      map(res => {
        if (res) {
          return true;
        } else {
          return false;
        }
      }),
      catchError(err => {
        return of(false).pipe(
          map(res => {
            return false;
          })
        );
      })
    );
  }
}
