import { Component, OnInit, ViewChild } from '@angular/core';
import { iperson_role, iteacher_subject, iposition, ioperate_lv, iconfig, ians_config, iSign_ctrl, ioperate, iteacher, iperson, istaff } from 'src/app/utils/interfaces';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog } from '@angular/material';
import { Router } from "@angular/router";
import { animate, state, style, transition, trigger } from '@angular/animations';
import { AdminService } from '../admin.service';
import { FormControl } from '@angular/forms';
import { DialogPosComponent } from '../dialog-pos/dialog-pos.component';
import { DialogConfirmComponent } from '../dialog-confirm/dialog-confirm.component';

@Component({
  selector: 'app-operation-lv',
  templateUrl: './operation-lv.component.html',
  styleUrls: ['./operation-lv.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class OperationLvComponent implements OnInit {

  ELEMENT_DATA: iperson_role[] = [
    {
      Role: 'เจ้าหน้าที่ตรวจสอบ',
      Name: 'Staff1',
      Privilege: 'xxx'
    }, {
      Role: 'รองคณบดี',
      Name: 'Teacher1',
      Privilege: 'xxx'
    }, {
      Role: 'หัวหน้าโครงการ TU-PINE',
      Name: 'Teacher2',
      Privilege: 'xxx'
    }
  ];
  backup_value = {}
  isLoading: boolean = false;
  isDataSourceEmpty: boolean = false;
  staffs: istaff[] = []
  signers: iconfig[] = []
  operate_lv = [
    {
      level_operate: 0,
      name_of_position: 'Normal'
    },
    {
      level_operate: 1,
      name_of_position: 'Can manage'
    },
  ]
  backup: string

  dataSource = new MatTableDataSource<istaff>();
  dataTempSource = new MatTableDataSource<iperson_role>()
  selection = new SelectionModel<istaff>(true, []);
  expandedElement: iconfig | null;
  columnsToDisplay = ['Name', 'Privilege'];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private service: AdminService, public dialog: MatDialog, ) { }


  ngOnInit() {
    this.paginator._intl.itemsPerPageLabel = "จำนวนรายการ"
    this.dataSource.paginator = this.paginator
    this.getTempTableData()
    this.getTableData()
  }

  getTempTableData() {
    this.dataTempSource = new MatTableDataSource<iperson_role>(this.ELEMENT_DATA)
  }

  refreshData() {
    this.getTempTableData()
    this.getTableData()
  }

  getTableData() {
    this.isLoading = true;
    this.service.getAllStaff().subscribe(res => {
      this.isLoading = false;
      if (res) {
        let persons: iperson[] = res
        this.staffs = []
        persons.forEach(el => {
          let staff: istaff = { staff: el, edit: false, form_manage_lv: new FormControl(el.opr_lv.level_operate) }
          staff.form_manage_lv.disable()
          this.staffs.push(staff)
        })
        let self = this
        self.dataSource = new MatTableDataSource<istaff>(self.staffs)
        self.dataSource.paginator = self.paginator
      } else { //load data failed
        this.isDataSourceEmpty = true;
      }
    })


  }

  data(element: istaff, column) {
    if (column === "Privilege") {
      let word = this.operate_lv.find(el => el.level_operate === element.form_manage_lv.value)
      return `${element.form_manage_lv.value} - ${word.name_of_position}`
    } else {
      let staff = this.staffs.find(el => el.staff.person.personid === element.staff.person.personid)
      if (staff) {
        return `${staff.staff.title.title_name_t} ${staff.staff.person.fname_t} ${staff.staff.person.lname_t}`
      } else {
        return;
      }
    }

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

  }


  touch(element: iconfig) {
    return element.teacher_uid.touched || element.manage_lv.touched
  }

  onSubmit(element: istaff) {
    let op: ioperate = { u_id: Number.parseInt(element.staff.person.personid), manage_lv: element.form_manage_lv.value }
    this.service.setOperateLv(op).subscribe(res => {
      this.onEdit(element)
      this.getTableData()
    })
  }

  onEdit(element: istaff) {
    if (!element.edit) {
      element.form_manage_lv.enable()
      this.backup = element.form_manage_lv.value
    } else {
      element.form_manage_lv.patchValue(this.backup)
      element.form_manage_lv.disable()
      this.backup = ""
    }
    element.edit = !element.edit
  }

  // delete(element: iteacher_subject) {
  //   this.dialog.open(DialogConfirmComponent, {
  //     width: '300px',
  //     data: { id: element.id, pos_detail: element.pos_detail }
  //   })
  // }

  onDialog() {

    const dialogRef = this.dialog.open(DialogPosComponent, {
      width: '300px',
      data: { operate_lv: this.operate_lv }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getTableData()
    })
  }


}
