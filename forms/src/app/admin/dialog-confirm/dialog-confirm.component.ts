import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { AdminService } from '../admin.service';
import { AppService } from 'src/app/app.service';
export interface payload {
  id: number;
  pos_detail: string;
}
@Component({
  selector: 'app-dialog-confirm',
  templateUrl: './dialog-confirm.component.html',
  styleUrls: ['./dialog-confirm.component.css']
})



export class DialogConfirmComponent implements OnInit {

  confirm: boolean = false;
  finish: boolean = false;
  constructor(@Inject(MAT_DIALOG_DATA) public data: payload,
    public dialogRef: MatDialogRef<DialogConfirmComponent>,
    private as: AdminService,
    private app: AppService,
  ) { }
  color = 'primary';
  mode = 'indeterminate';
  value = 20;
  isLoading: boolean = false;
  complete: boolean;

  ngOnInit() {
  }

  onConfirm() {
    this.isLoading = true;
    this.confirm = true;

    this.as.deleteConfig(this.data.id).subscribe(res => {
      if (res) {
        this.isLoading = false;
        this.complete = res == true ? true : false;
      }

    })

  }

  onCancel() {
    this.dialogRef.close('');
  }



}
