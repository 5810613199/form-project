import { Component, OnInit, ViewChild } from "@angular/core";
import {
  iperson_role,
  iteacher_subject,
  iposition,
  ioperate_lv,
  iconfig,
  ians_config,
  iSign_ctrl,
  ioperate,
  iperson
} from "src/app/utils/interfaces";
import { SelectionModel } from "@angular/cdk/collections";
import {
  MatPaginator,
  MatTableDataSource,
  MatSort,
  MatDialog
} from "@angular/material";
import { Router } from "@angular/router";
import {
  animate,
  state,
  style,
  transition,
  trigger
} from "@angular/animations";
import { AdminService } from "../admin.service";
import { FormControl } from "@angular/forms";
import { DialogPosComponent } from "../dialog-pos/dialog-pos.component";
import { DialogConfirmComponent } from "../dialog-confirm/dialog-confirm.component";

@Component({
  selector: "app-privilege",
  templateUrl: "./privilege.component.html",
  styleUrls: ["./privilege.component.css"],
  animations: [
    trigger("detailExpand", [
      state(
        "collapsed",
        style({ height: "0px", minHeight: "0", display: "none" })
      ),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      )
    ])
  ]
})
export class PrivilegeComponent implements OnInit {
  ELEMENT_DATA: iperson_role[] = [
    {
      Role: "เจ้าหน้าที่ตรวจสอบ",
      Name: "Staff1"
    },
    {
      Role: "รองคณบดี",
      Name: "Teacher1"
    },
    {
      Role: "หัวหน้าโครงการ TU-PINE",
      Name: "Teacher2"
    }
  ];

  isLoading: boolean = false;
  isDataSourceEmpty: boolean = false;
  edit: boolean = false;
  teachers: iperson[] = [];
  signers: iconfig[] = [];
  operate_lv: ioperate_lv[] = [];

  dataSource = new MatTableDataSource<iconfig>();
  dataTempSource = new MatTableDataSource<iperson_role>();
  selection = new SelectionModel<iconfig>(true, []);
  expandedElement: iconfig | null;
  columnsToDisplay = ["Role", "Name"];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private service: AdminService, public dialog: MatDialog) { }

  ngOnInit() {
    this.paginator._intl.itemsPerPageLabel = "จำนวนรายการ";
    this.dataSource.paginator = this.paginator;
    this.getTempTableData();
    this.getTableData();
  }

  getTempTableData() {
    this.dataTempSource = new MatTableDataSource<iperson_role>(
      this.ELEMENT_DATA
    );
  }

  refreshData() {
    this.getTempTableData();
    this.getTableData();
  }

  getTableData() {
    this.isLoading = true;
    this.service.getConfig().subscribe(res => {
      this.isLoading = false;
      if (res) {
        this.signers = res;
        this.signers.map(value => {
          value.edit = false;
          value.teacher_uid = new FormControl(value.personid);
          value.teacher_uid.disable();
        });

        this.service.getTeacherStaff().subscribe(res => {
          if (res) {
            this.teachers = res
          } else {
            this.isDataSourceEmpty = true;
          }
        });
        let self = this;
        self.dataSource = new MatTableDataSource<iconfig>(self.signers);
        self.dataSource.paginator = self.paginator;
      } else {
        this.isDataSourceEmpty = true;
      }
    });
  }

  data(element: iconfig, column) {
    if (column === "Role") {
      return element.pos_detail;
    } else {
      let teacher: iperson = this.teachers.find(
        el => el.person.personid === element.personid
      );
      if (teacher) {
        let title =
          teacher["title"]["titleacafullt"] === undefined
            ? teacher["title"]["title_name_t"]
            : teacher["title"]["titleacafullt"];
        return `${title} ${teacher["person"]["fname_t"]} ${
          teacher["person"]["lname_t"]
          }`;
      } else {
        return;
      }
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  setTeacher(event, element: iconfig) {
    let conf: ians_config = { u_id: event.value, id: element.id.toString() };
  }

  touch(element: iconfig) {
    return element.teacher_uid.touched || element.manage_lv.touched;
  }

  onSubmit(element: iconfig) {
    let conf: ians_config = {
      u_id: element.teacher_uid.value,
      id: element.id.toString()
    };
    this.service.setConfig(conf).subscribe(res => {
      this.onEdit(element);
      this.getTableData();
    });
  }

  onEdit(element: iconfig) {
    if (!element.edit) {
      element.teacher_uid.enable();
    } else {
      element.teacher_uid.disable();
    }
    element.edit = !element.edit;
  }

  delete(element: iconfig) {
    const dialogRef = this.dialog.open(DialogConfirmComponent, {
      width: "300px",
      data: { id: element.id, pos_detail: element.pos_detail }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getTableData();
    });
  }

  onDialog() {
    const dialogRef = this.dialog.open(DialogPosComponent, {
      width: "300px",
      data: { teacher: this.teachers, operate_lv: this.operate_lv }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getTableData();
    });
  }
}
