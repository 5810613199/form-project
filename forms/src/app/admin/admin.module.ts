import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from "../app.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from "@angular/common/http";

import { AdminRoutingModule } from './admin-routing.module';
import { PrivilegeComponent } from './privilege/privilege.component';
import { SharedModule } from "../shared/shared.module";
import { DialogPosComponent } from './dialog-pos/dialog-pos.component';
import { DialogConfirmComponent } from './dialog-confirm/dialog-confirm.component';
import { OperationLvComponent } from './operation-lv/operation-lv.component';
import { TokenInterceptorService } from '../http-interceptors/token-interceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthModule } from '../auth/auth.module';
// import {  } from "";

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    MaterialModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AuthModule
  ],
  declarations: [PrivilegeComponent, DialogPosComponent, DialogConfirmComponent, OperationLvComponent],
  entryComponents: [DialogPosComponent, DialogConfirmComponent],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true },

  ]
})
export class AdminModule { }
