import { Injectable } from '@angular/core';
import { AdminService } from './admin.service';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AppService } from '../app.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuardService {

  constructor(public service: AdminService, public router: Router, public app: AppService, public auth: AuthService) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.service.checkadmin().pipe(map
      (data => {
        if (data) {
          return true;
        } else {   
          this.router.navigate(['auth/login'])
          return false;
        }
      }))
  }
}
