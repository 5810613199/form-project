import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { AdminService } from '../admin.service';
import { iteacher_subject, ioperate_lv, icreate_config } from 'src/app/utils/interfaces';
import { FormGroup, FormControl, Validators } from '@angular/forms';
export interface dialog_data {
  teacher?: iteacher_subject[];
  operate_lv?: ioperate_lv[];
}
@Component({
  selector: 'app-dialog-pos',
  templateUrl: './dialog-pos.component.html',
  styleUrls: ['./dialog-pos.component.css']
})


export class DialogPosComponent implements OnInit {
  form = new FormGroup({
    position: new FormControl('', Validators.required),
    teacher_uid: new FormControl('', Validators.required),
    // manage_lv: new FormControl()
  });
  confirm: boolean = false;
  isLoading: boolean = false;
  complete: boolean;

  constructor(@Inject(MAT_DIALOG_DATA) public data: dialog_data,
    private service: AdminService,
    private dialogRef: MatDialogRef<DialogPosComponent>) { }

  ngOnInit() {
  }

  onFormSubmit() {
    this.isLoading = true
    this.confirm = true
    let cc: icreate_config = {
      u_id: this.form.value.teacher_uid,
      pos_detail: this.form.value.position
    }
    this.service.createConfig(cc).subscribe(res => {
      this.isLoading = false
      this.complete = res ? true : false;
    })
  }

  onCancel() {
    this.dialogRef.close('')
  }

}
