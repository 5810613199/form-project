import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogPosComponent } from './dialog-pos.component';

describe('DialogPosComponent', () => {
  let component: DialogPosComponent;
  let fixture: ComponentFixture<DialogPosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogPosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogPosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
