import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrivilegeComponent } from "./privilege/privilege.component";
import { OperationLvComponent } from './operation-lv/operation-lv.component';

const routes: Routes = [
  { path: 'home', component: PrivilegeComponent },
  { path: 'oplv', component: OperationLvComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
