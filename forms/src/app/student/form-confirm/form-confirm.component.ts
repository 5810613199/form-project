import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { isubmitform } from 'src/app/utils/interfaces';
import { StudentService } from '../student.service';
import { Router } from '@angular/router';
export interface payload {
  answer: isubmitform
}
@Component({
  selector: 'app-form-confirm',
  templateUrl: './form-confirm.component.html',
  styleUrls: ['./form-confirm.component.css']
})
export class FormConfirmComponent implements OnInit {
  isLoading: boolean = false;
  complete: boolean;
  confirm: boolean = false;
  constructor(@Inject(MAT_DIALOG_DATA) public data: payload,
    private dialogRef: MatDialogRef<FormConfirmComponent>,
    private ss: StudentService,
    private _rt: Router) { }

  ngOnInit() {
  }

  onConfirm() {
    this.confirm = true;
    this.isLoading = true;
    this.ss.submitRequest(this.data.answer).subscribe(res => {
      this.isLoading = false;
      this.complete = res === true ? true : false;

    })
  }
  onCancel() {
    this.dialogRef.close('');
  }

}
