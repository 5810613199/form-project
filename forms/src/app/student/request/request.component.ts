import { Component, OnInit } from '@angular/core';
import { StudentService } from "../student.service";
import { ActivatedRoute, Router } from "@angular/router";
import { question, ioption, iquestion_choice } from "../../utils/interfaces";
import { QuestionBase } from "../../utils/question-class/question-base";
import { CheckboxQuestion } from "../../utils/question-class/question-checkbox";
import { DropdownQuestion } from "../../utils/question-class/question-dropdown";
import { TextboxQuestion } from "../../utils/question-class/question-text";
import { FileQuestion } from "../../utils/question-class/question-file";
import { MultichoiceQuestion } from "../../utils/question-class/question-multichoice";
import { ParagraphQuestion } from "../../utils/question-class/question-paragraph";
import { SubjectQuestion } from "../../utils/question-class/question-subject";
import { FormControl, FormGroup, FormArray, Validators } from '@angular/forms';
import { QuestionControlService } from '../question-control.service';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {

  form_id: any;
  req_id: any;
  id: any;
  forms: any[] = [];
  questions: any[] = [];
  _formname: string = '';
  payLoad: string = '';
  form: FormGroup;
  order: any;
  items: any;
  constructor(private service: StudentService, private router: ActivatedRoute,private _rt:Router) { }

  ngOnInit() {
    this.id = this.router.snapshot.url[1].path;
    this.order = this.router.snapshot.url[0].path;
    if (this.order === 'history') {
      this.req_id = this.id
      this.getRequestById()
    }
    if (this.order === 'request') {
      this.form_id = this.id
      this.getQuestion(this.form_id)
    }

  }

  getRequestById() {
    this.service.getRequestById(this.req_id).subscribe(res => {
      if (res) {
        this.form_id = res.req_s.form_id
        this.getQuestion(this.form_id)
      }else{
        window.alert("Unauthorized Access")
        this._rt.navigate(['/student/home'])
      }

    })
  }

  getQuestion(form_id: any) {
    this.service.getQuestions(form_id).subscribe(ques => {
      let self = this;
      let q: question[] = ques
      q.forEach((el, index) => {
        let req_ans: boolean = el.req_ans === 1 ? true : false;
        let other: boolean = el.other === 1 ? true : false;
        switch (el.question_type.toLowerCase()) {
          case 'short answer': self.questions.push(new TextboxQuestion({
            key: el.id,
            label: el.question_detail,
            order: el.question_seq,
            required: req_ans,
          })); break;
          case 'large answer': self.questions.push(new ParagraphQuestion({
            key: el.id,
            label: el.question_detail,
            order: el.question_seq,
            required: req_ans
          })); break;
          case 'dropdown': self.questions.push(new DropdownQuestion({
            key: el.id,
            label: el.question_detail,
            order: el.question_seq,
            required: req_ans,
            options: el.question_choice
          })); break;
          case 'radio button': self.questions.push(new MultichoiceQuestion({
            key: el.id,
            label: el.question_detail,
            order: el.question_seq,
            required: req_ans,
            options: el.question_choice,
            other: other
          })); break;
          case 'checkboxes': self.questions.push(new CheckboxQuestion({
            key: el.id,
            label: el.question_detail,
            order: el.question_seq,
            required: req_ans,
            options: el.question_choice,
            other: other
          })); break;
          case 'file upload': self.questions.push(new FileQuestion({
            key: el.id,
            label: el.question_detail,
            order: el.question_seq,
            required: req_ans,
            filedetail: el.upload_q[0],

          })); break;
          default: self.questions.push(new SubjectQuestion({
            key: el.id,
            label: el.question_detail,
            order: el.question_seq,
            required: req_ans


          })); break;
        }
      })
    })

  }

}
