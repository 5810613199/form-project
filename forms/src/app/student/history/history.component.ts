import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AppService } from "../../app.service";
import { StudentService } from "../student.service";
import { animate, state, style, transition, trigger } from '@angular/animations';
import { UserInfo, iSign_ctrl, iSigner } from 'src/app/utils/interfaces';

import { MatStepper } from '@angular/material';


@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class HistoryComponent implements OnInit {
  ELEMENT_DATA: HistoryElement[] = [];
  dataSource = new MatTableDataSource<HistoryElement>();
  isDataSourceEmpty: boolean = false;
  selection = new SelectionModel<HistoryElement>(true, []);
  expandedElement: HistoryElement | null;
  columnsToDisplay = ['No', 'Name', 'Semester', 'Date', 'Progress'];
  badge: number = 10;
  completed: any[] = []
  isLoading: boolean = false;
  SignControl: iSign_ctrl[] = [];

  isLinear = true;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('stepper') stepper: MatStepper;

  constructor(private router: Router, private apps: AppService, private ss: StudentService, private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.dataSource.sort = this.sort
    this.paginator._intl.itemsPerPageLabel = "จำนวนรายการ"
    this.dataSource.paginator = this.paginator
    this.getLoadingRequest();
    this.getAllRequests();

  }

  getLoadingRequest() {
    const item: HistoryElement = { 'No': NaN, 'Name': null, 'Semester': null, 'Date': null, 'Progress': null }
    let element_dat = [item, item, item]
    this.dataSource = new MatTableDataSource<HistoryElement>(element_dat)
  }

  refreshData() {
    this.isDataSourceEmpty = false;
    this.getLoadingRequest();
    this.getAllRequests();
  }



  clearAdditionalForms(): void {
    this.stepper._stateChanged(); // <- this : Marks the component to be change detected.
  }

  ngAfterViewChecked() {
    const list = document.getElementsByClassName('mat-paginator-range-label');
    list[0].innerHTML = 'หน้า: ' + (this.paginator.length === 0 ? 0 : this.paginator.pageIndex + 1) + '/' + (Math.ceil(this.paginator.length / this.paginator.pageSize));

  }
  getAllRequests() {
    let user: UserInfo = JSON.parse(this.apps.getLocal('userinfo'))
    this.ss.getAllRequests(user.username).subscribe(res => {
      let self = this;
      this.SignControl = [];
      this.ELEMENT_DATA = [];
      let progress: any[] = res[1]
      let form: any[] = res[0]

      form.forEach((el, index) => {
        let nsign = el.form.sign_control[0]['n_sign']
        el.form.Progress = el.status === 'complete' ? 100 : progress[index]
        el.form.Id = el.id
        el.form.Name = el.form.form_name
        el.form.Date = el.created_at
        el.form.Semester = el.semester
        let sign_s = el[`sign_t${nsign}_s`] === null ? 0 : el[`sign_t${nsign}_s`]
        el.form.Status = (el.status === 'complete' && el[`sign_t1_s`] == 2) ? el[`sign_t1_s`] : sign_s;
        this.SignControl.push(el.form.sign_control[0])
        this.ELEMENT_DATA.push(el.form)
      })
      // loop for insert all role
      this.ss.getSigner().subscribe(signer => {
        let _sign: iSigner[] = signer
        this.ELEMENT_DATA.forEach((el, index) => {
          // {id: 129, u_id: 504, pos_detail: "รองคณบดี"}
          let obj = { id: el.Id, completed: [] }
          let findsigner: iSigner = {}
          // let user: UserInfo = JSON.parse(this.apps.getLocal('userinfo'))
          el.SignControl = [];
          for (let i = 1; i <= this.SignControl[index]['n_sign']; i++) {
            let sign = 'sign_t' + i;
            findsigner = _sign.find(el => el.personid === this.SignControl[index][sign])
            let sign_sn = `sign_t${i}_s`
            obj.completed.push(form[index][sign_sn])
            el.SignControl.push(findsigner)

          }
          self.completed.push(obj)
        })
        self.ELEMENT_DATA.sort((a, b) => {
          return (new Date(b.Date).getTime() - new Date(a.Date).getTime());
        })
        self.ELEMENT_DATA.sort((a, b) => {
          return (a.Status - b.Status);
        })
        self.ELEMENT_DATA.map((el, index) => el.No = index + 1)
        self.ELEMENT_DATA.forEach((dat_el, dat_index) => {
          if (dat_el.Id != this.completed[dat_index].id) {
            let temp = this.completed[dat_index]
            let com_index = this.completed.findIndex(el => el.id == dat_el.Id)
            this.completed[dat_index] = this.completed[com_index]
            this.completed[com_index] = temp
          }
        })
        if (self.ELEMENT_DATA.length) {
          this.isDataSourceEmpty = false;
          self.dataSource = new MatTableDataSource<HistoryElement>(self.ELEMENT_DATA)
          self.dataSource.sort = self.sort
          this.dataSource.paginator = this.paginator
        } else {
          this.isDataSourceEmpty = true;
        }
      })


    });
  }

  ElementHtml(data, type, element): string {
    return data + '%%' + type + '%%' + element.Status;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

  }

  disableBadge() {
    this.badge = 0;
  }

  select(row) {
    let url = '/student/history/' + row.Id
    this.router.navigate([url])
  }

}
export interface HistoryElement {
  Name?: string;
  No?: number;
  Id?: number;
  Date?: string;
  Progress?: number;
  Status?: any;
  Semester?: string;
  SignControl?: any[];
  description?: string;

}