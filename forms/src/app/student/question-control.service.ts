import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray, FormBuilder } from '@angular/forms';
import { QuestionBase } from "../utils/question-class/question-base";
import { DISABLED } from '@angular/forms/src/model';
import { StudentService } from './student.service';
import { ianswer, iquestion_choice } from "../utils/interfaces";
import { a } from '@angular/core/src/render3';
import { formControlBinding } from '@angular/forms/src/directives/ng_model';

@Injectable({
  providedIn: 'root'
})
export class QuestionControlService {

  constructor(private ss: StudentService) { }
  toFormGroup(questions: QuestionBase<any>[]) {
    let group: any = {};
    questions.forEach(question => {

      if (question.controlType === 'checkbox') {
        if (question.other) {
          // id?: number;
          // question_id?: number;
          // choice_detail?: string;
          // choice_seq?: number;
          let other: iquestion_choice = {
            id: -1,
            question_id: Number.parseInt(question.key),
            choice_detail: 'อื่น ๆ',
            choice_seq: question.options.length + 1
          }
          question.options.push(other)
        }
        const formControls = question.options.map(control => new FormControl(false));
        let array = question.required ? new FormArray(formControls, Validators.required) : new FormArray(formControls);
        // question.options.forEach(opt => {
        //   array.push(new FormControl())
        // });

        group[question.key] = array;
      }
      else if (question.controlType === 'file') {
        let formCtrl = [];
        for(let i =0;i<question.filedetail.n_file;i++){
          formCtrl.push(new FormControl())
        }
        let array = question.required ? new FormArray(formCtrl, Validators.required) : new FormArray(formCtrl);
        group[question.key] = array;
      }
      else {
        group[question.key] = question.required ? new FormControl(question.value || '', Validators.required)
          : new FormControl(question.value || '');
      }
    })
    return new FormGroup(group);
  }

  toFormHistory(questions: QuestionBase<any>[], answer: ianswer[]) {
    let group: any = {};
    questions.forEach(question => {


      if (question.controlType === 'dropdown' || question.controlType === 'multichoice') {
        question.options.forEach((x, index) => {
          let found = answer.find(el => el.ans_choice == x.id)
          if (found) {
            group[question.key] = new FormControl(question.options[index].id);
          }

        })


      } else if (question.controlType === 'checkbox') {
        let anscheckbox: any[] = [];
        question.options.forEach((x, index) => {
          let found = answer.find(el => el.ans_choice == x.id)
          if (found) {
            anscheckbox.push(question.options[index].id)
          } else {
            let foundOther = answer.find(el => (el.ans_other != undefined) && el.question_id === x.question_id)
            anscheckbox.push(-1)
          }
        })
        let checkboxArray = new FormArray(anscheckbox)
        group[question.key] = checkboxArray;
      }

      else if (question.controlType === 'textbox' || question.controlType === 'paragraph') {

        let found = answer.find(el => el.question_id == Number.parseInt(question.key))
        if (found) {
          group[question.key] = new FormControl(found.ans_text || '');
        }
      } else if (question.controlType === 'subject') {
        group[question.key] = new FormControl(question.key);

      }
      else {
        group[question.key] = new FormControl(question.key);
      }
      group[question.key].disable();



    })

    return new FormGroup(group);
  }
}
