import { Component, OnInit } from '@angular/core';
import { form } from "../../utils/interfaces";
import { StudentService } from "../student.service";
import { Router } from "@angular/router";
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  forms: form[] = [];
  copyforms: any;
  constructor(private services: StudentService, private router: Router) { }

  ngOnInit() {
    this.copyforms = Object.assign([], this.forms)
    this.getAllForms();
  }

  getAllForms() {
    // this.services.checkStudent().subscribe()
    this.services.getAllForms().subscribe(res => {
      this.forms = res
    })
  }

  search(word: string) {
    this.forms = this.copyforms.filter(el => { return el.name.indexOf(word) != -1 })


  }

  navigate(form: form) {
    this.router.navigate(['student/request/' + form.id])
  }

}
