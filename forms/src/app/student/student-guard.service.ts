import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { StudentService } from './student.service';
import { AppService } from '../app.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StudentGuardService implements CanActivate {

  constructor(public service: StudentService, public router: Router, public app: AppService) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    
    return this.service.checkStudent().pipe(map(data => {
      if (data) {
        return true;
      } else {
        this.router.navigate(['auth/login'], { queryParams: { returnUrl: state.url } })
        return false;
      }
    }))
  }
}
