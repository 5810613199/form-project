import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Http, Response, Headers, RequestOptions, ResponseContentType } from "@angular/http";
import { Observable, of } from 'rxjs';
import { subject, isubmitform, UserInfo } from "../utils/interfaces";
import { AppService } from '../app.service';
import { map, catchError } from 'rxjs/operators';
// import "rxjs/add/operator/map";

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  api: string = 'http://formapi.pro/'
  apif: string = 'api/forms'
  apiq: string = 'api/questions'
  apic: string = 'api/choices'
  options: RequestOptions

  constructor(private http: HttpClient, private _http: HttpClient, private app: AppService, private http_client: HttpClient) {
  }

  private _setHeader(): HttpHeaders {
    let username = this.app.getLocal('username')
    let header = new HttpHeaders({
      'Content-Type': 'application/json',
      'username': username + ''
    }
    )
    return header;
  }

  checkStudent(): Observable<boolean> {
    let url = this.api + 'api/checkstudent'
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get<boolean>(url, httpOptions).pipe(
      map(res => { return res }),
      catchError(err => {
        return this.recheckstudent()
      }))
  }

  private recheckstudent(): Observable<boolean> {
    let url = this.api + 'api/checkstudent'
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get<boolean>(url, httpOptions).pipe(
      map(res => { return res }),
      catchError(err => {
        return of(false)
      })
    )
  }

  getAllForms(): Observable<any> {
    let url = this.api + 'api/getAllForms'
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url, httpOptions);

  }
  getAllRequests(user_id: string): Observable<any> {
    let url = this.api + 'api/getAllRequests/' + user_id;
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url, httpOptions).pipe(map(res => {
      return res;
    }), catchError(err => {
      return of([[], []])
    }));
  }

  getAllSubject(): Observable<any> {
    let url = this.api + 'api/getSubject';
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url, httpOptions);
  }

  getTeacherStaff(): Observable<any> {
    let url = this.api + 'api/getTeacherStaff';
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url, httpOptions)
  }

  getFormByID(id: number): Observable<any> {
    let url = this.api + 'api/getFormByID/' + id;
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url, httpOptions)
      .pipe(map(res => {
        return res;
      }), catchError(err => {
        return of(null)
      }));
  }

  getRequestById(req_id: number): Observable<any> {
    let url = this.api + 'api/getRequestById/' + req_id;
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url, httpOptions)
      .pipe(map(res => {
        return res;
      }), catchError(err => {
        return of(null)
      }));;
  }

  getAnswerForm(req_id: number): Observable<any> {
    let url = this.api + 'api/getAnswerForm/' + req_id;
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url, httpOptions)
      .pipe(map(res => {
        return res;
      }), catchError(err => {
        return of(null)
      }));;
  }

  getQuestions(id: number): Observable<any> {
    let url = this.api + 'api/getQuestions/' + id;
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url, httpOptions)
      .pipe(map(res => {
        return res;
      }), catchError(err => {
        return of([])
      }));;
  }

  getFile(filename: string): Observable<any> {
    let url = this.api + 'api/downloadfile/' + filename;
    return this._http.get(url, {
      headers: this._setHeader(),
      responseType: 'blob'
    })
  }



  getSigner(): Observable<any> {
    let url = this.api + 'api/getSigner'
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.get(url, httpOptions)
      .pipe(map(res => {
        return res;
      }), catchError(err => {
        return of([])
      }));
  }

  submitRequest(request: isubmitform): Observable<any> {
    let url = this.api + 'api/submitForm'
    const httpOptions = {
      headers: this._setHeader()
    };
    return this._http.post(url, JSON.stringify(request), httpOptions)
      .pipe(map(res => {
        return res;
      }), catchError(err => {
        return of(false)
      }));

  }



}
