import { Component, Input, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";

import { QuestionBase } from "../../utils/question-class/question-base";
import { QuestionControlService } from "../question-control.service";
import {
  comment,
  form,
  isubmitform,
  ianswer,
  iother,
  UserInfo,
  istudent_info,
  icomment,
  iform,
  icommentadv,
  iposition,
  iupload_ans,
  ireq_status,
  iSigner,
  iperson
} from "../../utils/interfaces";
import { StudentService } from "../student.service";
import { AppService } from "src/app/app.service";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { FormConfirmComponent } from "../form-confirm/form-confirm.component";

@Component({
  selector: "app-dynamic-form",
  templateUrl: "./dynamic-form.component.html",
  styleUrls: ["./dynamic-form.component.css"],
  providers: [QuestionControlService]
})
export class DynamicFormComponent implements OnInit {
  @Input() order: any;
  @Input() questions: QuestionBase<any>[] = [];
  @Input() id: number = NaN; //form_id
  @Input() req_id: number = NaN;
  request: ireq_status = {};
  _form: form = {};
  subject: number = NaN;
  form: FormGroup;
  other: iother[] = [];
  status: string = "";
  payLoad = "";
  comments: icomment[] = [];
  student_info: istudent_info;
  issigned: boolean;
  alertNotValid: boolean = false;
  semester_value: FormControl;
  year_value: FormControl;
  semestervalue: string = "";
  semester: string[] = ["1", "2", "S"];
  years: number[] = [];

  constructor(
    private qcs: QuestionControlService,
    private router: ActivatedRoute,
    private _rt: Router,
    private ss: StudentService,
    private app: AppService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.form = this.qcs.toFormGroup(this.questions);
    this.semester_value = new FormControl("", Validators.required);
    this.year_value = new FormControl("", Validators.required);
    this.getFormInfo();
    this.getYear();
    if (this.order == "history") {
      this.getCommentInfo();
    }
  }

  getYear() {
    let year = new Date().getFullYear();
    for (let i = 0; i < 10; i++) {
      this.years.push(year + 543 - i);
    }
  }

  getFormInfo() {
    this.ss.getFormByID(this.id).subscribe(res => {
      this._form = res;
    });
  }


  getSubject(event) {
    this.subject = event;
  }

  getCommentInfo() {
    this.ss.getRequestById(this.req_id).subscribe(res => {
      let result = res.req_s;
      this.request = result;
      this.status = result.status;
      let signctrl = result.form.sign_control[0];
      this.semestervalue = result.semester;
      let frm: iform = result.form;
      let user: UserInfo = JSON.parse(this.app.getLocal("userinfo"));
      let tempcomment: icommentadv = result.comment[0];
      if (tempcomment != undefined) {
        let countsigner = 0;
        for (let i = 1; i <= 5; i++) {
          let sign = `sign_t${i}`;

          if (result[sign + "_s"] != 0) {
            this.issigned = true;
          }
          if (result[sign] != null) {
            countsigner++;
          }
        }
        this.ss.getTeacherStaff().subscribe(per_person => {
          this.ss.getSigner().subscribe(out => {
            let persons: iperson[] = per_person
            let op: iSigner[] = out;
            for (let i = 1; i <= countsigner; i++) {
              let cmnt = `comment_t${i}`;
              let sign = `sign_t${i}`;
              let sign_time = `sign_time_${i}`;
              let teacher: UserInfo = result[`st${i}`];
              let advisor: iSigner = op.find(
                element => signctrl[sign] === element.personid
              );
              let objcomment: icomment = {
                req_id: tempcomment.req_id,
                user_id: result[sign],
                comment: tempcomment[cmnt],
                role: advisor.pos_detail,
                status: result[sign + "_s"],
                timestamp: tempcomment[sign_time],
                teacher_info: persons.find(info=>info.person.personid===result[sign]),
              };
              this.comments.push(objcomment);
            }
            let findStaffApprove = this.comments.find(
              el => el.status == 1 && el.role === "เจ้าหน้าที่ตรวจสอบ"
            );
            this.comments = this.comments.filter(el => {
              if (el.role == "เจ้าหน้าที่ตรวจสอบ" && el.status == 2) {
                //can see only staff not approve
                return true;
              } else if (findStaffApprove) {
                //if staff approve
                return true;
              } else {
                return false;
              }
            });
          });
        });
      }
    });
  }

  checkValid(): boolean {
    return (
      this.form.valid &&
      this.checkFileValid() &&
      this.checkCheckboxValid() &&
      this.semester_value.valid &&
      this.year_value.valid
    );
  }

  private checkFileValid(): boolean {
    let find = this.questions.find(el => el.controlType === "file");
    if (find) {
      let isFileValid = true;
      this.questions.forEach(ques => {
        if (ques.controlType === "file") {
          let boo: boolean = false;
          if (ques.required) {
            boo = false;
          } else {
            boo = true;
          }

          this.form.value[ques.key].forEach(el => {
            if (el != null) {
              boo = true;
            }
          });
          isFileValid = isFileValid && boo;
        }
      });
      return isFileValid;
    } else {
      return true;
    }
  }

  private checkCheckboxValid(): boolean {
    let find = this.questions.find(el => el.controlType === "checkbox");
    if (find) {
      let isCheckboxValid = false;
      this.questions.forEach(ques => {
        if (ques.controlType === "checkbox" && ques.required) {
          this.form.value[ques.key].forEach(el => {
            if (el === true) {
              isCheckboxValid = true;
            }
          });
        }
      });
      return isCheckboxValid;
    } else {
      return true;
    }
  }

  onSubmit() {
    let user: UserInfo = JSON.parse(this.app.getLocal("userinfo"));
    let ans_info: isubmitform = {
      form_id: this._form.id,
      req_ans: [],
      student_id: user.id,

      semester: this.semester_value.value + "/" + this.year_value.value
    };
    this.questions.forEach(el => {
      let ans: ianswer = {};
      if (el.controlType === "checkbox") {
        let selectedCheckbox = this.form.value[el.key]
          .map((checked, index) => (checked ? el.options[index].id : null))
          .filter(value => value !== null);

        selectedCheckbox.forEach(val => {
          //check has other answer
          let other_ans: iother = this.other.find(
            ele => ele.id === Number.parseInt(el.key)
          );
          let cba: ianswer = {
            question_id: Number.parseInt(el.key),
            ans_text: null,
            ans_choice: val != -1 ? val : NaN,
            ans_other: val == -1 ? other_ans.value : null
          };
          ans_info.req_ans.push(cba);
        });
      } else if (el.controlType === "subject") {
        ans.ans_choice = NaN;
        ans.ans_other = null;
        ans.ans_text = this.form.value[el.key];
        ans.question_id = Number.parseInt(el.key);
        ans_info.req_ans.push(ans);
        ans_info.subject = this.subject;
      } else if (
        el.controlType === "dropdown" ||
        el.controlType === "multichoice"
      ) {
        let other_ans: iother = this.other.find(
          ele => ele.id === Number.parseInt(el.key)
        );
        ans.ans_choice =
          this.form.value[el.key] == -1 ? NaN : this.form.value[el.key];
        ans.ans_other = this.form.value[el.key] != -1 ? null : other_ans.value;
        ans.ans_text = null;
        ans.question_id = Number.parseInt(el.key);
        ans_info.req_ans.push(ans);
      } else if (el.controlType === "file") {
        ans.upload_ans = [];
        this.form.value[el.key].forEach((val, index) => {
          if (val != null) {
            let uploadFile: iupload_ans = {
              filename: val.filename,
              image: val.image,
              upload_id: el.filedetail.id,
              upload_seq: index + 1
            };
            ans.upload_ans.push(uploadFile);
          }
        });
        ans.ans_choice = NaN;
        ans.ans_other = null;
        ans.ans_text = null;
        ans.question_id = Number.parseInt(el.key);
        ans_info.req_ans.push(ans);
      } else {
        ans.ans_choice = NaN;
        ans.ans_other = null;
        ans.ans_text = this.form.value[el.key];
        ans.question_id = Number.parseInt(el.key);
        ans_info.req_ans.push(ans);
      }
    });
    if (this.checkValid()) {
      let dialogRef = this.dialog.open(FormConfirmComponent, {
        width: "300px",
        data: { answer: ans_info }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result != "") {
          this._rt.navigate(["student/history"]);
        }
      });
    } else {
      this.alertNotValid = true;
    }
  }

  getother(event) {
    let findother = this.other.find(el => el.id === event.id);
    if (!findother) {
      this.other.push(event);
    } else {
      findother.value = event.value;
    }
  }

  back() {
    this._rt.navigate(["/student/history"]);
  }
}
