import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicFormComponent } from './dynamic-form.component';
import { NavComponent } from 'src/app/shared/nav/nav.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/app.module';
import { DynamicFormQuestionComponent } from '../dynamic-form-question/dynamic-form-question.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from 'src/app/auth/auth.service';
import { HttpModule } from '@angular/http';
import { By } from '@angular/platform-browser';

describe('DynamicFormComponent', () => {
  let component: DynamicFormComponent;
  let fixture: ComponentFixture<DynamicFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DynamicFormComponent, NavComponent, DynamicFormQuestionComponent],
      providers: [AuthService],
      imports: [ReactiveFormsModule, FormsModule, MaterialModule, HttpClientTestingModule, RouterTestingModule, HttpModule]

    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicFormComponent);
    fixture.componentInstance.id = 1
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {

  });
});
