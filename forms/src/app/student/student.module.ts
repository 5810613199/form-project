import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { MaterialModule } from '../app.module';
import { HttpModule } from "@angular/http";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";


import { StudentRoutingModule } from './student-routing.module';
import { HomeComponent } from './home/home.component';
import { HistoryComponent } from './history/history.component';
import { RequestComponent } from './request/request.component';
import { DynamicFormComponent } from './dynamic-form/dynamic-form.component';
import { DynamicFormQuestionComponent } from './dynamic-form-question/dynamic-form-question.component';
import { SharedModule } from '../shared/shared.module';
import { FormConfirmComponent } from './form-confirm/form-confirm.component';
import { TokenInterceptorService } from '../http-interceptors/token-interceptor.service';
import { StudentService } from './student.service';


@NgModule({
  imports: [
    CommonModule,
    StudentRoutingModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    SharedModule

  ],
  declarations: [HomeComponent, HistoryComponent, RequestComponent, DynamicFormComponent, DynamicFormQuestionComponent,  FormConfirmComponent],
  entryComponents: [FormConfirmComponent],
  providers: [
    StudentService,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true },

  ]
})
export class StudentModule { }
