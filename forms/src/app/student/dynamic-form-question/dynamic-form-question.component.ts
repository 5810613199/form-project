import { Component, Input, ViewChild, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { StudentService } from "../student.service";
import { QuestionBase } from '../../utils/question-class/question-base';
import { isubject, isection, iquestion_choice, ianswer, iteacher_subject, iupload_ans } from "../../utils/interfaces";
import { ActivatedRoute } from '@angular/router';
import { MatChipInputEvent } from '@angular/material';


export class FileUploadModel {
  data: File;
  state: string;
  inProgress: boolean;
  progress: number;
  canRetry: boolean;
  canCancel: boolean;
  touch: boolean;
}

@Component({
  selector: 'app-question',
  templateUrl: './dynamic-form-question.component.html',
  styleUrls: ['./dynamic-form-question.component.css']
})
export class DynamicFormQuestionComponent implements OnInit {

  @Input() question: QuestionBase<any>;
  @Input() form: FormGroup;
  @Input() disable: boolean;
  @Output() other: EventEmitter<any> = new EventEmitter();
  @Output() subject: EventEmitter<any> = new EventEmitter();
  // @Output() fileUpload: EventEmitter<any> = new EventEmitter();
  // @ViewChild('fileInput') FileInput;
  fileToUpload: File = null;

  subjects: isubject[] = [];
  sections: isection[] = [];
  answers: ianswer[] = [];
  backupSection: isection[] = [];
  formTeacher: FormControl
  formSubject: FormControl
  formOther: FormControl
  disabledOther: boolean = true;
  req_id: any;
  files: any[] = [];
  accept: string = 'image/*'
  fileUrl: any[] = [];
  teachers: iteacher_subject[] = []
  showLimit: boolean = false;
  fileIndex: number = NaN;
  warning_limit_numfile: string = 'limit upload file to ';
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];



  get isValid() {
    if (this.question.controlType === 'file') {
      return !!this.files.length
    }
    return this.form.controls[this.question.key].valid &&
      this.form.controls[this.question.key].touched;
  }

  constructor(private ss: StudentService, private actRouter: ActivatedRoute) {

  }

  ngOnInit() {

    if (this.disable) {
      this.req_id = this.actRouter.snapshot.url[1].path

      this.ss.getAnswerForm(this.req_id).subscribe(res => {
        if (res) {
          let ans: ianswer[] = res.req_s.req_ans
          ans.forEach(el => {
            if (el.question_id == Number.parseInt(this.question.key)) {
              this.answers.push(el)
            }

          })
        }


      })
    }

    if (this.question.controlType === 'subject') {
      this.getSubject()
      this.formTeacher = new FormControl('', Validators.required)
    }
    if (this.question.controlType === 'file') {
      if (this.question.filedetail.file_type.indexOf('อื่น ๆ') != -1) {
        this.accept = '';
      } else {
        this.accept = this.question.filedetail.file_type.replace(/\/+/g, ',')
      }
      this.warning_limit_numfile += this.question.filedetail.n_file
    }

    if (this.question.controlType === 'multichoice' || this.question.controlType === 'checkbox') {

      if (this.question.other) {
        this.formOther = new FormControl('', Validators.required)
        let obj: iquestion_choice = {
          id: -1,
          choice_seq: this.question.options.length + 1,
          choice_detail: 'อื่น ๆ',
          question_id: Number.parseInt(this.question.key)
        }
        this.question.options.push(obj)
      }
    }

  }

  checkNoAnswer(answer: ianswer) {
    return !answer.ans_text && !answer.ans_other && !answer.upload_ans.length && !answer.ans_choice
  }

  onSelectedFile() {
    const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
    let fs: any
    fileUpload.onchange = () => {
      for (let index = 0; index < fileUpload.files.length; index++) {
        let file = (fileUpload.files[index])
        let farr = this.form.controls[this.question.key] as FormArray
        let fileReader = new FileReader()
        fileReader.readAsDataURL(file)
        fileReader.onload = (event: any) => {
          let obj = {};
          obj[this.question.key] = event.target.result
          if (this.pushFile(file)) {
            farr.at(this.files.length - 1).patchValue({ image: event.target.result, filename: file.name })
          }
        }
      }
    }
    fileUpload.click();




  }

  downloadfile(filename: string) {
    this.ss.getFile(filename).subscribe(res => {
      var url = window.URL.createObjectURL(res);
      var a = document.createElement('a');
      document.body.appendChild(a);
      a.setAttribute('style', 'display: none');
      a.href = url;
      a.download = filename;
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove();
    }, error => {
    }, () => {
    });

  }

  isImage(file: iupload_ans): boolean {
    let imageType = ['.png', '.jpg', 'jpeg']
    if (imageType.indexOf(file.upload_file) != -1) {
      return true;
    }
    return false;
  }


  pushFile(file: any): boolean {
    if (this.files.length < this.question.filedetail.n_file) {
      this.files.push(file)
      this.showLimit = false;
      return true;
    }
    this.showLimit = true;
    return false

  }

  add(event: MatChipInputEvent): void {

  }



  cancelFile(file) {
    // file.sub.unsubscribe();
    const index = this.files.indexOf(file);
    if (index > -1) {
      this.files.splice(index, 1);
      let farr = this.form.controls[this.question.key] as FormArray
      farr.removeAt(index)
      farr.push(new FormControl())
      this.showLimit = false;
    }
  }

  selectedOther(i) {
    if (this.question.options[i].id === -1) {
      this.disabledOther = !this.disabledOther;
    }
  }


  show() {
  }

  sendSubject(event) {
    this.subject.emit(event.value)
  }


  getSubject() {
    this.ss.getAllSubject().subscribe(res => {
      this.teachers = (res)
    })
  }

  filterSection(event) {
    this.sections = this.backupSection.filter(el => el.subcode === event.value)
    this.sections.sort((a, b) => a.section - b.section)
    this.form.value[this.question.key] = event.value
  }

  select(event) {
    let ans: any[] = this.form.value[this.question.key].split('/')
    let obj: any = {}
    obj[this.question.key] = ans[0] + '/' + event.value
    this.form.patchValue(obj)
  }

  log(x: any) {
  }

  changeState(key: string, i: number) {
    let length = this.form.value[key].length
    let deleteIndex = NaN;
    if (length) {
      for (let j = 0; j < length; j++) {
        if (this.form.value[key][j] === this.question.options[i].id) {
          deleteIndex = j;
        }
      }
    }
    //check disabledOther
    if (this.question.options[deleteIndex] === -1) {
      this.disabledOther = true;
    } else {
      this.disabledOther = false;
    }

    if (isNaN(deleteIndex)) {
      this.form.value[key].push(this.question.options[i].id)
    } else {
      this.form.value[key].splice(deleteIndex, 1)
    }
  }

  radioChange(event) {
    if (event.value == -1) {
      this.disabledOther = false;
    } else {
      this.disabledOther = true;
    }
  }

  sendother() {
    let q = { id: this.question.key, value: this.formOther.value }
    this.other.emit(q)
  }

  updateRadioOther(key: string, value: string) {
    if (!this.disabledOther) {
      let arr: any[] = this.form.value[this.question.key].split('/')
      let obj: any = {};
      obj[key] = arr[0] + '/' + value;
      this.form.patchValue(obj)
    }
  }

  updateCheckboxOther(key: string, value: string) {
    if (!this.disabledOther) {
      let length = this.form.value[key].length
      let obj = this.form.value[key]
      let temp = []
      for (let i = 0; i < length; i++) {
        if (typeof (obj[i]) != 'number') {
          obj[i].other = value
        }
        temp.push(obj[i])
      }

      this.form.value[key] = temp


    }
  }

  checkboxOther(key) {
    let length = this.form.value[key].length
    let objOther = { other: null }
    let deleteIndex = NaN;
    if (length) {
      for (let j = 0; j < length; j++) {
        if (typeof (this.form.value[key][j]) != 'number') {
          deleteIndex = j;
        }
      }
    }

    if (isNaN(deleteIndex)) {
      this.disabledOther = false;
      this.form.value[key].push(objOther)
    } else {
      this.disabledOther = true;
      this.form.value[key].splice(deleteIndex, 1)
    }
  }

  onFileComplete(data: any) {
  }

  check() {
  }

  get diagnostic() {
    return JSON.stringify(this.form)
  }




}
