import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { HistoryComponent } from './history/history.component';
import { RequestComponent } from "./request/request.component";
import { StudentGuardService } from './student-guard.service';

const routes: Routes = [
  { path: 'home', component: HomeComponent,  },
  { path: 'history', component: HistoryComponent, 
  // canActivate: [StudentGuardService] 
},
  { path: 'request/:id', component: RequestComponent,
  //  canActivate: [StudentGuardService] 
},
  { path: 'history/:id', component: RequestComponent, 
  // canActivate: [StudentGuardService] 
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentRoutingModule { }
