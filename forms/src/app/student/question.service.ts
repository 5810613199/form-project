import { Injectable } from '@angular/core';
import { StudentService } from "./student.service";
import { QuestionBase } from "../utils/question-class/question-base";
import { CheckboxQuestion } from "../utils/question-class/question-checkbox";
import { DropdownQuestion } from "../utils/question-class/question-dropdown";
import { TextboxQuestion } from "../utils/question-class/question-text";
import { FileQuestion } from "../utils/question-class/question-file";
import { MultichoiceQuestion } from "../utils/question-class/question-multichoice";
import { ParagraphQuestion } from "../utils/question-class/question-paragraph";

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor(private fs: StudentService) { }

  getQuestions(): QuestionBase<any>[] {

    return;
  }


}
