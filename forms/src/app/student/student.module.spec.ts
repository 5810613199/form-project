import { StudentModule } from './student.module';

describe('StaffPageModule', () => {
  let studentModule: StudentModule;

  beforeEach(() => {
    studentModule = new StudentModule();
  });

  it('should create an instance', () => {
    expect(studentModule).toBeTruthy();
  });
});
