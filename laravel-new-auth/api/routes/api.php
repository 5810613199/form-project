<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return  $request->user();
});

Route::middleware('auth:api')->get('/checktoken', function (Request $request) {
    return  response()->json(true, 200);
});




//------------------------------ Form --------------------------
// List form
Route::get('getAllForms', 'FormController@index');

// get form
Route::get('getFormByID/{id}', 'FormController@getFormToShow');

// get form question
Route::get('getQuestions/{id}', 'FormController@getQuestions');

// update form
Route::put('formupdate', 'FormController@updateForm');

// create form
Route::post('formcreate', 'FormController@createForm');

// Delete form
Route::delete('form/{id}', 'FormController@destroy');

// Update form name
Route::put('updateFormName', 'FormController@updateFormName');

// Update form pos (swap)
Route::put('updatePosition', 'FormController@updatePosition');

//------------------------------ Request --------------------------

// Check can mange
Route::post('checkCanManage/{id}', 'RequestController@checkCanManage');

// List All Request 
Route::get('getAllRequests/{id}', 'RequestController@getAllRequest');

// get RequestByid !!!!change output
Route::get('getRequestById/{id}', 'RequestController@getRequestById');

// get Comment
Route::get('getComment/{id}', 'RequestController@getComment');

// get Req_ans !!!!change output
Route::get('getAnswerForm/{req_id}', 'RequestController@getAnswerForm');

// submitForm(Request) 
Route::post('submitForm', 'RequestController@submitForm');

// submit Comment
Route::post('submitComment', 'RequestController@submitComment');

// get Signer
Route::get('getSigner', 'RequestController@getSigner');

// get Subject !!!!change output
Route::get('getSubject', 'RequestController@getSubject');

// get Subject_detail !!!!change output
Route::post('getSubjectDetail', 'RequestController@getSubjectDetail');

// get Advisor !!!!change output
Route::get('getAdvisor/{id}', 'RequestController@getAdvisor');

// get All Req !!!!change output
Route::get('getInfoReq', 'RequestController@getInfoReq');



// -------------------------- UserController ----------------------------

// checkstudent
Route::get('checkstudent', 'UserController@checkIsStudent');

// check teacher
Route::get('checkteacher', 'UserController@checkIsTeacher');

// check staff
Route::get('checkstaff', 'UserController@checkIsStaff');

// check admin
Route::get('checkadmin', 'UserController@checkIsAdmin');





// get Student info !!! merge uid and id && change output
Route::get('getStudentInfo/{id}', 'UserController@getStudentInfo');

// get Teacher info  !!! change output
Route::get('getTeacherInfo/{id}', 'UserController@getTeacherInfo');

// get Staff info    !!!change output
Route::get('getStaffInfo/{id}', 'UserController@getStaffInfo');

// get Role By ID    !!! change output
Route::get('getRoleByIdTeacher/{id}', 'UserController@getRoleByIdTeacher');

// get setable config
Route::post('getUserInfo', 'UserController@getUserInfo');


// get all teacher staff !!!change output
Route::get('getTeacherStaff', 'UserController@getTeacherStaff');


// get all staff 
Route::post('getAllStaff', 'UserController@getAllStaff');


// download file with file name
Route::get('downloadfile/{filename}', 'RequestController@downloadfile');

// check valid token
Route::post('checkvalidtoken', 'UserController@checkvalidtoken');

// download file with file name


// logout api

Route::group(['middleware' => 'auth:api'], function(){
    // set role
    Route::post('setConfig', 'UserController@setConfig');

    // set operation_lv !!!change input add name_of_position
    Route::post('setOperate_lv', 'UserController@setOperate_lv');

    // get All Teacher  !!!change output
    Route::get('getAllTeacher', 'UserController@getAllTeacher');



    // create new config
    Route::post('createConfig', 'UserController@createConfig');

    // get all operate_lv
    Route::get('getAllOperate_lv', 'UserController@getAllOperate_lv');

    // get setable config
    Route::get('getsetConfig', 'UserController@getsetConfig');

    Route::delete('deleteConfig/{id}', 'UserController@deleteConfig');

});
Route::post('checkauth', 'UserController@checkIsAuth');
Route::post('logout','UserController@logoutApi');




// Route::group(['middleware' => ['web']], function () {
    Route::post('loginAd', 'AuthAd\LoginController@loginAdmin');
    Route::post('logoutAd', 'AuthAd\LoginController@logout');
// });

