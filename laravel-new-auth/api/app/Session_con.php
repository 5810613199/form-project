<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session_con extends Model
{
    //Table Name
    protected $table = 'sessions';

    protected $connection = 'form';
    
    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    // we only want these 10 attributes able to be filled
    protected $fillable = array('username', 'payload', 'last_activity');

    // Timestamps
    public $timestamps = false;
}
