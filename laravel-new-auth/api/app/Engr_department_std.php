<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Engr_department_std extends Model
{
    //Table Name
    protected $table = 'engr_department';

    protected $connection = 'std';

    protected $fillable = array('depid', 'major_th', 'major_en', 'department_th', 'department_en', 'dep_refer');

    // Timestamps
    public $timestamps = false;
}
