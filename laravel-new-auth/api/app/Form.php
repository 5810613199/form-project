<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    //Table Name
    protected $table = 'form';
    protected $connection = 'form';

    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    // we only want these 5 attributes able to be filled
    protected $fillable = array('form_name', 'mod_by', 'mod_date', 'created_at', 'updated_at', 'form_pos');

    // Timestamps
    public $timestamps = true;

    // DEFINE RELATIONSHIPS --------------------------------------------------
    // each form HAS many form_question
    public function form_question() {
        return $this->hasMany('App\Form_question', 'form_id'); // this matches the Eloquent model
    }

    // each form HAS many sign_control
    public function sign_control() {
        return $this->hasMany('App\Sign_control', 'form_id'); // this matches the Eloquent model
    }

    // each form HAS many req_status
    public function req_status() {
        return $this->hasMany('App\Req_status'); // this matches the Eloquent model
    }

    // each form HAS many upload_q
    public function upload_q() {
        return $this->hasMany('App\Upload_q', 'form_id'); // this matches the Eloquent model
    }


}
