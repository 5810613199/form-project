<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operation_lv extends Model
{
    //Table Name
    protected $table = 'operation_lv';
    
    protected $connection = 'form';

    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    // we only want these 6 attributes able to be filled
    protected $fillable = array('level_operate', 'name_of_position');

    // Timestamps
    public $timestamps = false;

}
