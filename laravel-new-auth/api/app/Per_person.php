<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Per_person extends Model
{
    //Table Name
    protected $table = 'per_person';

    protected $connection = 's_t';

    protected $fillable = array('personid', 'title', 'fname_t', 'lname_t', 'fname_e', 'lname_e', 'department', 'title_academic', 'ad_user');

    // Timestamps
    public $timestamps = false;
}
