<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Engr_titleacademic extends Model
{
    //Table Name
    protected $table = 'engr_titleacademic';

    protected $connection = 's_t';

    protected $fillable = array('titleacaid', 'titleacafullt', 'titleacafulle', 'titleacalitt', 'titleacalite');

    // Timestamps
    public $timestamps = false;
}
