<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Carbon\Carbon;

class Req_status extends Model
{
    //Table Name
    protected $table = 'req_status';

    protected $connection = 'form';
    
    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    // we only want these 10 attributes able to be filled
    protected $fillable = array('form_id', 'student_id', 'sign_t1_s', 'sign_t2_s', 'sign_t3_s', 'sign_t4_s', 'sign_t5_s', 'created_at', 'updated_at', 'status');

    // Timestamps
    public $timestamps = true;

    // DEFINE RELATIONSHIPS --------------------------------------------------
    // each req_status belong to form
    public function form() {
        return $this->belongsTo('App\Form'); // this matches the Eloquent model
    }

    // each req_status HAS many req_ans
    public function req_ans() {
        return $this->hasMany('App\Req_ans', 'req_id'); // this matches the Eloquent model
    }

    // each req_status HAS many comment
    public function comment() {
        return $this->hasMany('App\Comment', 'req_id'); // this matches the Eloquent model
    }

}
