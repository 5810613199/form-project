<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question_choice extends Model
{
    //Table Name
    protected $table = 'question_choice';
    protected $connection = 'form';

    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    // we only want these 3 attributes able to be filled
    protected $fillable = array('question_id', 'choice_detail', 'choice_seq');

    // Timestamps
    public $timestamps = false;

    // DEFINE RELATIONSHIPS --------------------------------------------------
    // each question_choice belong to form_question
    public function form_question() {
        return $this->belongsTo('App\Form_question', 'id'); // this matches the Eloquent model
    }

    // each question_choice HAS many req_ans
    public function req_ans() {
        return $this->hasMany('App\Req_ans'); // this matches the Eloquent model
    }
}
