<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Engr_title_prt extends Model
{
    //Table Name
    protected $table = 'engr_titlename';

    protected $connection = 's_t';

    protected $fillable = array('id', 'title_name_t', 'title_name_e');

    // Timestamps
    public $timestamps = false;
}
