<?php

namespace App\Http\Controllers\AuthAd;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use App\Form;
use App\Form_question;
use App\Question_choice;
use App\Upload_q;
use App\Sign_control;
use App\User_fake;
use App\User;
use App\Stu_student;
use App\Per_person;
use App\Per_prtson_detail;
use App\Engr_department_prt;
use App\Engr_titleacademic;
use App\Engr_title_prt;
use App\Engr_title_std;
use App\Engr_degree_std;
use App\Engr_department_std;
use App\Req_status;
use App\Req_ans;
use App\Comment;
use App\Config_db;
use App\Subject_db;
use App\Upload_ans;
use App\Session_con;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:ad')->except('logout');
    }

    public function login(Request $request)
    {

    }

    /**
    * Validate the user login request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return void
    */
    protected function validateLogin(Request $request)
    {
    $this->validate($request, [
            'username' => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)
            ? $this->username()
            : 'username';

        return [
            $field => $request->get($this->username()),
            'password' => $request->password,
        ];
    }

    public function username()
    {
        $loginType = request()->input('username');
        $this->username = filter_var($loginType, FILTER_VALIDATE_EMAIL)? 'email' : 'username';
        request()->merge([$this->username => $loginType]);

        return property_exists($this, 'username') ? $this->username : 'email';
    }

    public function loginAdmin(Request $request)
    {
      // Attempt to log the user in
      if (Auth::guard('ad')->attempt(['username' => $request->username, 'password' => $request->password], $request->remember)) {
        Session_con::where('username', $request->username)->delete();
        $ses = new Session_con();
        $ses->username = $request->username;
        $ses->payload = 'login';
        $ses->last_activity= 0;
        $ses->save();
        
        
        return response()->json(Auth::user(), 200);
      }
      // if unsuccessful, then redirect back to the login with the form data
      return response()->json(false, 200);
    }
    public function logout(Request $request)
    {
        $username = $request->header('username');
        if(Session_con::where('username', $username)->delete()){
            return response()->json(true, 200);
        }
        return response()->json(false, 200);
    }
}
