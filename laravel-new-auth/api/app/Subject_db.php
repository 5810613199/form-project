<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject_db extends Model
{
    //Table Name
    protected $table = 'subject';

    protected $connection = 'form';
    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    // we only want these 5 attributes able to be filled
    protected $fillable = array('personid', 'sub_code', 'sub_name');

    // Timestamps
    public $timestamps = false;

}
