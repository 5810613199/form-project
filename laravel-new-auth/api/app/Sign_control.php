<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sign_control extends Model
{
    //Table Name
    protected $table = 'sign_control';
    protected $connection = 'form';

    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    // we only want these 7 attributes able to be filled
    protected $fillable = array('form_id', 'n_sign', 'sign_t1', 'sign_t2', 'sign_t3', 'sign_t4', 'sign_t5');

    // Timestamps
    public $timestamps = false;

    // DEFINE RELATIONSHIPS --------------------------------------------------
    // each sign_control belong to form
    public function form() {
        return $this->belongsTo('App\Form', 'id'); // this matches the Eloquent model
    }
    
}
