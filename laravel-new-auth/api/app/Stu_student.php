<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stu_student extends Model
{
    //Table Name
    protected $table = 'stu_student';

    protected $connection = 'std';

    protected $fillable = array('id', 'stu_code', 'title_id', 'fname_th', 'lname_th', 'fname_en', 'lname_en', 'degree_id',
                                 'department_id', 'advisor_id', 'reg_telephone', 'email');

    // Timestamps
    public $timestamps = false;
}
