<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Per_prtson_detail extends Model
{
    //Table Name
    protected $table = 'per_prtson_detail';

    protected $connection = 's_t';

    protected $fillable = array('personid', 'idcard', 'email', 'positionid');

    // Timestamps
    public $timestamps = false;
}
